// Daniel Shiffman http://codingtra.in 
// Videos // https://youtu.be/HyK_Q5rrcr4
// Depth-first search / Recursive backtracker
// https://en.wikipedia.org/wiki/Maze_generation_algorithm
// Turned into a circular playable Maze 
// by Gilles Gonon http://gilles.gonon.free.fr


let cols, rows, h;
let innerRadius = 100; // inner ring radius
let nRings = 4; // 
let nCellsPerRing = 10;
let angResolution;
let grid = [];
let current;
let stack = []; // Used for maze generation (back propagation)
let moves = []; // store user moves in play mode
let alpha;
let alphaTarget;
let target; // Index of target cell
let distTarget;
let txtInfo;
let nState, bWin;
let level;
let sndR, sndL, sndUD, sndW, sndWin, sndGen, sndEndGen; // game sounds
let moveSeq = []; // Record sequence moves to play it as a beat

function preload() {
  soundFormats('mp3');
  sndL = loadSound('../assets/medias/left.mp3');
  sndR = loadSound('../assets/medias/right.mp3');
  sndUD = loadSound('../assets/medias/up-down.mp3');
  sndW = loadSound('../assets/medias/wall.mp3');
  sndWin = loadSound('../assets/medias/win.mp3');
  sndGen = loadSound('../assets/medias/generate.mp3');
  sndEndGen = loadSound('../assets/medias/end-gen.mp3');
}

function windowResized() {
  let cnvSize = min(windowHeight - 140, 0.98 * windowWidth);
  resizeCanvas(cnvSize, cnvSize);
  document.getElementById("buttons").style.width = cnvSize+'px';
  // update size params
  innerRadius = map(width,360,1200, 50,100);
  h = floor((0.5 * width - innerRadius) / nRings);
}

function setup() {
  // Interface
  let cnvSize = min(windowHeight - 140, 0.98 * windowWidth);
  createCanvas(cnvSize, cnvSize);
  document.getElementById("buttons").style.width = cnvSize+'px';
  // const oButtons = { "LEFT": "←", "UP": "↑", "DOWN": "↓", "RIGHT": "→" };
  // for (const prop in oButtons) {
  //   let btn = createButton(oButtons[prop]);
  //   btn.size(50, 50);
  //   btn.mousePressed(function () { move(prop); });
  // }

  // Maze parameters 
  cols = nCellsPerRing;
  rows = nRings;
  console.log('Maze size : '+ (nCellsPerRing*nRings));
  // compute cell height & angular width
  innerRadius = map(width,360,1200, 50,100);
  h = floor((0.5 * width - innerRadius) / nRings);
  angResolution = TWO_PI / nCellsPerRing;
  // Create maze cell
  for (let j = 0; j < rows; j++) {
    for (let i = 0; i < cols; i++) {
      var cell = new Cell(i, j);
      grid.push(cell);
    }
  }

  nState = 0; // Program state : 0:generating | 1:playing | 2: playing beat
  bWin = false; // Playing status : has win ?
  current = grid[0]; // Starting cell
  target = current; // Storing target cells used for output
  distTarget = 0;    // Use furthest cell 

  txtInfo = "Generating Maze";
  alpha = 0; alphaTarget = 0; // Maze orientation (with easing)
  level = 0;
  strokeWeight(4); // Wall size
  sndGen.loop();


}

function draw() {
  background(255, 245, 215);

  // Smooth update for alpha → alphaTarget
  alpha += (alphaTarget - alpha) * 0.05;

  push();
  translate(width / 2, height / 2);
  rotate(-HALF_PI - 0.5 * angResolution + alpha);  // alpha += level/100;
  for (let i = 0; i < grid.length; i++) {
    grid[i].show();
  }
  target.highlight(color(255, 0, 0, 128))

  current.visited = true;
  current.highlight();

  pop();

  if (nState == 0) { // Generating Maze
    // STEP 1
    let next = current.checkNeighbors();
    if (next) {
      next.visited = true;

      // STEP 2
      stack.push(current);
      // STEP 3
      removeWalls(current, next);
      // STEP 4
      current = next;
      txtInfo = "Generating\nNew\nMaze";
      
      // Set target to furthest position
      if (stack.length > distTarget) {
        distTarget = stack.length;
        target = current;
      }

    } else if (stack.length > 0) {
      // When we reach last unvisited cell stack.length== #total Cells
      let nVisited = grid.reduce((acc,el) => acc+el.visited, 0);
      if (nVisited == nRings*nCellsPerRing) {
        txtInfo = 'Ready';
        current =  grid[0];
        stack = [];
      } else {
        current = stack.pop();
        txtInfo = 'Stuck\nBacktracking\npath';  
      }
    } else {
      nState = 1; // Switch to playMode()
      txtInfo = 'Play !';
      sndGen.stop();
      sndEndGen.play();
    } 
  } else if (nState == 1) {
    // Playing Mode
    if (current == target) {
      txtInfo = 'You win !\n\'r\' to \nRestart';
      
      if (!bWin) { 
        sndWin.play(); 
        sndWin.onended(() => { /* Beat mode */ 
          nState = 2; 
          playBeatMaze(); 
          sndWin.onended(() => {}); }); 
      }
      bWin = true;
      alphaTarget = -2 * TWO_PI;
      // Change level after turning
      // if (abs(alpha - alphaTarget) < 0.2) { 
      //   level++; 
      //   resetGrid(); 
      // }
    }
  } else {
    // Beat mode
    
  }


  textAlign(CENTER, CENTER);
  textSize(26)
  fill(25, 25, 128)
  text(txtInfo, 0.5 * width, 0.5 * height)

}

function index(i, j) {
  if (i < 0 || j < 0 || i > cols - 1 || j > rows - 1) {
    return -1;
  }
  return i + j * cols;
}

function removeWalls(a, b) {
  let x = a.i - b.i;
  if (x === 1 || x === -cols + 1) {
    a.walls[3] = false;
    b.walls[1] = false;
  } else if (x === -1 || x === cols - 1) {
    a.walls[1] = false;
    b.walls[3] = false;
  }
  let y = a.j - b.j;
  if (y === 1) {
    a.walls[0] = false;
    b.walls[2] = false;
  } else if (y === -1) {
    a.walls[2] = false;
    b.walls[0] = false;
  }
}

function keyPressed() {
  if (key == 'r') { // Reset grid
    resetGrid();
  }
  if (nState == 1) {
    // console.log('Current i:'+current.i+', j:'+current.j)
    if (keyCode == UP_ARROW) {
      move('UP');
    }
    if (keyCode == DOWN_ARROW) {
      move('DOWN');
    }
    if (keyCode == LEFT_ARROW) {
      move('LEFT');
    }
    if (keyCode == RIGHT_ARROW) {
      move('RIGHT');
    }
    // console.log('New i:'+current.i+', j:'+current.j)
  }
}

function move(direction) {
  if (nState != 1 && !bWin) return undefined; // No move while generating maze

  snd = undefined; // sound to play
  switch (direction) {
    case 'UP':
      idxCell = (current.j + 1) * cols + current.i
      if (!current.walls[2] && idxCell < grid.length) {
        current = grid[idxCell];
        snd = sndUD;
      } else {
        snd = sndW;
      }
      break;

    case 'DOWN':
      idxCell = (current.j - 1) * cols + current.i
      if (!current.walls[0] && idxCell >= 0) {
        current = grid[idxCell];
        snd = sndUD;
      } else {
        snd = sndW;
      }
      break;

    case 'RIGHT':
      if (current.i == cols - 1) {
        idxCell = (current.j) * cols
      } else {
        idxCell = (current.j) * cols + current.i + 1
      }

      if (!current.walls[1]) {
        current = grid[idxCell];
        snd = sndR;
      } else {
        snd = sndW;
      }
      alphaTarget = alphaTarget - angResolution;
      break;

    case 'LEFT':
      if (current.i == 0) {
        idxCell = (current.j) * cols + cols - 1
      } else {
        idxCell = (current.j) * cols + current.i - 1
      }

      if (!current.walls[3]) {
        current = grid[idxCell];
        snd = sndL;
      } else {
        snd = sndW;
      }
      alphaTarget = alphaTarget + angResolution;
      break;

    default:
      console.log('unknown move');
      break;
  }

  if(snd) {
    moves.push({'sound':snd, 'at':millis()/1000});
    snd.play();
  }
}

function resetGrid() {
  console.log('Resetting Grid !');
  sndWin.stop();
  sndGen.loop();
  // Adds rings or cell per ring
  if (level % 2) {
    nRings++;
  } else {
    nCellsPerRing += 4;
  }
  // Reset grid sizes
  cols = nCellsPerRing;
  rows = nRings;
  angResolution = TWO_PI / nCellsPerRing;
  // cell height
  h = floor((0.5 * width - innerRadius) / nRings);
  grid = [];
  for (let j = 0; j < rows; j++) {
    for (let i = 0; i < cols; i++) {
      var cell = new Cell(i, j);
      grid.push(cell);
    }
  }

  stack = [];
  current = grid[0];
  target = current;
  distTarget = 0;
  nState = 0; bWin = false;
  // alpha = 0; // reset only alphaTarget to return smoothly
  alphaTarget = 0;

}

function playBeatMaze() {
  // Play the Beat
  console.log('Beat Mode')
  txtInfo = "Your moves\nare\nthe beat !"
  
  let tempo = 100; // Target tempo
  let quantiz = tempo/(16*60); // duration unit quantized at 1/16 step
  let meanDuration = (moves[moves.length-1].at - moves[0].at) / moves.length;
  // for (let i = 1; i < moves.length; i++) {
  //     meanDuration = min(meanDuration,moves[i].at - moves[i-1].at);
  // }
  console.log('Mean move duration :'+meanDuration)
  moves[0].sound.play(); // Play First sound
  let timeOff = 0;
  for (let i = 1; i < moves.length; i++) {
    const durQ = ceil((moves[i].at - moves[i-1].at)/meanDuration);
    console.log('durQ:'+durQ+', playAt:'+ (timeOff + durQ * quantiz))
    const playAt = timeOff + durQ * quantiz
    // Replay at tempo
    moves[i].sound.play(playAt);
    timeOff = playAt;
  }
  
  let endOfBeatMs = 1000*(timeOff+moves[moves.length-1].sound.duration());
  //console.log('End of beat at : '+ floor(endOfBeatMs )+ 'ms')
  setTimeout(() => {
    moves = [];
    resetGrid();
  }, endOfBeatMs);
}

const findMedian = (arr = []) => {
  const sorted = arr.slice().sort((a, b) => {
     return a - b;
  });
  if(sorted.length % 2 === 0){
     const first = sorted[sorted.length / 2 - 1];
     const second = sorted[sorted.length / 2];
     return (first + second) / 2;
  }
  else{
     const mid = Math.floor(sorted.length / 2);
     return sorted[mid];
  };
};