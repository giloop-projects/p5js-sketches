// Daniel Shiffman
// http://codingtra.in
// http://patreon.com/codingtrain

// Videos
// https://youtu.be/HyK_Q5rrcr4
// https://youtu.be/D8UgRyRnvXU
// https://youtu.be/8Ju_uxJ9v44
// https://youtu.be/_p5IH0L63wo

// Depth-first search
// Recursive backtracker
// https://en.wikipedia.org/wiki/Maze_generation_algorithm

function Cell(i, j) {
    this.i = i;
    this.j = j;
    this.walls = [true, true, true, true]; // Top, right, bottom, left
    this.visited = false;
  
    this.checkNeighbors = function() {
      // Returns an array of existing non visited cells around the current one
      let neighbors = [];
  
      let top = grid[index(i, j - 1)];
      let right = grid[index((i+1)%cols, j)];
      let bottom = grid[index(i, j + 1)];
      let left = grid[index(i==0?cols-1:i-1, j)];
    
      for (side of [top, right, bottom, left]) {
        if (side && !side.visited) {
            neighbors.push(side);
          }    
      }
  
      if (neighbors.length > 0) {
        let r = floor(random(0, neighbors.length));
        return neighbors[r];
      } else {
        return undefined;
      }
    };

    this.highlight = function(col = color(0, 0, 255, 100)) {
      let a1 = angResolution * this.i;
      let a2 = angResolution * (this.i + 1);
      let r1 = innerRadius + this.j * h;
      let r2 = innerRadius + (this.j+1) * h;
      let aRes = (a2-a1)/10.0; // ang resolution
      noStroke();
      fill(col);
      // quad(r2*cos(a1), r2*sin(a1), r2*cos(a2), r2*sin(a2), r1*cos(a2), r1*sin(a2), r1*cos(a1), r1*sin(a1));

      beginShape(TRIANGLE_STRIP);
      for(a=a1; a<a2+0.001*aRes; a=a+aRes) {
        vertex(r1*Math.cos(a), r1*Math.sin(a));
        vertex(r2*Math.cos(a), r2*Math.sin(a));
      }
      endShape();
      // circle((r1+r2)/2 * cos(0.5*(a1+a2)), (r1+r2)/2 * sin(0.5*(a1+a2)), r2-r1);
      // ellipse((r1+r2)/2 * cos(0.5*(a1+a2)), (r1+r2)/2 * sin(0.5*(a1+a2)), r2-r1);
    };
  
    this.show = function() {
      let a1 = (TWO_PI / nCellsPerRing) * this.i;
      let a2 = (TWO_PI / nCellsPerRing) * (this.i + 1);
      let r1 = innerRadius + this.j * h;
      let r2 = innerRadius + (this.j+1) * h;
      let a;
      let aRes = (a2-a1)/10.0; // ang resolution

      // Fill visited Cells
      if (this.visited) {
        noStroke();
        fill(240,180,90, 100); //255, 50, 255, 100);
        // quad(r2*cos(a1), r2*sin(a1), r2*cos(a2), r2*sin(a2), r1*cos(a2), r1*sin(a2), r1*cos(a1), r1*sin(a1));
        // Based on annulus function by jWilliamDunn 20200803 https://openprocessing.org/sketch/811370
        beginShape(TRIANGLE_STRIP);
        for(a=a1; a<a2+0.001*aRes; a = a + aRes) {
          vertex(r2*Math.cos(a), r2*Math.sin(a));
          vertex(r1*Math.cos(a), r1*Math.sin(a));
        }
        endShape();
      }

      // Draw Cell Walls
      push();
      noFill();
        stroke(25,25,100);
        // Wall orders : Top, right, bottom, left
        if (this.walls[2]) {
          beginShape();
          for(a=a1; a<a2+0.001*aRes; a = a + aRes) {
            vertex(r2*cos(a), r2*sin(a));
          }
          endShape();
          // line(r2*cos(a1), r2*sin(a1), r2*cos(a2), r2*sin(a2));
        }
        if (this.walls[3]) {
          line(r1*cos(a1), r1*sin(a1), r2*cos(a1), r2*sin(a1));
        }
        if (this.walls[0]) {
          // line(r1*cos(a1), r1*sin(a1), r1*cos(a2), r1*sin(a2));
          beginShape();
          for(a=a1; a<a2+0.001*aRes; a = a + aRes) {
          vertex(r1*cos(a), r1*sin(a));
          }
          endShape();
        }
        if (this.walls[1]) {
          line(r1*cos(a2), r1*sin(a2), r2*cos(a2), r2*sin(a2));
        }
      pop();



    };

    this.clearCell = function() {
        // Remove existing wall
        this.walls = [true, true, true, true]; // Top, right, bottom, left
        this.visited = false;
    }
  }


