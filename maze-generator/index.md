---
title: "Générateur de labyrinthe"
description: "Adaptation d'un épisode du coding train pour générer un labyrinthe circulaire. Une jouabilité originale qui fait bien travailler le cerveau."
image: labyrinthe-circ.jpg
category: code
tags: [generative, art, visual]
---

# Maze generator 

https://thecodingtrain.com/CodingChallenges/010.1-maze-dfs-p5.html

