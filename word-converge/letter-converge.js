let font;
function preload() {
  font = loadFont('../assets/fonts/Aileron-Thin.otf');
}

let points;
const N = 600;
const repulsion = 0.75;     // how much the particles repel each other
const contraction = 0.04; // how much they are pulled towards the center
const drift = 0.1;          // how strong the random forces are
const impulse = 50;           // number of frames until the particles reach the target
const activeTime = 100;        // number of frames until they become idle again

let damping = 0.99;      // 0:static, 1:no damp, >1:diverge
let idle = false;        // idle => particles repel and drift around
// const wordList = ["L'art", "rend", "Les gens", "meilleurs", "Giloop"];
const wordList = ["L'art rend","les gens", "meilleurs", "Giloop"];
let wordCpt = 0;
let counter = 0;
const parts = [];
let bShowPoints = false; // true; // 

function setup() {
  createCanvas(700, 300);
  stroke(5);
  fill(255, 104, 204);
  // colorMode(HSB, 100);
  background(100);
  smooth();
  //colorMode(RGB, 1, 1, 1);

  // Création des points pour le texte
  points = font.textToPoints(wordList[wordCpt], 0.05*width, 0.75*height, 100, {
    sampleFactor: 3.5,
    simplifyThreshold: 0
  });

  // Création des particules
  for (let i = 0; i < N; i++) {
    parts.push(new Particle(random(0, width), random(0, height), random(-2,2), random(-2,2)));
  }

}

function draw() {
  // background(250);
  
  if (bShowPoints) {
    // Affichage des points
    //translate(-bounds.x * width / bounds.w, -bounds.y * height / bounds.h);
    for (let i = 0; i < points.length; i++) {
      let p = points[i];
      //ellipse(p.x*width/bounds.w, p.y*height/bounds.h, 5, 5);
      ellipse(p.x, p.y, 3, 3);
    }
  }
  // update idle status 
  if (isIdle()) { 
    // randomPushParticles();
    idleUpdate(); 
  }
  if (!idle && counter > activeTime) { idle = true; }

// Erase alpha factor
  noStroke();
  fill(250,250,250,50);
  rect(0, 0, width, height);

  for (p of parts) {
    p.update();
    noStroke();
    p.draw();
  }

}

// isIdle if all particles have a null speed
function isIdle() {
  let nz = 0;
  for (p of parts) {
    if (abs(p.vx)<0.1 && abs(p.vy)<0.1) {
      nz++;
    }
  }
  // console.log(nz)
  return nz==parts.length;
}

function idleUpdate() { 
  damping = 0.95;
  for (p of parts) {
    // iterate over the particles which the current particle has not interacted with yet
    for (np of parts) {
      const p2npx = np.x-p.x;
      const p2npy = np.y-p.y;
      const sqdist = max(0.001, p2npx*p2npx + p2npy*p2npy);

      //apply repulsive forces
      p.vx -= p2npx/sqdist * repulsion;
      np.vx+= p2npx/sqdist * repulsion;
      p.vy -= p2npy/sqdist * repulsion;
      np.vy+= p2npy/sqdist * repulsion;
    }
  }
}

function pushParticles() {
  // Création des points pour le texte
  let x,y;
  if (wordCpt==0) {
    // First Word at same place for Gif purposes
    x = 0.15*width;
    y = 0.65*height;
  } else if  (wordCpt==wordList.length-1) {
    // Last Word at same place for Gif purposes
    x = 0.55*width;
    y = 0.85*height;
  } else {
    x = random(0,0.25)*width;
    y = random(0.5,0.95)*height;
  }
  points = font.textToPoints(wordList[wordCpt], x, y, 100, {
    sampleFactor: 4 ,
    simplifyThreshold: 0
  });  

  const isGiloop = (wordList[wordCpt] == "Giloop");

  for (p of parts) {
    // pick a random pixel of the letter and push the particle towards it
    let targPixel = floor(random(0, points.length));
    let targX = points[targPixel].x;
    let targY = points[targPixel].y;
    let dx = targX - p.x;
    let dy = targY - p.y;
    let factor = (1-damping)/(1-pow(damping, impulse));
    p.vx = dx*factor;
    p.vy = dy*factor;
    if (isGiloop) {
      p.c = color(random(0,100), random(128,255),random(0,100));
    } else {
      p.c = color(random(128,255),random(0,128), random(0,100));
    }
    //p.c = color(random(0,0.75),random(0.25, 0.75), random(0.25,0.9));
  } 
}

function randomPushParticles() {
  for (p of parts) {
    p.vx = random(-10, 10);
    p.vy = random(-5, 5);
    p.c = color(random(0, 255), random(20, 160), random(150, 250));
  }
  damping = 0.96;
}

function keyPressed() {
  if (key == ' ') {
     randomPushParticles();
  }
  if (key == 'b') {
    bShowPoints = !bShowPoints;
  }
  /*if (key >= '!' && key <= '~') {
    buffer[wordCpt] = buffer[wordCpt] + key;
  }*/
}

function changeWord() {
  damping = 0.91;
  pushParticles();
  wordCpt = (wordCpt+1)%wordList.length;
  idle = false;
  counter = 0;
}

/* Timeout update */
setInterval(changeWord, 5000);


/* A simple Particle Class */
class Particle {
  constructor(xx = 0, yy = 0, vxx = 0, vyy = 0) {
    this.x = xx;
    this.y = yy;
    this.vx = vxx;
    this.vy = vyy;
    this.s = 5 ; // size
    this.speedSq = 0;
    this.c = color(random(0, 255), random(20, 160), random(150, 250));
  }
  //color c = color(random(0,0.5), random(0.5,1), random(0.75,1));
  // blue green-ish
  //color c = color(random(0,0.1), random(0.25,0.5), random(0.5,0.75));
  // Dark red
  //color c = color(0.5,0.1,0.1);

  update() {
    this.x += this.vx;
    this.y += this.vy;
    this.vx *= damping;
    this.vy *= damping;
    this.speedSq = this.vx * this.vx + this.vy * this.vy;

    if (this.x < 0) {
      this.x = 0;
      this.vx = -this.vx;
    } else if (this.x > width) {
      this.x = width;
      this.vx = -this.vx;
    }

    if (this.y < 0) {
      this.y = 0;
      this.vy = -this.vy;
    } else if (this.y > height) {
      this.y = height;
      this.vy = -this.vy;
    }

  }

  draw() {
    noStroke();
    fill(this.c);
    ellipse(this.x, this.y, this.s, this.s);
  }
}
/* End Of simple Particle Class */