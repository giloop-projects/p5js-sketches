---
title: Convergence
description: La convergence des particules élémentaires peut parfois nous délivrer un message à méditer.
image: Giloop-lart.jpg
category: visual
tags: [generative, art, particles]
---

# Convergence de mots

Ce sketch rassemble des particules qui convergent vers un mot. J'ai vu qu'il y a avait un épisode du 
coding train sur ce thème, mais en fait ce sketch est un recodage d'un code Processing fait il y a quelques années. 
