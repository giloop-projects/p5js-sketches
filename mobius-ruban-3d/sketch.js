// Création d'un ruban de Möbius à partir des équations
// https://fr.wikipedia.org/wiki/Ruban_de_M%C3%B6bius
// Author : Gilles Gonon
// Site : http://gilles.gonon.free.fr
//

let a = 0;
let nTurns = 4;
let radius = 80;

function setup() {
  createCanvas(800, 400, WEBGL);
  // Setup for GIF loop creation (un|comment)
  // frameRate(20);
  // createLoop(5, { gif: { fileName: "moebiusLoop.gif" } })
}

function draw() {
  background(250);
  // Rotating visualization
  rotateY(frameCount * 0.01);
  // rotateY(animLoop.theta); // Rotation for GIF creation
  rotateX(HALF_PI);

  //drag to move the world.
  orbitControl();

  noFill();
  // fill(237, 34, 93, 128);
  strokeWeight(2);
  stroke(200,153,0);
  moebius(nTurns, radius);
  noStroke();
  fill(255,0,0);
  // Moving cubes with var a
  moebiusPoint(nTurns, radius, a);
  // moebiusPoint(nTurns, radius, animLoop.theta); // Moving cubes for GIF creation
  a += radians(1);
  if (a > TWO_PI) {a = 0;}
}


function moebius(k, r=50) {
  for (let t = -1; t < 1; t += 0.4) {
    beginShape(LINES);
    for (let v = 0; v < PI; v += radians(2)) {
      px = (2 +  t*cos(k*v)) * cos(2*v);
      py = (2 +  t*cos(k*v)) * sin(2*v);
      pz = t * sin(k*v);
      vertex(r*px, r*py, r*pz);
    }
    endShape(CLOSE);
  }
}

function moebiusPoint(k,r=50, angle) {
  for (let t = -1; t < 1; t += 0.4) {
      px = (2 +  t*cos(k*angle)) * cos(2*angle);
      py = (2 +  t*cos(k*angle)) * sin(2*angle);
      pz = t * sin(k*angle);
      push(); 
      translate(r*px, r*py, r*pz);
      box(5);
      pop();
  }
}