// Constants
const triSize = 40;
let nRows;
let nCols;

// Colors palettes, from https://coolors.co/palettes/trending
// import { arPalettes } from '../lib/color-palettes.js';

let oPalette;
let btnPalette;
let bRotate; 

function setup() {
  createCanvas(windowWidth*0.99, windowHeight*0.75);
  pickPalette()
  bRotate = true;
  nCols = ceil(width/triSize);
  nRows = floor(height/triSize);

  drawTriGrid();
  frameRate(5);
}

function pickPalette() {
  oPalette =  random(arPalettes);
  document.getElementById('info').innerHTML = "Palette de couleur : <i>"+oPalette.name+"</i>";
}

function windowResized() {
  resizeCanvas(0.99*windowWidth, 0.75*windowHeight);
  nCols = round(width/triSize);
  nRows = round(height/triSize);

  drawTriGrid();
  txtSize = floor(map(width, 300, 1600, 12, 30));
} 

function draw() {
    // Nothing goin on in there
}

function setTriGradient(cx, cy, radius, c1, c2) {
  noFill();
  let x1 = cx + cos(TWO_PI / 3) * radius;
  let y1 = cy + sin(TWO_PI / 3) * radius;
  let x2 = cx + radius;
  let y2 = cy;
  let x3 = x1;
  let y3 = cy - sin(TWO_PI / 3) * radius;
  let h_w = 0.5*(x2-x1);
  // Left to right gradient
  push()
  translate(-h_w-x1,0)
  rotate(bRotate * random([0,1,2]) * TWO_PI/3)
  for (let x = x1; x <= x2; x++) {
    let inter = map(x, x1, x2, 0, 1);
    let h = map(x, x1, x2, y3 - y2, 0);
    let c = lerpColor(c1, c2, inter);
    stroke(c);
    line(x, cy - h, x, cy + h);
  }
  pop()
}

function mousePressed() {
  pickPalette();
  drawTriGrid();
  // bRotate = !bRotate
}


function drawTriGrid() {
    // Grid 
    yOffset = triSize ;
    for (let y = 0; y < nRows; y++) {
      yRow = y*0.8825*triSize;
      xOffset = 0.5*triSize; // 0; // 
      aOffset = 180 * (y%2)
      
      for (let x = 0; x < nCols; x++) {
        // c1 = color(random(0,150), random(0,255),random(0,125)); // color(200,0,0); // 
        // c2 = color(random(128,255),random(0,128), random(0,250)); // color(0,0,200); // 
        c1 = color(random(oPalette.palette))
        c2 = color(random(oPalette.palette))
        
        push()
          translate(xOffset + x*1.5*triSize, yRow + yOffset);        
          rotate(PI * (x%2 + y%2));
          translate((x%2)*triSize)
          setTriGradient(0, 0, triSize, c1, c2);
        pop()
      }
    }
}


function draw_test_poly() {
  // Test de placement des triangles
  c1 = color(random(0,200), random(58,255),random(0,250)); // color(200,0,0); // 
  c2 = color(random(128,255),random(0,128), random(0,250)); // color(0,0,200); // 
  push()
  for (let i = 1; i < 6; i++) {
    push()
    translate(i*1.5*triSize,2*triSize)
    rotate(PI*(i%2))
    setTriGradient(0, 0, triSize, c1, c2);
    pop()
  }
  pop()
}