---
title: Inspirational tilings
description: Laisser vous porter par ces pavages de triangles aux couleurs douces.
image: triangles-gradient.jpg
category: visual
tags: [generative, art]
---

# Exploring triangle tilings

A colorful inspirational sketch click and watch. 
The starting point for this sketch is a great [codepen](https://codepen.io/smlsvnssn) from swedish artist/coder Samuel Svensson. I spent a great time on his [blog](https://blog.lhli.net/). There is a nice work on colours palette and [some tools](https://lokeshdhakar.com/projects/color-thief/) to get them.