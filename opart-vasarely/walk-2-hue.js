// 2 HUEs rectangles walking in different direction
const lRect = 30;
let hue1;
let hue2;
let counter = 0;
let nCols, nRows;
let iCols, iRows;
let bShape;
function setup() {
    myCanvas = createCanvas(0.99*windowWidth, 0.7*windowHeight);
    rectMode(CENTER);
    colorMode(HSB, 100);
    bShape = false;
    noStroke();
    mousePressed();
    nCols = ceil(width/lRect);
    nRows = ceil(height/lRect);
    iCols = 0;
    iRows = 0;
    counter = 0;
    background(0, 0, 100);
    
}
  
function draw() {
    // HU1 External
    fill(color((hue1+2*counter)%100, 75, (100 - abs(50-counter%100))));
    rect(lRect*(iCols+0.5), lRect*(iRows+0.5), lRect, lRect);
    fill(color((hue2+3*counter)%100, 100, 100));
    rect(lRect*(iCols+0.5), lRect*(iRows+0.5), 0.75*lRect, 0.75*lRect);

    // Update position
    if (iCols>=nCols) { 
        iCols = 0; 
        iRows++;
    } else { iCols++ }
    if (iRows>=nRows) { iRows = 0; }
    counter++;

    // Update inner shape

}

function mousePressed() {
    hue1 = floor(random(100));
    hue2 = floor(random(100));
    counter = 0;
} 
