---
title: Vasarely fantasy
description: Fantaisie autour des géométries colorées de Vasarely
image: opart-vasa.jpg
category: code
publish: true
tags: [generative, art, visual, audio]
---

# Always start with a blank page 

Pour démarrer avec une page blanche, je recopie ce dossier et le renomme. 

Utiliser la commande suivante pour copier un dossier dans la console de studio code ou autre (l'option /E assure la copie des sous-dossiers et /I considère la destionation comme un dossier et le crée si besoin).  

```
xcopy.exe /E/I empty-example new-example
```
