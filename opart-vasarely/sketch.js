let nRect = 10;
let black;
let arCoul;

function setup() {
    myCanvas = createCanvas(0.99*windowWidth, 0.7*windowHeight);
    // myCanvas.mouseWheel(addCones);  
    black = color(5,5,5);
    arCoul = Array(nRect);
    colorMode(HSB, 100);
    mousePressed();
}
  
function draw() {
    // Draw some points
    background(0, 0, 100);
    
    noStroke();

    // Outer cone
    let hue = 150;
    rectMode(CENTER);

    let dim = min(0.4*width, 0.8*height);
    for (let idx = nRect; idx > 0; idx--) {
        // rect
        fill(idx % 2?black:arCoul[idx-1]); 
        rect(0.5*width-0.6*dim, 0.5*height, idx/nRect*dim, idx/nRect*dim)
        fill(idx % 2?arCoul[nRect-idx]:black); 
        rect(0.5*width+0.6*dim, 0.5*height, idx/nRect*dim, idx/nRect*dim)
    }   
}

function mousePressed() {
    let hStart = floor(random(100));
    for (let i=0; i<nRect; i++) {
        arCoul[i] = color((hStart+4*i)%100, 75, 100);
    } 
}
