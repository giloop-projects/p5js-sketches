let myCanvas; 
let font;
let fSize = 30;
let img;
const elMessage = document.getElementById('monMessage');
let xText, yText;
let monTexte;

document.getElementById('BalanceTonPanneau').addEventListener('submit', (e) => {
  e.preventDefault();
});

function preload() {
  font = loadFont('../assets/fonts/mnicmp-Light.otf');
  // font = loadFont('../assets/fonts/zx_spectrum-7.ttf');
  // La zone du panneau est x:221px y:78 largeur:314 hauteur:314;
  yText = 100;
  img = loadImage('data/panneau.jpg');
}

function setup() {
  myCanvas = createCanvas(736, 1024);
 textFont(font)
 textSize(fSize);
 background(56);
 fill(255);
 noStroke();
 imageMode(CENTER);
 monTexte = 'BONJOUR !\nEntrer un\nmessage dans la\nzone de texte';
 changeTextAlign()
}

function draw() {
  // put drawing code here
 image(img, width / 2, height / 2);
 fill(255,128,0,220);
 noStroke();
 text(monTexte, xText, yText, 305, 305);

}

function changeTextAlign() {
  if (document.getElementById('centerMessage').checked) {
    textAlign(CENTER, TOP);
    xText = 230;
  } else {
    textAlign(LEFT, TOP);
    xText = 230;
  }
}

function updateText() {
  monTexte = elMessage.value;
}

function saveImage() {
  d = new Date();
  saveCanvas(myCanvas, 'Panneau Tinténiac le '+d.toISOString().replaceAll(':','').slice(0,17), 'jpg');
}

elMessage.oninput = updateText;