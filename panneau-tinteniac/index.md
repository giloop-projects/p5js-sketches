---
title: Panneau Municipal
description: Ce sketch simule l'écriture sur un panneau d'information de votre ville. Citoyen c'est à toi ! Est-ce que tu seras inspiré ? 
image: data/panneau-gilou-loves-you.jpg
category: visual
tags: [hack, citoyen, democratie]
---

# Le panneau de votre ville devient citoyen !

Grace à cette page web, le panneau est à vous, vous pouvez maintenant 
envoyer vos plus beaux messages d'amour à qui vous voulez, et pourquoi pas la mairie de votre ville ?
