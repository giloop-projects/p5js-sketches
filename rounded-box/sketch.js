let palette = [{"name":"Imperial Red","hex":"e63946","rgb":[230,57,70],"cmyk":[0,75,70,10],"hsb":[355,75,90],"hsl":[355,78,56],"lab":[52,66,34]},{"name":"Honeydew","hex":"f1faee","rgb":[241,250,238],"cmyk":[4,0,5,2],"hsb":[105,5,98],"hsl":[105,55,96],"lab":[97,-5,5]},{"name":"Powder Blue","hex":"a8dadc","rgb":[168,218,220],"cmyk":[24,1,0,14],"hsb":[182,24,86],"hsl":[182,43,76],"lab":[84,-16,-6]},{"name":"Celadon Blue","hex":"457b9d","rgb":[69,123,157],"cmyk":[56,22,0,38],"hsb":[203,56,62],"hsl":[203,39,44],"lab":[49,-8,-24]},{"name":"Prussian Blue","hex":"1d3557","rgb":[29,53,87],"cmyk":[67,39,0,66],"hsb":[215,67,34],"hsl":[215,50,23],"lab":[22,3,-23]}]
let alphaTarget; 
let alphas;
let x,y,t;
let nBoxes = 12;
let drawShape;

function setup() {
  // put setup code here
  let cnv = createCanvas(750,750);
  cnv.mousePressed(changeShape);
  background(240);
  alphas = []; alphaTarget = 0; t = 0;
  for (let i = 0; i < nBoxes; i++) {
    alphas.push(0)
  }
  drawShape = drawRect;
  t = 0;
}

function draw() {
  background(230,230,250,100)
  let col;
  let smax = 0;
  t += 0.01;
  alphaTarget = TWO_PI*noise(t); // += 0.01; // 
  
  push()
  translate(width/2, height/2)
  
  for (let i = 0; i < nBoxes; i++) {
    const p = palette[i%palette.length];
    col = color(...p['rgb']);
    fill(col);
    noStroke();
    let s = 0.5*min(width-(0.8*width)*i/nBoxes, height-(0.8*height)*i/nBoxes)
    smax = smax<s?s:smax;
    let deltaMouse = 0.45*(smax-s);
    let dx =  map(mouseX, 0, width, -deltaMouse, deltaMouse, true);
    let dy =  map(mouseY, 0, height, -deltaMouse, deltaMouse, true);
    // The smaller the box, the higher the damping factor
    alphas[i] += (alphaTarget - alphas[i]) * map(i, 0, nBoxes, 0.15, 0.025);

    push()
    rotate(alphas[i])
    // Keep track of mouse woth rotation matrix transform
    drawShape(s,(dx*cos(-alphaTarget)-dy*sin(-alphaTarget)), (dx*sin(-alphaTarget)+dy*cos(-alphaTarget)))
    // drawShape(s, dx, dy)
    pop()
  }
  pop()
  
}

function changeShape() {
  if (drawShape == drawRect) {
    drawShape = drawCircle;
  } else if(drawShape == drawCircle) {
    drawShape = drawEllipse;
  } else {
    drawShape = drawRect;
  }
}

function drawRect(s,dx,dy) {
  rect(-0.5*s+dx, -0.5*s+dy,s , s, 0.2*s);
}
function drawCircle(s, dx, dy) {
  circle(dx, dy, s);
}

function drawEllipse(s, dx, dy) {
  ellipse(dx, dy, 0.8*s , 1.2*s);
}