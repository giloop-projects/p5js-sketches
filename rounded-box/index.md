---
title: L'arrondi pointe son nez
description: Une fonction pour dessiner des formes polygonales avec les angles arrondis. Ça manquait, c'est chose faite ! 
image: rounded-box.jpg
category: drawning
tags: [generative, art, visual]
---

# L'arrondi pointe son nez

Test de reproduction d'un sketch vu sur Paper.js, lui-même sans doute recopié d'un 
sketch openprocessing. La boucle est bouclée, et le rendu visuel est plutôt réussi. 
