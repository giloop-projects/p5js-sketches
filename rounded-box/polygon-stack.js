const palette1 = [{ "name": "Imperial Red", "hex": "e63946", "rgb": [230, 57, 70], "cmyk": [0, 75, 70, 10], "hsb": [355, 75, 90], "hsl": [355, 78, 56], "lab": [52, 66, 34] }, { "name": "Honeydew", "hex": "f1faee", "rgb": [241, 250, 238], "cmyk": [4, 0, 5, 2], "hsb": [105, 5, 98], "hsl": [105, 55, 96], "lab": [97, -5, 5] }, { "name": "Powder Blue", "hex": "a8dadc", "rgb": [168, 218, 220], "cmyk": [24, 1, 0, 14], "hsb": [182, 24, 86], "hsl": [182, 43, 76], "lab": [84, -16, -6] }, { "name": "Celadon Blue", "hex": "457b9d", "rgb": [69, 123, 157], "cmyk": [56, 22, 0, 38], "hsb": [203, 56, 62], "hsl": [203, 39, 44], "lab": [49, -8, -24] }, { "name": "Prussian Blue", "hex": "1d3557", "rgb": [29, 53, 87], "cmyk": [67, 39, 0, 66], "hsb": [215, 67, 34], "hsl": [215, 50, 23], "lab": [22, 3, -23] }]
const palette2 = [{ "name": "Dark Slate Blue", "hex": "54478c", "rgb": [84, 71, 140], "cmyk": [40, 49, 0, 45], "hsb": [251, 49, 55], "hsl": [251, 33, 41], "lab": [35, 23, -37] }, { "name": "Sapphire Blue", "hex": "2c699a", "rgb": [44, 105, 154], "cmyk": [71, 32, 0, 40], "hsb": [207, 71, 60], "hsl": [207, 56, 39], "lab": [43, -3, -32] }, { "name": "Blue Munsell", "hex": "048ba8", "rgb": [4, 139, 168], "cmyk": [98, 17, 0, 34], "hsb": [191, 98, 66], "hsl": [191, 95, 34], "lab": [53, -21, -24] }, { "name": "Keppel", "hex": "0db39e", "rgb": [13, 179, 158], "cmyk": [93, 0, 12, 30], "hsb": [172, 93, 70], "hsl": [172, 86, 38], "lab": [66, -43, 0] }, { "name": "Medium Aquamarine", "hex": "16db93", "rgb": [22, 219, 147], "cmyk": [90, 0, 33, 14], "hsb": [158, 90, 86], "hsl": [158, 82, 47], "lab": [78, -61, 23] }, { "name": "Light Green", "hex": "83e377", "rgb": [131, 227, 119], "cmyk": [42, 0, 48, 11], "hsb": [113, 48, 89], "hsl": [113, 66, 68], "lab": [82, -49, 44] }, { "name": "Inchworm", "hex": "b9e769", "rgb": [185, 231, 105], "cmyk": [20, 0, 55, 9], "hsb": [82, 55, 91], "hsl": [82, 72, 66], "lab": [86, -34, 56] }, { "name": "Corn", "hex": "efea5a", "rgb": [239, 234, 90], "cmyk": [0, 2, 62, 6], "hsb": [58, 62, 94], "hsl": [58, 82, 65], "lab": [91, -15, 68] }, { "name": "Maize Crayola", "hex": "f1c453", "rgb": [241, 196, 83], "cmyk": [0, 19, 66, 5], "hsb": [43, 66, 95], "hsl": [43, 85, 64], "lab": [81, 4, 61] }, { "name": "Sandy Brown", "hex": "f29e4c", "rgb": [242, 158, 76], "cmyk": [0, 35, 69, 5], "hsb": [30, 69, 95], "hsl": [30, 86, 62], "lab": [72, 24, 54] }];


let alphaTarget;
let alphas;
// Array of polygons
const nRows = 2;
const nCols = 2;
const nBoxes = 8;
let sidesOffset = 2;

function setup() {
    // put setup code here
    let cnvSize = min(windowWidth*0.95,700)
    let cnv = createCanvas(cnvSize,cnvSize)
    cnv.mousePressed(updateSides)
    background(240);
    alphas = []; alphaTarget = 0; t = 0;
    for (let i = 0; i < nBoxes; i++) {
        alphas.push(0)
    }
    t = 0;
}

function draw() {
    background(230, 230, 250, 100)
    // Update rotationnal noise 
    t += 0.01;
    alphaTarget = TWO_PI * noise(t); // += 0.01; // 
    for (let i = 0; i < alphas.length; i++) {
        // The smaller the box, the higher the damping factor
        alphas[i] += (alphaTarget - alphas[i]) * map(i, 0, nBoxes, 0.15, 0.025);
    }
    let colW = width / nCols;
    let colH = height / nRows;
    let s = min(0.3 * width / nCols, 0.3 * height / nRows);
    textSize(32)
    fill(color(...palette1[2]['rgb'])); noStroke();
    text('ROUNDED POLYGONS', 0.3 * width, height-1)
    fill(color(...palette1[3]['rgb']));
    push()
    translate(width,height)
    rotate(-HALF_PI)
    textSize(26)
    text('by Giloop', 36, -10)
    textSize(12)
    // fill(color(...palette1[2]['rgb'])); noStroke();
    text('try left & right click', height-120, -10)
    pop()
    textSize(18)
    for (let row = 0; row < nRows; row++) {
        for (let col = 0; col < nCols; col++) {
            let sides = (row) * nCols + col + sidesOffset;
            drawStack(colW * (0.5 + col), colH * (row + 0.5), sides, s, nBoxes, palette1)
        }
    }

}


function drawStack(x, y, sides, s, nBoxes = 10, colors = palette1) {

    let smax = s;
    for (let i = 0; i < nBoxes; i++) {
        s = smax * (1 - 0.9 * i / nBoxes); // size reduction for next poly
        let radius = .2 * s;
        const p = colors[i % colors.length];
        col = color(...p['rgb']);
        fill(col);
        noStroke();
        let deltaMouse = 0.45 * (smax - s);
        let dx = map(mouseX - x, -2 * smax, 2 * smax, -deltaMouse, deltaMouse, true);
        let dy = map(mouseY - y, -2 * smax, 2 * smax, -deltaMouse, deltaMouse, true);

        // Keep track of mouse with rotation matrix transform
        polygon(x + dx, y + dy, s, sides, radius, 10, alphas[i])
    }
}

function updateSides(event) { 
    if (mouseButton === LEFT) {
        sidesOffset++; 
    }
    if (mouseButton === RIGHT) {
    sidesOffset = max(2, sidesOffset-1);
    }
    
}