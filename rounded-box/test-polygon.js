// Demo sketch for rounded polygon function

// Array of polygons
const nRows = 4;
const nCols = 4;

function setup() {
    createCanvas(windowWidth*0.95, windowHeight*0.75)
    background(247, 237, 226); // 234, 172, 139); // 
    let colW = width/nCols;
    let colH = height/nRows;
    let size = min(0.3*width/nCols, 0.3*height/nRows);
    // 
    textAlign(CENTER, CENTER);
    fill(53, 80, 112); noStroke();
    textSize(36)
    text('ROUNDED POLYGONS', 0.5*width, 2*colH)
    text('by Giloop', 0.5*width, 3*colH)
    textSize(18)
    for (let row = 0; row < nRows; row++) {
        for (let col = 0; col < nCols; col++) {
            let sides = (row) * nCols + col +2;
            //console.log('Sides:'+sides)
            fill(242, 132, 130);
            stroke(53, 80, 112); 
            strokeWeight(2)
            // Calling polygon function
            polygon(colW*(0.5+col), colH*(row+0.5), size, sides, 10)
            fill(53, 80, 112); noStroke();
            text(sides, colW*(0.5+col), colH*(row+0.5))
        }
    }
    noLoop()
}

function draw() {}
