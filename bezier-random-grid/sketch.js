// Creation et visualisation d'un bruit de Perlin en 2D/3D 
var inc = 0.1;
var t_inc = 0;
var scl = 30;
var cols, rows;
var arPoints;
var arConnects;
let bDrawCtrl = false;

class Connexion {
  // 
  constructor(ptStart, ptEnd, idxStart, idxEnd, scale) {
      // Start / End points coordinates
      this.ptStart = ptStart;
      this.ptEnd = ptEnd;
      // Scale for control points magnitude
      this.scale = scale;
      // Angle of connexion (used to limit control points angle)
      this.heading = createVector(ptStart.x-ptEnd.x, ptStart.y-ptEnd.y).heading()

      // Start/End index in arPoints connexion matrix
      this.idxStart = idxStart;
      this.idxEnd = idxEnd;
      // Random color
      this.color = color(random(255), random(255), random(255));
      // Add noise to control points
      this.offset1 = random()*100;
      this.offset2 = random()*100;
      this.noise1 = createVector(noise(this.offset1, 0), noise(0, this.offset1));
      this.noise2 = createVector(noise(this.offset2, 0), noise(0, this.offset2));

      // Bézier control points
      this.ptCtrl1 = createVector(1,1);
      this.ptCtrl2 = createVector(1,1);

      this.update();
      // createVector(this.ptEnd.x + 5 * random(), this.ptEnd.y + 5 * random());
      
  }

  draw(bDebug = true) {
    if (bDebug) {
      fill(color(0,0,0,128))
      noStroke();
      ellipse(this.ptCtrl1.x, this.ptCtrl1.y, 5, 5)
      ellipse(this.ptCtrl2.x, this.ptCtrl2.y, 5, 5)
      stroke(color(0,0,0,200))
      line(this.ptStart.x, this.ptStart.y, this.ptCtrl1.x , this.ptCtrl1.y)
      line(this.ptEnd.x, this.ptEnd.y, this.ptCtrl2.x, this.ptCtrl2.y)
      // angle limits ...
      stroke(color(50,50,50,50))
      const lim11 = p5.Vector.add(this.ptStart, 
        createVector(1,1).setHeading(this.heading + 0.25).setMag(10) );
      const lim12 = p5.Vector.add(this.ptStart, 
        createVector(1,1).setHeading(this.heading + 6.05).setMag(10) )
      
      line(this.ptStart.x, this.ptStart.y, lim11.x, lim11.y )
      line(this.ptStart.x, this.ptStart.y, lim12.x, lim12.y )

      const lim21 = p5.Vector.add(this.ptEnd, 
        createVector(1,1).setHeading(PI+this.heading - 0.25).setMag(10) );
      const lim22 = p5.Vector.add(this.ptEnd, 
        createVector(1,1).setHeading(PI+this.heading - 6.05).setMag(10) )
      
      line(this.ptEnd.x, this.ptEnd.y, lim21.x, lim21.y )
      line(this.ptEnd.x, this.ptEnd.y, lim22.x, lim22.y )
    }


    noFill();
    stroke(this.color);
    bezier(this.ptStart.x, this.ptStart.y, this.ptCtrl1.x , this.ptCtrl1.y, 
           this.ptCtrl2.x , this.ptCtrl2.y, this.ptEnd.x, this.ptEnd.y);  
  }

  update() {
    this.offset1 += 0.01;
    this.offset2 += 0.01;
    this.noise1 = createVector(noise(this.offset1, 0), noise(0, this.offset1));
    this.noise2 = createVector(noise(this.offset2, 0), noise(0, this.offset2));
    this.ptCtrl1 = p5.Vector.add(this.ptStart, 
    createVector(1,1)
    .setHeading(map(this.noise1.y, 0, 1, this.heading +  0.25, this.heading +  6.05))
    .setMag(map(this.noise1.x, 0, 1, 1, this.scale)) );
    this.ptCtrl2 = p5.Vector.add(this.ptEnd, 
    createVector(1,1)
    .setMag(map(this.noise2.x, 0, 1, 1, this.scale))
    .setHeading(map(this.noise2.y, 0, 1, PI+this.heading - 0.25, PI+this.heading - 6.05) ) );
  }
}

function setup() {
    createCanvas(400, 400);
    cols = floor(width / scl);
    rows = floor(height / scl);
    // arPoints : bool array indicate if point is connected
    arPoints = new Array(cols * rows).map(el => false);
    // arConnects : bezier connexions (Connexion objects)
    arConnects = [];
    
    generateConnexions()
    
    strokeWeight(1)

    fr = createP('');

}

function generateConnexions() {
  for (let l = 0; l < rows; l++) {
    for (let c = 0; c < cols; c++) {
      // Point suivant si déjà connecté
      if (arPoints[getIndex(l,c)]) { continue }
      // Recherche d'une connexion parmi les dispos autour du point
      let arVoisin = [{lig:l-1, col:c-1}, {lig:l, col:c-1}, {lig:l+1, col:c-1}, 
        {lig:l-1, col:c}, {lig:l+1, col:c}, 
        {lig:l-1, col:c+1}, {lig:l, col:c+1}, {lig:l+1, col:c+1}]
        .filter(el => el.lig>=0 && el.lig<rows && el.col>+0 && el.col<rows) // filter outer points
        .map(el => getIndex(el.lig, el.col)) // convert to indices
        .filter(el => !arPoints[el]);        // filter points already connected

      if (arVoisin.length > 0) { 
        // Crée une connexion sur un point aléatoire
        idxConnect = random(arVoisin)
        const lcEnd = getLigneCol(idxConnect)
        arConnects.push(new Connexion(getCoords(l, c, scl), getCoords(lcEnd.l, lcEnd.c, scl), 
        idxConnect, getIndex(l,c), scl))
        arPoints[idxConnect] = true;
        arPoints[getIndex(l,c)] = true;
      }
    } 
  }
}

function getIndex(l,c) { return(c + l*cols) }
function getLigneCol(idx) { return({l: floor(idx/cols), c:idx%cols}) }
function getCoords(l, c, ech) { return (createVector((c+0.5)*ech, (l+0.5)*ech))}
function draw() {
  background(255);
  noStroke();
  fill(0, 0, 255, 100);
  for (let l = 0; l < rows; l++) {
    for (let c = 0; c < cols; c++) {
      const pt = getCoords(l,c, scl)
      ellipse(pt.x, pt.y, 4, 4)    
    } 
  }

  arConnects.map(el => el.update());
  arConnects.map(el => el.draw(bDrawCtrl));

}

function keyPressed() {
  bDrawCtrl = !bDrawCtrl;
}