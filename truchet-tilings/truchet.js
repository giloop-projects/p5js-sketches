
/** Une étude sur les pavages de Truchet 
 * Avec pas mal de petites variations pour manipuler les pavages : 
 * - mode 'Track mouse' : les pavés suivent la souris
 * - clic pour faire tourner les pavages
 * - Noir et blanc ou couleur avec différents modes de gradient (dans le code)
 * @author Gilles Gonon
 * @license GPL
*/

let nRows = nCols = 12;
const margin = 4; // space between tiles
let arPaves = [];
let s;             // computed tile size
let xOff, yOff;    // Center drawning offset
let cMoved,rMoved; // Last tile turned by mouse move
let selGeom, selPattern;       // Popup to choose geometry & patterns
// Define gradient colors
let from, to;
let myCanvas; 
let arP;
const elPattern = document.getElementById('monPattern');


let oPavages = [ {"nom": "Carrés décalés", "motif":"BDAC\nACBD", "tag":"2x4"}, 
{"nom": "Carrelage Pl1-L", "motif":"DADA\nCDAB\nDCBA\nCBCB", "tag":"4x4"}, 
{"nom": "Carrelage Pl7-5", "motif":"DBCBCA\nBDADAC\nACDABD\nBDCBAC\nACBCBD\nCADADB", "tag":"6x6"}, 
{"nom": "Carrelage Pl7-6", "motif":"BCBCBC\nADDAAD\nBDBCAC\nACADBD\nBCCBBC\nADADAD", "tag":"6x6"}, 
{"nom": "Diag Pl1-A", "motif":"DB\nBD", "tag":"2x2"}, 
{"nom": "Diag 2", "motif":"AC\nCA", "tag":"2x2"}, 
{"nom": "Zig Zag Vert", "motif":"AC\nBD", "tag":"2x2"}, 
{"nom": "Zig Zag Horiz", "motif":"DA\nBC", "tag":"2x2"}, 
{"nom": "Hélice Pl1-I", "motif":"CD\nBA", "tag":"2x2"}, 
{"nom": "Diags décalées Pl1-F", "motif":"AC\nCA\nCA\nAC", "tag":"4x2"}, 
{"nom": "Diags décalées 1", "motif":"AAAAADDDDD\nCCCCCBBBBB", "tag":"2x10"}, 
{"nom": "Flêches 1", "motif":"AAAAADDDDD\nBBBBBCCCCC", "tag":"2x10"}, 
{"nom": "Flêches 2", "motif":"A\nB\nD\nC", "tag":"4x1"},
{"nom": "Becs", "motif":"AC\nCB", "tag":"2x2"}, 
{"nom": "Horiz 1", "motif":"DB", "tag":"1x2"}, 
{"nom": "Vert 1", "motif":"D\nB", "tag":"2x1"} ,
{"nom": "Carrés Pl1-G", "motif":"BC\nAD", "tag":"2x2"}, 
{"nom": "Carrés Pl1-M", "motif":"BC\nCB", "tag":"2x2"}, 
{"nom": "Flêches Pl1-M", "motif":"DA\nAD", "tag":"2x2"}, 
{"nom": "Carrés Pl1-H", "motif":"BCDA\nCBAD\nDABC\nADCB", "tag":"4x4"}, 
{"nom": "Carrés Pl1-T", "motif":"BCDA", "tag":"1x4"}, 
{"nom": "Carrés Pl1-V", "motif":"DADABCBC\nBCBCDADA", "tag":"2x8"}, 
{"nom": "Zig zag Pl2-Q", "motif":"DDAA\nBBCC", "tag":"2x4"},
{"nom": "Zig zag Pl3-Da", "motif":"ADBC\nDBCA", "tag":"2x4"}, 
{"nom": "Zig zag Pl3-Fa", "motif":"ADB\nCAD", "tag":"2x3"},
{"nom": "S Pl3-Oa", "motif":"BC\nBD\nAD", "tag":"3x2"}, 
{"nom": "Tresse Pl6-Ob", "motif":"BAC\nACD", "tag":"2x3"},
{"nom": "Pl5-Gb", "motif":"BDAC\nCADB", "tag":"2x4"}, 
]

document.getElementById('UnPaveDansLaMarre').addEventListener('submit', (e) => {
    e.preventDefault();
  });

function setup() {
    if (document.URL.search('truchet-svg')>0) {
        myCanvas = createCanvas(0.99*windowWidth, 0.7*windowHeight, SVG);
    } else {
        myCanvas = createCanvas(0.99*windowWidth, 0.7*windowHeight);
    }

    let uiOffsetY = 158;
    selGeom = document.getElementById('selGeom');
    selPattern = document.getElementById('selPattern');
    cbMouse = document.getElementById('cbMouse');
    cbColor = document.getElementById('cbColor');
  
    // Ajout des options de pavages au menu déroulant
    for (let p of oPavages) {
        var opt = document.createElement('option');
        opt.value = p.motif;
        opt.innerHTML = p.nom + " " + p.tag;
        selPattern.appendChild(opt);
    }
    // Gradient colors
    from = color(200,20,202);
    to = color(0, 0, 239);
    
    // Create tiles
    let geom = selGeom.value
    // Grid size
    s = min(0.9*width, 0.9*height-80) / nRows;
    xOff = 0.5*(width-s*nRows);
    yOff = 0.5*(height-s*nRows);
    
    for (let i = 0; i < nRows*nCols; i++) {
        // 
        let ds = defineStyle(i,'vertical'); // 'radial'); // 'diagonal'); //       
        arPaves.push(new PaveTruchet(geom, ds))
    }
    
    fill(0);
    stroke(0);
    
}
  
function draw() {
    background(240)
    
    for (let r = 0; r < nRows; r++) {
       for (let c = 0; c < nCols; c++) {
           const idx = r*nCols + c;
           // console.log(idx)
           arPaves[idx].draw(xOff+c*s, yOff+r*s, s-margin)
       } 
    }
    noLoop()
}


class drawStyle {
    constructor(fillc, strokec, strokew=1, bFill=true) {
        this.fillColor = fillc;
        this.strokeColor = strokec;
        this.strokeWeight = strokew;
        this.bFill = bFill;
    }
    apply() {
        if (this.bFill == true) {
            fill(this.fillColor);
        } else { noFill(); }
        if (this.strokeWeight > 0) {
            stroke(this.strokeColor);
            strokeWeight(this.strokeWeight);
        } else { noStroke(); }
    }
}

class PaveTruchet {
    constructor(geom, style=undefined) {
        this.geom = geom;
        this.orientation = random([0, 1, 2, 3]);
        this.pos = createVector(0,0);
        this.style = style;
    }

    draw(x,y,s) {
        // save pos
        this.pos.x = x;  this.pos.y = y;
        push()
        // Apply style if given
        if (this.style) { this.style.apply(); }
        translate(x+0.5*s,y+0.5*s)
        // Apply rotation
        push() 
        rotate(this.orientation*HALF_PI)
        push()
        translate(-0.5*s,-0.5*s)

        if (this.geom==="mixed") {
            this.geom = random(["arc","arrow","fox","corner","smith","stairs","L","N","triangle"]);
        }      

        if (this.geom ==="arc") {
            arc(0,0,2*s,2*s,0,HALF_PI,PIE);
        } else if (this.geom ==="arrow") {
            beginShape()
            vertex(0,0);
            vertex(0.5*s,0);
            vertex(s,0.5*s);
            vertex(0.5*s,s);
            vertex(0,0.5*s);
            endShape(CLOSE);
        } else if (this.geom ==="smith") {
            noFill(); strokeWeight(4)
            arc(0,0,s,s,0,HALF_PI);
            arc(s,s,s,s,PI,PI+HALF_PI);
            //arc(s,s,0.5*s,0.5*s,0,PI+HALF_PI,0);
        } else if (this.geom ==="fox") {
            beginShape()
            vertex(0,0);
            vertex(0.75*s,0);
            vertex(s,0.5*s);
            vertex(0.5*s,0.5*s);
            vertex(0.5*s,s);
            vertex(0,0.75*s);
            endShape(CLOSE);
        } else if (this.geom ==="corner") {
            beginShape()
            vertex(0,0);
            vertex(s,0);
            vertex(s,0.5*s);
            vertex(0.5*s,0.5*s);
            vertex(0.5*s,s);
            vertex(0,s);
            endShape(CLOSE);
        } else if (this.geom ==="stairs") {
            beginShape()
            vertex(0,0);
            vertex(s,0);
            vertex(0.5*s,0);
            vertex(0.5*s,0.5*s);
            vertex(0,0.5*s);
            vertex(0,s);
            endShape(CLOSE);
        } else if (this.geom ==="N") {
            beginShape()
            vertex(0,0);
            vertex(0,s);
            vertex(s,s);
            vertex(s,0.5*s);
            vertex(0.5*s,0.5*s);
            vertex(0.5*s,s);
            endShape(CLOSE);
        } else if (this.geom ==="L") {
            beginShape()
            vertex(0,-0);
            vertex(0,s);
            vertex(s,s);
            vertex(0.5*s,s);
            vertex(0.5*s,0.5*s);
            vertex(0.5*s,0);
            endShape(CLOSE);
        } else  { // original Triangle shape
            triangle(0,0,0,s,s,0)
        }
        pop()
        pop()
        pop()
    }

    turn(orientation=undefined) {
        if (orientation in [0,1,2,3]) {
            this.orientation = orientation;
        } else {
            this.orientation = (this.orientation+1)%4;
        }
            
    }

}

function mouseMoved() {
    if (cbMouse.checked) {
        // Orientation follow mouse
        let orient = 0;
        for (p of arPaves) {
            orient = p.orientation;
            if (mouseX<=p.pos.x+s-margin && mouseY<=p.pos.y) { orient = 0;}
            else if (mouseX>p.pos.x+s-margin && mouseY<=p.pos.y) { orient = 1;}
            else if (mouseX>p.pos.x+s-margin && mouseY>p.pos.y+s-margin) { orient = 2;}
            else if (mouseX<=p.pos.x && mouseY>p.pos.y+s-margin) { orient = 3;}
            // Otherwise keep actual orientation
            p.turn(orient);
        }
        draw()
    } /*else {
        // Mouse hover changes orientation
        let colClicked = floor(map(mouseX-xOff, 0, s*nCols, 0, nCols));
        let rowClicked = floor(map(mouseY-yOff, 0, s*nRows, 0, nRows));
        if (cMoved == colClicked && rMoved == rowClicked) {
            return; // Turn tile only once on mouse move
        }
        if (colClicked >= 0 && colClicked<nCols && rowClicked >= 0 && rowClicked<nRows) {
            const idx = rowClicked*nCols + colClicked;
            arPaves[idx].turn();
            draw();
            
        }
        cMoved = colClicked;
        rMoved = rowClicked;    
    }*/
}

// function click() {
function mousePressed() {
    // idx = r*nCols + c;
    let colClicked = floor(map(mouseX-xOff, 0, s*nCols, 0, nCols));
    let rowClicked = floor(map(mouseY-yOff, 0, s*nRows, 0, nRows));
    //let txtInfo = 'colClicked:'+colClicked+', rowClicked:'+rowClicked;
    if (colClicked >= 0 && colClicked<nCols && rowClicked >= 0 && rowClicked<nRows) {
        const idx = rowClicked*nCols + colClicked;
        arPaves[idx].turn();
        draw();
        //console.log(txtInfo)
    } else {
        //console.log('Outside tiles grid x:'+mouseX+',y:'+mouseY+','+txtInfo)
    }
}


function randomOrient() {
    cbMouse.checked = false; // Untick checkbox
    // Random Geom 
    let selVal = floor(random(selGeom.options.length));
    selGeom.value = selGeom.options[selVal].value
    // Random orientations
    for (p of arPaves) {
        p.turn(random([0,1,2,3]));
    }
    // update
    changeGeom()
}

function geometricOrient(symetry) {
    let idxC, idxR;
    let orient;
    for (let i = 0; i < arPaves.length; i++) {
        // i = idxR*nCols + idxC;
        idxC =  i % nCols;
        idxR = floor(i/nRows);
        console.log('col:'+idxC+', row:'+idxR)
        if (symetry==='horiz') {
            orient = 2*(idxC % 2)+1;
            //orient = idxC % 4;
        } else if (symetry==='horiz2') {
            orient = (((i+idxR%2)%2)?0:2);
        } else if (symetry==='vert') {
            // orient = 2*(idxR % 2)+1;
            orient = (idxR+1) % 4;
        } else if (symetry==='quad1') {
            orient = (i%4); // (idxC % 2) +2*(idxR % 2)+1 ;
        } else if (symetry==='quad2') {
            orient = idxR%2?(i%2):3-(i%2); // (idxC % 2) +2*(idxR % 2)+1 ;
        } else if (symetry==='random') {
            orient = random([0,1,2,3]);
        }
        arPaves[i].turn(orient);
    }
    draw();
}


function changeGeom() {
    let geom = selGeom.value
    for (let i = 0; i < nRows*nCols; i++) {
        arPaves[i].geom = geom;
    }
    draw();
}

function changeColor() {
    for (let i = 0; i < nRows*nCols; i++) {
        if (cbColor.checked) {arPaves[i].style = defineStyle(i, 'vertical');} 
        else {arPaves[i].style = undefined;}
    }
    draw();
}

function defineStyle(i, style) {
    let interpVal;
    switch (style.toLowerCase()) {
        case 'vertical':
            // Vertical color gradient
            interpVal =  i/(nRows*nCols); 
            break;
        case 'radial':
            // Radial color gradient 
            interpVal = 1 - (1.4*sqrt((i%nCols - 0.5*nCols)**2 + (floor(i/nRows) - 0.5*nRows)**2))/nRows
            break;
        case 'diagonal':
            // Diagonal
            interpVal = 1 - sqrt(((i%nCols)/nCols)**2 + (floor(i/nRows)/nRows)**2 )/(1.42)
            break;
        default:
            interpVal =  i/(nRows*nCols); 
        break;
    }
    let col = lerpColor(from, to, interpVal)
    return new drawStyle(col, col, 1, true);
}

function saveImage() {
    let d = new Date();
    if (document.URL.search('truchet-svg')>0) {
        save(); // Canvas(myCanvas, 'Pavage Truchet -'+d.toISOString().replaceAll(':','').slice(0,17), 'svg');
    } else {
        saveCanvas(myCanvas, 'Pavage Truchet -'+d.toISOString().replaceAll(':','').slice(0,17), 'png');
    }
}


function updatePattern() {
    let monPattern = elPattern.value;
    let idxC, idxR;
    let orient;
    arP = monPattern.split('\n')
    const mapAToO = {"A":3, "B":0, "C":1, "D":2}
    let nRowsP = arP.length
    let nColsP = arP[0].length
    for (let i = 0; i < arPaves.length; i++) {
        // i = idxR*nCols + idxC;
        idxC =  i % nCols;
        idxR = (i-idxC)/nCols;
        orient = mapAToO[arP[idxR%nRowsP][idxC%nColsP]];
        arPaves[i].turn(orient);
    }
    draw();
  }

elPattern.oninput = updatePattern;

function changePattern() {
    let pattern = selPattern.value
    elPattern.value = pattern;
    updatePattern();
}


