---
title: Pavages de Truchet
description: > 
    Variations sur le thème des pavages de Truchet avec 1/4 de rotation des élements.
    Plein d'options : éditeur de pattern, aléatoire, motifs différents, suivi de la souris ...
image: images/truchet-0.jpg
category: op-art
tags: [generative, art, visual]
---

# Always start with a blank page 

Pour démarrer avec une page blanche, je recopie ce dossier et le renomme. 

Utiliser la commande suivante pour copier un dossier dans la console de studio code ou autre (l'option /E assure la copie des sous-dossiers et /I considère la destionation comme un dossier et le crée si besoin).  

```
xcopy.exe /E/I empty-example new-example
```
