var fcn_proba;
let img, img2; // Black white images, same sizes
let sel; // select popup
let count_pt; // Count drawn points

const max_pt = 50000;
function preload() {
  img = loadImage('rose-bw.jpg');
  img2 = loadImage('gg.jpg');
}

function setup() {
  img.loadPixels();
  createCanvas(img.width, img.height);
  stroke('#444');
  strokeWeight(1);
  background(230); 
  sel = createSelect();
  sel.option('X Plane');
  sel.option('XY plane');
  sel.option('Image density');
  sel.option('Image 2 density');
  
  sel.changed(selectProba);
  selectProba();
  fcn_proba = x_plane;
}

function gen_random_array(len, vMax = 1) {
  return(Array.from({length: len}, () => Math.floor(Math.random()*vMax)));
}

function draw() {
  if (count_pt<max_pt) {
    // Génère des points
    const n = 500;
    const x = gen_random_array(n, width);
    const y = gen_random_array(n, height); // 
    const p = Array.from({length: n}, () => Math.random()); // proba
    
    // Récupère dans un tableau les points échantillonnés 
    for (let idx = 0; idx < n; idx++) {
      if (fcn_proba(x[idx],y[idx])>p[idx]) {
        point(x[idx], y[idx]);  
        count_pt++;
      }
    }  
  }
   
  // text(floor(frameRate()), 8, height)
}

// 
function x_plane(x,y) {
  // A plane in x,y direction
  return(x/width);
}
function xy_plane(x,y) {
  // A plane in x,y direction
  return(0.5*(x/width+y/height));
}

function prob_img(x,y) {
  let pix = img.get(floor(x), floor(y));
  return(1-(pix[0]/255));
}


function prob_img2(x,y) {
  let pix = img2.get(floor(x), floor(y));
  return(1-(pix[0]/255));
}

function selectProba() {
  let item = sel.value();
  if (item=='X Plane') {
    fcn_proba = x_plane;
  } else if (item=='XY plane') {
    fcn_proba = xy_plane;
  } else if (item=='Image density') {
    fcn_proba = prob_img;
    strokeWeight(2);
  } else if (item=='Image 2 density') {
    fcn_proba = prob_img2;
    strokeWeight(1);
  }
  background(250);
  count_pt = 0; // réinit point counter
}