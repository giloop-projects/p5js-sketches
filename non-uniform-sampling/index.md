---
title: Non-uniform Sampling
description: Générer des points dans le plan selon des densités variés, c'est pas si compliqué !
image: image.jpg
category: visual
publish: false
tags: [randomness, visual]
---

# Always start with a blank page 

Pour démarrer avec une page blanche, je recopie ce dossier et le renomme. 

Utiliser la commande suivante pour copier un dossier dans la console de studio code ou autre (l'option /E assure la copie des sous-dossiers et /I considère la destionation comme un dossier et le crée si besoin).  

```
xcopy.exe /E/I empty-example new-example
```
