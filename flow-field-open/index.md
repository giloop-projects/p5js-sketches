---
title: Rectangles Floating on Perlin Noise
description: >
 Des rectangles qui flottent au rythme d'un bruit de Perlin. 
 Une reprise inspirée du visuel du site OpenAI.
image: flow-field-open.png
category: visual
publish: true
tags: [generative, art, visual]
---

# Les champs de force vectoriel

Perdu dans un champs de bruit de Perlin.