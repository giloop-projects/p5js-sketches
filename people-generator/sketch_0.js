let him;
let nPose = 0;
let fun = "drawDebug";

function setup() {
  createCanvas(400,400);

  him = new Someone(color(20,20,20), body_height = height/4, body_width = height/8)
  him.pos.x = width/2;
  him.pos.y = height/2;
  
}

function draw() {
  // put drawing code here
  background(255);
  
  let t = map(mouseX, 0, width, -2, 2); // 2; // 
  curveTightness(t); 
  him[fun]();
}

function mouseReleased() {
  switch (nPose) {
    case 1:
      him.shake()
      break;
    case 2:
      him.bigShake()
      break;
      
    default:
      him.rest()
      break;
  }
  
  nPose = (nPose + 1) % 3;
}

function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    fun = "drawDebug";
  } else if (keyCode === RIGHT_ARROW) {
    fun = "drawSmooth";
  }
}