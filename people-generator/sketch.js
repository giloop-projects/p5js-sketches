let him;
let arBodies = [];
let N = 16;
let nPose = 0;
let fun = "draw";

function setup() {
  myCanvas = createCanvas(0.99*windowWidth, 0.7*windowHeight);
  for (let element of document.getElementsByClassName("p5Canvas")) {
    element.addEventListener("contextmenu", (e) => e.preventDefault());
  }

  let h = min(width,height) / 10
  let w = h / 3
  for (let idx = 0; idx < N; idx++) {

    him = new Someone(color(random(0,255),random(50,200),random(100,200)), body_height = h, body_width = w)
    him.pos.x = 1.5*h+((width-3*h)/8*(floor(idx/4)%2))+width/4.4*(idx%4);
    him.pos.y = h+height/4.2*(floor(idx/4));
    arBodies.push(him);
  }
  
}

function draw() {
  // put drawing code here
  background(255);
  let t = map(mouseX, 0, width, -5, 5); // 2; // 
  curveTightness(t); 
  for (him of arBodies) {
    him[fun]();
  }
}

function mousePressed() {
  arBodies.map(el => el.rest())
}

function mouseReleased() {
  switch (nPose) {
    case 1:
      arBodies.map(el => el.shake())
      break;
    case 2:
      arBodies.map(el => el.bigShake())
      break;
      
    default:
      arBodies.map(el => el.rest())
      break;
  }
  
  nPose = (nPose + 1) % 3;
}

function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    fun = "drawDebug";
  } else if (keyCode === RIGHT_ARROW) {
    fun = "drawSmooth";
  }
}