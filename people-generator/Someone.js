class Someone {
  constructor(fillc, body_height = 50, body_width = 10) {
    this.color = fillc;
    this.pos = createVector(0, 0)
    this.bodyHeight = body_height / 1.25;
    this.bodyWidth = body_width;

    // Resting params
    this.headSize = body_height / 2.6;
    this.headAngle = random(-0.2, 0.2);
    this.neckSize = body_height / 12;
    this.neckAngle = random(-0.2, 0.2);
    // Body 2 segments with angle
    this.bodyUpAngle = random(-0.2, 0.2);
    this.bodyDownAngle = random(-0.2, 0.2);
    // shoulder & hips
    this.shoulderWidth = 1.5 * body_width;
    this.hipsWidth = 0.9 * body_width;
    // arms
    this.armUpSize = body_height / 2.5;
    this.armDownSize = body_height / 2.5;
    this.armRupAngle = random(-0.2, 0.2);
    this.armRdownAngle = random(-0.2, 0.2);
    this.armLupAngle = random(-0.2, 0.2);
    this.armLdownAngle = random(-0.2, 0.2);

    // legs
    this.legUpSize = body_height / 2;
    this.legDownSize = body_height / 2;
    this.legRupAngle = random(-0.2, 0.2);
    this.legRdownAngle = random(-0.2, 0.2);
    this.legLupAngle = random(-0.2, 0.2);
    this.legLdownAngle = random(-0.2, 0.2);
  }

  draw(pos = null) {
    fill(this.color);
    stroke(this.color);
    strokeWeight(2);

    push()
    // Draw upper body
    translate(this.pos.x, this.pos.y)
    push()
    rotate(this.bodyUpAngle)
    line(0, 0, 0, -this.bodyHeight / 2);
    push()
    translate(0, -this.bodyHeight / 2);
    // épaule
    line(-this.shoulderWidth / 2, 0, this.shoulderWidth / 2, 0);
    // Left arm
    push()
    translate(-this.shoulderWidth / 2, 0);
    rotate(this.armLupAngle);
    line(0, 0, 0, this.armUpSize);
    push()
    translate(0, this.armUpSize);
    rotate(this.armLdownAngle);
    line(0, 0, 0, this.armDownSize);
    pop()
    pop()
    // Right arm
    push()
    translate(this.shoulderWidth / 2, 0);
    rotate(this.armRupAngle);
    line(0, 0, 0, this.armUpSize);
    push()
    translate(0, this.armUpSize);
    rotate(this.armRdownAngle);
    line(0, 0, 0, this.armDownSize);
    pop()
    pop()
    // Neck 
    push()
    rotate(this.neckAngle)
    line(0, 0, 0, -this.neckSize)
    // Head
    push()
    translate(0, -this.neckSize - 0.45 * this.headSize)
    rotate(this.headAngle)
    ellipse(0, 0, this.headSize / 2, this.headSize);
    pop()
    pop()
    pop()
    pop()
    // lower body part
    push()
    rotate(this.bodyDownAngle)
    line(0, 0, 0, this.bodyHeight / 2);
    // Hips
    line(- this.hipsWidth / 2, this.bodyHeight / 2, this.hipsWidth / 2, this.bodyHeight / 2);
    // Left leg
    push()
    translate(- this.hipsWidth / 2, this.bodyHeight / 2);
    rotate(this.legLupAngle);
    line(0, 0, 0, this.legUpSize);
    push()
    translate(0, this.legUpSize);
    rotate(this.legLdownAngle);
    line(0, 0, 0, this.legDownSize);
    pop()
    pop()
    // Right leg
    push()
    translate(this.hipsWidth / 2, this.bodyHeight / 2);
    rotate(this.legRupAngle);
    line(0, 0, 0, this.legUpSize);
    push()
    translate(0, this.legUpSize);
    rotate(this.legRdownAngle);
    line(0, 0, 0, this.legDownSize);
    pop()
    pop()

    pop()


    pop()
  }

  drawSmooth(pos = null) {
    // Skeleton parts as vectors
    let vBodyUp = p5.Vector.fromAngle(- HALF_PI + this.bodyUpAngle, this.bodyHeight / 2);
    let vBodyDown = p5.Vector.fromAngle(HALF_PI + this.bodyDownAngle, this.bodyHeight / 2);

    let arPtBody = [p5.Vector.fromAngle(vBodyDown.heading() + 0.5 * (this.bodyUpAngle - this.bodyDownAngle) - HALF_PI, 0.5 * this.hipsWidth),
    p5.Vector.fromAngle(vBodyDown.heading() + 0.5 * (this.bodyUpAngle - this.bodyDownAngle) + HALF_PI, 0.5 * this.hipsWidth)];


    // Upper parts
    let vNeck = p5.Vector.fromAngle(vBodyUp.heading() + this.neckAngle, this.neckSize);
    let arPtNeck = [p5.Vector.add(vBodyUp, vNeck).add(p5.Vector.fromAngle(vNeck.heading() + HALF_PI, 0.5 * this.neckSize)),
    p5.Vector.add(vBodyUp, vNeck).add(p5.Vector.fromAngle(vNeck.heading() - HALF_PI, 0.5 * this.neckSize))];

    let pCenterHead = p5.Vector.add(vBodyUp, vNeck).add(createVector(0, -0.5 * this.headSize));

    let vShoulderLeft = p5.Vector.fromAngle(HALF_PI + vBodyUp.heading(), this.shoulderWidth / 2);
    let pShoulderLeft = p5.Vector.add(vBodyUp, vShoulderLeft);
    let vShoulderRight = p5.Vector.fromAngle(-HALF_PI + vBodyUp.heading(), this.shoulderWidth / 2);
    let pShoulderRight = p5.Vector.add(vBodyUp, vShoulderRight);
    let vArmUpLeft = p5.Vector.fromAngle(this.armLupAngle + HALF_PI, this.armUpSize);
    let pElbowLeft = p5.Vector.add(pShoulderLeft, vArmUpLeft);
    let vArmDownLeft = p5.Vector.fromAngle(vArmUpLeft.heading() + this.armLdownAngle, this.armDownSize);
    let vArmUpRight = p5.Vector.fromAngle(this.legRupAngle + HALF_PI, this.armUpSize);
    let pElbowRight = p5.Vector.add(pShoulderRight, vArmUpRight);
    let vArmDownRight = p5.Vector.fromAngle(vArmUpRight.heading() + this.armRdownAngle, this.armDownSize);

    let arElbowLeft = [p5.Vector.add(pElbowLeft, p5.Vector.fromAngle(vArmUpLeft.heading() - 0.5 * (vArmUpLeft.heading() - vArmDownLeft.heading()) - HALF_PI, 0.05 * this.armUpSize)),
    p5.Vector.add(pElbowLeft, p5.Vector.fromAngle(vArmUpLeft.heading() - 0.5 * (vArmUpLeft.heading() - vArmDownLeft.heading()) + HALF_PI, 0.05 * this.armUpSize))];
    let arElbowRight = [p5.Vector.add(pElbowRight, p5.Vector.fromAngle(vArmUpRight.heading() - 0.5 * (vArmUpRight.heading() - vArmDownRight.heading()) - HALF_PI, 0.05 * this.armUpSize)),
    p5.Vector.add(pElbowRight, p5.Vector.fromAngle(vArmUpRight.heading() - 0.5 * (vArmUpRight.heading() - vArmDownRight.heading()) + HALF_PI, 0.05 * this.armUpSize))];
    let arHandRight = [p5.Vector.add(pElbowRight, vArmDownRight).add(p5.Vector.fromAngle(vArmDownRight.heading() - HALF_PI, 0.05 * this.armUpSize)),
    p5.Vector.add(pElbowRight, vArmDownRight).add(p5.Vector.fromAngle(vArmDownRight.heading(), 0.1 * this.armUpSize)),
    p5.Vector.add(pElbowRight, vArmDownRight).add(p5.Vector.fromAngle(vArmDownRight.heading() + HALF_PI, 0.05 * this.armUpSize))];
    let arHandLeft = [p5.Vector.add(pElbowLeft, vArmDownLeft).add(p5.Vector.fromAngle(vArmDownLeft.heading() - HALF_PI, 0.05 * this.armUpSize)),
    p5.Vector.add(pElbowLeft, vArmDownLeft).add(p5.Vector.fromAngle(vArmDownLeft.heading(), 0.1 * this.armUpSize)),
    p5.Vector.add(pElbowLeft, vArmDownLeft).add(p5.Vector.fromAngle(vArmDownLeft.heading() + HALF_PI, 0.05 * this.armUpSize))];


    // Down parts
    let vHipLeft = p5.Vector.fromAngle(HALF_PI + vBodyDown.heading(), this.hipsWidth / 2);
    let pHipLeft = p5.Vector.add(vBodyDown, vHipLeft);
    let vHipRight = p5.Vector.fromAngle(-HALF_PI + vBodyDown.heading(), this.hipsWidth / 2);
    let pHipRight = p5.Vector.add(vBodyDown, vHipRight);
    let vLegUpLeft = p5.Vector.fromAngle(this.legLupAngle + HALF_PI, this.legUpSize);
    let pKneeLeft = p5.Vector.add(pHipLeft, vLegUpLeft);
    let vLegDownLeft = p5.Vector.fromAngle(vLegUpLeft.heading() + this.legLdownAngle, this.legDownSize);
    let vLegUpRight = p5.Vector.fromAngle(this.legRupAngle + HALF_PI, this.legUpSize);
    let pKneeRight = p5.Vector.add(pHipRight, vLegUpRight);
    let vLegDownRight = p5.Vector.fromAngle(vLegUpRight.heading() + this.legRdownAngle, this.legDownSize);

    fill(255);
    stroke(this.color);
    strokeWeight(2);
    push()
    // Draw upper body
    translate(this.pos.x, this.pos.y)

    // Body
    // stroke(color(255, 20, 20))
    noFill();
    beginShape();
    curveVertex(pElbowLeft.x + vArmDownLeft.x, pElbowLeft.y + vArmDownLeft.y);
    curveVertex(pElbowLeft.x + vArmDownLeft.x, pElbowLeft.y + vArmDownLeft.y);
    curveVertex(pElbowLeft.x, pElbowLeft.y);
    curveVertex(pShoulderLeft.x, pShoulderLeft.y);
    curveVertex(vBodyUp.x, vBodyUp.y);
    curveVertex(0,0);
    curveVertex(vBodyDown.x, vBodyDown.y);
    curveVertex(pHipLeft.x,pHipLeft.y);
    curveVertex(pKneeLeft.x,pKneeLeft.y);
    curveVertex(pKneeLeft.x + vLegDownLeft.x, pKneeLeft.y + vLegDownLeft.y);
    curveVertex(pKneeLeft.x + vLegDownLeft.x, pKneeLeft.y + vLegDownLeft.y);
    endShape();
    
    beginShape();
    curveVertex(pElbowRight.x + vArmDownRight.x, pElbowRight.y + vArmDownRight.y);
    curveVertex(pElbowRight.x + vArmDownRight.x, pElbowRight.y + vArmDownRight.y);
    curveVertex(pElbowRight.x, pElbowRight.y);
    curveVertex(pShoulderRight.x, pShoulderRight.y);
    curveVertex(vBodyUp.x, vBodyUp.y);
    curveVertex(0,0);
    curveVertex(vBodyDown.x, vBodyDown.y);
    curveVertex(pHipRight.x,pHipRight.y);
    curveVertex(pKneeRight.x,pKneeRight.y);
    curveVertex(pKneeRight.x + vLegDownRight.x, pKneeRight.y + vLegDownRight.y);
    curveVertex(pKneeRight.x + vLegDownRight.x, pKneeRight.y + vLegDownRight.y);
    endShape();
    
    push()
    translate(pCenterHead.x, pCenterHead.y)
    rotate(this.headAngle)
    ellipse(0, 0, this.headSize / 1.5, this.headSize);
    strokeWeight(2);
    pop()

    // // Shoulders
    // line(vBodyUp.x, vBodyUp.y, pShoulderLeft.x, pShoulderLeft.y);
    // stroke(color(25, 20, 200))
    // line(vBodyUp.x, vBodyUp.y, pShoulderRight.x, pShoulderRight.y);
    // // Hips
    // stroke(color(20, 25, 200))
    // line(vBodyDown.x, vBodyDown.y, pHipLeft.x, pHipLeft.y);
    // stroke(color(25, 20, 200))
    // line(vBodyDown.x, vBodyDown.y, pHipRight.x, pHipRight.y);
    // // Legs
    // stroke(color(250, 20, 20))
    // line(pHipLeft.x, pHipLeft.y, pKneeLeft.x, pKneeLeft.y);
    // line(pHipRight.x, pHipRight.y, pKneeRight.x, pKneeRight.y);
    // stroke(color(250, 200, 20))
    // line(pKneeLeft.x, pKneeLeft.y, pKneeLeft.x + vLegDownLeft.x, pKneeLeft.y + vLegDownLeft.y);
    // line(pKneeRight.x, pKneeRight.y, pKneeRight.x + vLegDownRight.x, pKneeRight.y + vLegDownRight.y);
    // // Arms
    // stroke(color(250, 20, 20))
    // line(pShoulderLeft.x, pShoulderLeft.y, pElbowLeft.x, pElbowLeft.y);
    // line(pShoulderRight.x, pShoulderRight.y, pElbowRight.x, pElbowRight.y);
    // stroke(color(250, 200, 20))
    // line(pElbowLeft.x, pElbowLeft.y, pElbowLeft.x + vArmDownLeft.x, pElbowLeft.y + vArmDownLeft.y);
    // line(pElbowRight.x, pElbowRight.y, pElbowRight.x + vArmDownRight.x, pElbowRight.y + vArmDownRight.y);

    // // Points 
    // stroke('purple'); // Change the color
    // strokeWeight(8);
    // point(pCenterHead)
    // arPtNeck.map(el => point(el))
    // arPtBody.map(el => point(el))
    // strokeWeight(4);
    // arElbowLeft.map(el => point(el))
    // arElbowRight.map(el => point(el))
    // arHandRight.map(el => point(el))
    // arHandLeft.map(el => point(el))
    pop()
  }

  drawDebug(pos = null) {
    // Skeleton parts as vectors
    let vBodyUp = p5.Vector.fromAngle(- HALF_PI + this.bodyUpAngle, this.bodyHeight / 2);
    let vBodyDown = p5.Vector.fromAngle(HALF_PI + this.bodyDownAngle, this.bodyHeight / 2);

    let arPtBody = [p5.Vector.fromAngle(vBodyDown.heading() + 0.5 * (this.bodyUpAngle - this.bodyDownAngle) - HALF_PI, 0.5 * this.hipsWidth),
    p5.Vector.fromAngle(vBodyDown.heading() + 0.5 * (this.bodyUpAngle - this.bodyDownAngle) + HALF_PI, 0.5 * this.hipsWidth)];


    // Upper parts
    let vNeck = p5.Vector.fromAngle(vBodyUp.heading() + this.neckAngle, this.neckSize);
    let arPtNeck = [p5.Vector.add(vBodyUp, vNeck).add(p5.Vector.fromAngle(vNeck.heading() + HALF_PI, 0.5 * this.neckSize)),
    p5.Vector.add(vBodyUp, vNeck).add(p5.Vector.fromAngle(vNeck.heading() - HALF_PI, 0.5 * this.neckSize))];

    let pCenterHead = p5.Vector.add(vBodyUp, vNeck).add(createVector(0, -0.5 * this.headSize));

    let vShoulderLeft = p5.Vector.fromAngle(HALF_PI + vBodyUp.heading(), this.shoulderWidth / 2);
    let pShoulderLeft = p5.Vector.add(vBodyUp, vShoulderLeft);
    let vShoulderRight = p5.Vector.fromAngle(-HALF_PI + vBodyUp.heading(), this.shoulderWidth / 2);
    let pShoulderRight = p5.Vector.add(vBodyUp, vShoulderRight);
    let vArmUpLeft = p5.Vector.fromAngle(this.armLupAngle + HALF_PI, this.armUpSize);
    let pElbowLeft = p5.Vector.add(pShoulderLeft, vArmUpLeft);
    let vArmDownLeft = p5.Vector.fromAngle(vArmUpLeft.heading() + this.armLdownAngle, this.armDownSize);
    let vArmUpRight = p5.Vector.fromAngle(this.legRupAngle + HALF_PI, this.armUpSize);
    let pElbowRight = p5.Vector.add(pShoulderRight, vArmUpRight);
    let vArmDownRight = p5.Vector.fromAngle(vArmUpRight.heading() + this.armRdownAngle, this.armDownSize);

    let arElbowLeft = [p5.Vector.add(pElbowLeft, p5.Vector.fromAngle(vArmUpLeft.heading() - 0.5 * (vArmUpLeft.heading() - vArmDownLeft.heading()) - HALF_PI, 0.05 * this.armUpSize)),
    p5.Vector.add(pElbowLeft, p5.Vector.fromAngle(vArmUpLeft.heading() - 0.5 * (vArmUpLeft.heading() - vArmDownLeft.heading()) + HALF_PI, 0.05 * this.armUpSize))];
    let arElbowRight = [p5.Vector.add(pElbowRight, p5.Vector.fromAngle(vArmUpRight.heading() - 0.5 * (vArmUpRight.heading() - vArmDownRight.heading()) - HALF_PI, 0.05 * this.armUpSize)),
    p5.Vector.add(pElbowRight, p5.Vector.fromAngle(vArmUpRight.heading() - 0.5 * (vArmUpRight.heading() - vArmDownRight.heading()) + HALF_PI, 0.05 * this.armUpSize))];
    let arHandRight = [p5.Vector.add(pElbowRight, vArmDownRight).add(p5.Vector.fromAngle(vArmDownRight.heading() - HALF_PI, 0.05 * this.armUpSize)),
    p5.Vector.add(pElbowRight, vArmDownRight).add(p5.Vector.fromAngle(vArmDownRight.heading(), 0.1 * this.armUpSize)),
    p5.Vector.add(pElbowRight, vArmDownRight).add(p5.Vector.fromAngle(vArmDownRight.heading() + HALF_PI, 0.05 * this.armUpSize))];
    let arHandLeft = [p5.Vector.add(pElbowLeft, vArmDownLeft).add(p5.Vector.fromAngle(vArmDownLeft.heading() - HALF_PI, 0.05 * this.armUpSize)),
    p5.Vector.add(pElbowLeft, vArmDownLeft).add(p5.Vector.fromAngle(vArmDownLeft.heading(), 0.1 * this.armUpSize)),
    p5.Vector.add(pElbowLeft, vArmDownLeft).add(p5.Vector.fromAngle(vArmDownLeft.heading() + HALF_PI, 0.05 * this.armUpSize))];


    // Down parts
    let vHipLeft = p5.Vector.fromAngle(HALF_PI + vBodyDown.heading(), this.hipsWidth / 2);
    let pHipLeft = p5.Vector.add(vBodyDown, vHipLeft);
    let vHipRight = p5.Vector.fromAngle(-HALF_PI + vBodyDown.heading(), this.hipsWidth / 2);
    let pHipRight = p5.Vector.add(vBodyDown, vHipRight);
    let vLegUpLeft = p5.Vector.fromAngle(this.legLupAngle + HALF_PI, this.legUpSize);
    let pKneeLeft = p5.Vector.add(pHipLeft, vLegUpLeft);
    let vLegDownLeft = p5.Vector.fromAngle(vLegUpLeft.heading() + this.legLdownAngle, this.legDownSize);
    let vLegUpRight = p5.Vector.fromAngle(this.legRupAngle + HALF_PI, this.legUpSize);
    let pKneeRight = p5.Vector.add(pHipRight, vLegUpRight);
    let vLegDownRight = p5.Vector.fromAngle(vLegUpRight.heading() + this.legRdownAngle, this.legDownSize);

    fill(255);
    stroke(this.color);
    strokeWeight(2);
    push()
    // Draw upper body
    translate(this.pos.x, this.pos.y)

    // Body
    stroke(color(255, 20, 20))
    line(0, 0, vBodyUp.x, vBodyUp.y);
    stroke(color(20, 255, 20))
    line(0, 0, vBodyDown.x, vBodyDown.y);
    stroke(color(20, 25, 200))
    // Neck+Head
    stroke(color(25, 250, 20))
    line(vBodyUp.x, vBodyUp.y, vBodyUp.x + vNeck.x, vBodyUp.y + vNeck.y);
    push()
    strokeWeight(4);
    stroke(25)
    translate(pCenterHead.x, pCenterHead.y)
    rotate(this.headAngle)
    ellipse(0, 0, this.headSize / 1.5, this.headSize);
    strokeWeight(2);
    pop()

    // Shoulders
    line(vBodyUp.x, vBodyUp.y, pShoulderLeft.x, pShoulderLeft.y);
    stroke(color(25, 20, 200))
    line(vBodyUp.x, vBodyUp.y, pShoulderRight.x, pShoulderRight.y);
    // Hips
    stroke(color(20, 25, 200))
    line(vBodyDown.x, vBodyDown.y, pHipLeft.x, pHipLeft.y);
    stroke(color(25, 20, 200))
    line(vBodyDown.x, vBodyDown.y, pHipRight.x, pHipRight.y);
    // Legs
    stroke(color(250, 20, 20))
    line(pHipLeft.x, pHipLeft.y, pKneeLeft.x, pKneeLeft.y);
    line(pHipRight.x, pHipRight.y, pKneeRight.x, pKneeRight.y);
    stroke(color(250, 200, 20))
    line(pKneeLeft.x, pKneeLeft.y, pKneeLeft.x + vLegDownLeft.x, pKneeLeft.y + vLegDownLeft.y);
    line(pKneeRight.x, pKneeRight.y, pKneeRight.x + vLegDownRight.x, pKneeRight.y + vLegDownRight.y);
    // Arms
    stroke(color(250, 20, 20))
    line(pShoulderLeft.x, pShoulderLeft.y, pElbowLeft.x, pElbowLeft.y);
    line(pShoulderRight.x, pShoulderRight.y, pElbowRight.x, pElbowRight.y);
    stroke(color(250, 200, 20))
    line(pElbowLeft.x, pElbowLeft.y, pElbowLeft.x + vArmDownLeft.x, pElbowLeft.y + vArmDownLeft.y);
    line(pElbowRight.x, pElbowRight.y, pElbowRight.x + vArmDownRight.x, pElbowRight.y + vArmDownRight.y);

    // Points 
    stroke('purple'); // Change the color
    strokeWeight(8);
    point(pCenterHead)
    arPtNeck.map(el => point(el))
    arPtBody.map(el => point(el))
    strokeWeight(4);
    arElbowLeft.map(el => point(el))
    arElbowRight.map(el => point(el))
    arHandRight.map(el => point(el))
    arHandLeft.map(el => point(el))
    pop()
  }

  rest() {
    this.headAngle = 0;
    this.neckAngle = 0;

    // Body 2 segments with angle
    this.bodyUpAngle = 0;
    this.bodyDownAngle = 0;
    // shoulder & hips
    this.armRupAngle = 0;
    this.armRdownAngle = 0;
    this.armLupAngle = 0;
    this.armLdownAngle = 0;

    // legs
    this.legRupAngle = 0;
    this.legRdownAngle = 0;
    this.legLupAngle = 0;
    this.legLdownAngle = 0;
  }

  shake() {
    this.headAngle = random(-0.2, 0.2);
    this.neckAngle = random(-0.2, 0.2);

    // Body 2 segments with angle
    this.bodyUpAngle = random(-0.2, 0.2);
    this.bodyDownAngle = random(-0.2, 0.2);
    // shoulder & hips
    this.armRupAngle = random(-0.2, 0.2);
    this.armRdownAngle = random(-0.2, 0.2);
    this.armLupAngle = random(-1, 0.2);
    this.armLdownAngle = random(-0.2, 1);

    // legs
    this.legRupAngle = random(-0.3, 0.3);
    this.legRdownAngle = random(-0.3, 0.3);
    this.legLupAngle = random(-0.3, 0.3);
    this.legLdownAngle = random(-0.3, 0.3);
  }

  bigShake() {
    this.headAngle = random(-0.5, 0.5);
    this.neckAngle = random(-0.5, 0.5);

    // Body 2 segments with angle
    this.bodyUpAngle = random(-0.6, 0.6);
    this.bodyDownAngle = random(-0.6, 0.6);
    // shoulder & hips
    this.armRupAngle = random(-HALF_PI, 1.5*HALF_PI);
    this.armRdownAngle = random(-HALF_PI, HALF_PI);
    this.armLupAngle = random(-HALF_PI, HALF_PI);
    this.armLdownAngle = random(-HALF_PI, HALF_PI);

    // legs
    this.legRupAngle = random(-HALF_PI, 1.5*HALF_PI);
    this.legRdownAngle = random(-HALF_PI, 1.5*HALF_PI);
    this.legLupAngle = random(-HALF_PI, HALF_PI);
    this.legLdownAngle = random(-HALF_PI, HALF_PI);
  }
}
