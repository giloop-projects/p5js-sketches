---
title: People generator
description: Basic skeleton and pose generator
image: people-gen.jpg
category: visual
publish: true
tags: [generative, art, visual, audio]
---

# People generator

Test de génération de poses aléatoire à partir d'une forme de squelette *articulé*.


