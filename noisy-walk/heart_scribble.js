// Colors palettes, from https://coolors.co/palettes/trending
// import { arPalettes } from '../lib/color-palettes.js';

// let arPalette = ["#e28413","#f56416","#dd4b1a","#ef271b","#ea1744"];
let arPalette = ["#ff0000","#d90202","#c20303","#ab0404","#930505","#7c0606"]
let bRotate; 
let vPos; // point position
let vPosPrec;
let speed;
let offset;
let c1;
let arHeart;
let r_fact;
let count_out;
function setup() {
  createCanvas(windowWidth*0.99, windowHeight*0.7);
  // Heart points
  // This variant is from https://editor.p5js.org/AnonymousPyro/full/klkuDmy4
  arHeart = []
  for (t = 0; t < TWO_PI; t += radians(5)) {
    r = map(sin(t), 0, 1, height / 40, height / 35);
    x = r * 16 * pow(sin(t), 3);
    y = -r * (13 * cos(t) - 5 * cos(2 * t) - 2 * cos(3 * t) - cos(4 * t) + 5);
    arHeart.push([x+0.5*width, y+0.5*height]);
  }

  speed = createVector(0.005, -0.005);
  offset = createVector(0,0);
  // vPos = createVector(random(0,width), random(0,height))
  vPos = createVector(0.5*width, 0.5*height)
  c1 = color(255,215,5)
  stroke(c1)
  background(253);
  r_fact = 0;
  count_out = 0;

}


function windowResized() {
  resizeCanvas(0.99*windowWidth, 0.7*windowHeight);
  vPos = createVector(random(0,width), random(0,height))
  background(251);
} 

function draw() {
  // Update walk
  vPosPrec = vPos.copy();
  // let vNoise = createVector(width*noise(offset.x, 0), height*noise(0, offset.y))
  let vNoise = createVector(map(noise(offset.x, 0),0,1,-10,10), map(noise(0, offset.y), 0,1,-10,10))

  vPos = p5.Vector.add(vPosPrec, vNoise)
  if (! isPointInsidePolygon([vPos.x, vPos.y], arHeart)) {
    count_out = count_out + 1;
    if (count_out<5) {
        // Heuristic to get back in polygon
        r_fact = r_fact + random([-1,1])*0.9*PI; // smoother : 0.25*HALF_PI
    } else {
        // Turn till get back in : not working
        //  r_fact + 0.05*HALF_PI
        // Stuck : ideally direct back to center
        r_fact = 0; 
        // 
        vNoise.setHeading(p5.Vector.sub(createVector(width/2, height/2), vPosPrec).heading())
    }
    
  } else {
    if (count_out>0) {
        console.log("back in after "+count_out+" iters")
        count_out = 0;
        let coff = random([-20, 0, 20])
        if (vPos.y < height/2.5) {
          c1 = color(max(0,coff),87+coff,183+coff)
        } else {
          c1 = color(255,215+coff,max(0,5+coff))
        }
        stroke(c1)        
      } else {
        r_fact = r_fact*0.99
      }
  }
  
  

  vPos = p5.Vector.add(vPosPrec, vNoise.rotate(r_fact))

  offset = offset.add(speed);
  line(vPosPrec.x, vPosPrec.y, vPos.x, vPos.y)
  // Change color
  // if (random()<0.005) {
  //   c1 = color(random(arPalette))
  //   stroke(c1)
  // }

  // Draw heart shape
  push()
    color(0)
    stroke(2)
    for (pt of arHeart) {
      point(pt[0], pt[1]);
    }
  pop()
  
}


function mousePressed() {
  background(250)
}


// This function comes from 
// https://stackoverflow.com/questions/22521982/check-if-point-is-inside-a-polygon
const isPointInsidePolygon = (point, vertices) => {
    const x = point[0]
    const y = point[1]
    
    let inside = false
    for (let i = 0, j = vertices.length - 1; i < vertices.length; j = i++) {
      const xi = vertices[i][0],
        yi = vertices[i][1]
      const xj = vertices[j][0],
        yj = vertices[j][1]
    
      const intersect = yi > y != yj > y && x < ((xj - xi) * (y - yi)) / (yj - yi) + xi
      if (intersect) inside = !inside
    }
    
    return inside
}