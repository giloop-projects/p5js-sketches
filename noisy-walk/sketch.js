// Colors palettes, from https://coolors.co/palettes/trending
// import { arPalettes } from '../lib/color-palettes.js';

let oPalette;
let btnPalette;
let bRotate; 
let vPos; // point position
let vPosPrec;
let speed;
let offset;
let c1;

function setup() {
  createCanvas(windowWidth*0.99, windowHeight*0.7);
  pickPalette()
  speed = createVector(0.01, -0.01);
  offset = createVector(0,0);
  vPos = createVector(random(0,width), random(0,height))
  c1 = color(random(oPalette.palette))
  stroke(c1)
  background(253);
}

function pickPalette() {
  oPalette =  random(arPalettes);
  document.getElementById('info').innerHTML = "<i>"+oPalette.name+"</i>";
}

function windowResized() {
  resizeCanvas(0.99*windowWidth, 0.7*windowHeight);
  vPos = createVector(random(0,width), random(0,height))
  background(251);
} 

function draw() {
  // Update walk
  vPosPrec = vPos.copy();
  let vNoise = createVector(width*noise(offset.x, 0), height*noise(0, offset.y))
  // Lowpass factor : -> 0 smooth, -> 1 no filter
  let lowpass = 1; //
  vPos = createVector(lowpass*vNoise.x + (1-lowpass)*vPosPrec.x, lowpass*vNoise.y + (1-lowpass)*vPosPrec.y)
  offset = offset.add(speed);
  line(vPosPrec.x, vPosPrec.y, vPos.x, vPos.y)
  // Change color
  if (random()<0.005) {
    c1 = color(random(oPalette.palette))
    stroke(c1)
  }
}

function drawTile(tPos, tSize, tType = "square") {
  c1 = color(random(oPalette.palette),)
  c2 = color(random(oPalette.palette))
  c2.setAlpha(175)
  stroke(c1)
  fill(c2)
  let s = random([0,1,2,3])
  strokeWeight(s)

  if (tType==="mixed") { tType = random(["square", "circle"]) }
  if (tType==="square") {
    rect(tPos.x+.5*(tileSize-tSize), tPos.y+.5*(tileSize-tSize), tSize, tSize, random(arSizes), random(arSizes), random(arSizes), random(arSizes));
  } else {
    circle(tPos.x+.5*tileSize, tPos.y+.5*tileSize, tSize);
  }
}

function mousePressed() {
  pickPalette();
  background(250)
  // drawTriGrid();
  // bRotate = !bRotate
}
