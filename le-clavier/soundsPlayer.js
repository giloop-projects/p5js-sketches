class mapSound {
    constructor(name) {
        this.strIndex = name; // 
        this.sounds = []; 
        this.next = undefined; // sound to play after
        this.prev = undefined; // sound played before
    }

    // Add a sound
    addSound(soundPath) {
        let newSound = new p5.SoundFile(soundPath,
            function success() { console.log(soundPath + ' loaded');  },
            function error() { console.log('problem loading ' + soundPath); },
            function loading() { console.log("Loading " + soundPath); }
        );
        this.sounds.push(newSound);
    };

    // play one of the sound with given or random index
    playSound(idx = -1) {
        if (this.sounds.length == 0) return;
        if (idx == -1 || idx >= this.sounds.length) { idx = floor(random(0, this.sounds.length)); };
        console.log(`Playing ${this.strIndex}[${idx}]`)
        this.sounds[idx].play();
        // Append 
        if (this.next) {
            let thisSound = this;
            this.sounds[idx].onended(function playNext() {
                thisSound.next.prev=thisSound;
                thisSound.next.playSound();
            });
        }
        // Remove callback if this was the previous sound
        if (this.prev) {
            this.prev.onended(() => {});
        }
    };

    // Append next : play a map sound after this one reaches the end
    appendNext(nextSound) {
        let thisSound = this;
        for (let s of this.sounds) {
            // Remove all callbacks 
            s.onended(() => { 
                nextSound.playSound(); 
                thisSound.removeNext(); 
            }); 
        }
    }

    removeNext() {
        for (let s of this.sounds) {
            // Remove all onended callbacks 
            s.onended(() => {}); 
        }
    }

    isLoaded() {
        const testLoaded = (accum, snd) => accum && snd.isLoaded();
        return this.sounds.reduce(testLoaded,true);
    }
};

class SoundList {
    // Constructor loads all sounds given in a json list in soundsDir
    constructor(soundsDir, soundsJson) {
        this.mySounds = [];
        // Load default at index 0
        let defaultSound = new mapSound("default");
        defaultSound.addSound(soundsDir + "/default.mp3");
        this.mySounds.push(defaultSound);

        // Load all sounds listed in a JSON file
        const thisSL = this;
        fetch(soundsJson).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            
            data.map(el => {
                // Add the sample to mySounds
                console.log(el);
                thisSL.addSound(soundsDir, el);
            });
        }).catch(err => {
            // Do something for an error here
            console.log('Something went wrong while reading file list :' + err);
        });
    }

    addSound(dir, name) {
        /* Add a sample in mySounds. Each sample must be named 'key_name.mp3' 
        * where key is the ascii code
        */
        let fileParts = name.split("_");
        // Check if result is non empty
        if (fileParts.length < 2) {
            console.log("file " + name + " has no '_' for parsing");
            return;
        }
        let index = fileParts[0];
        // Lookfor 
        let s = this.mySounds.filter(el => el.strIndex == index);
        if (s.length > 0) {
            // append sample in list
            console.log("ajout de " + name + " à " + index);
            s[0].addSound(dir + "/" + name);
        } else {
            // create new sample and append it in array mySounds
            console.log("nouvel index : " + index + ", nom : " + name);
            let newMap = new mapSound(index);
            newMap.addSound(dir + "/" + name);
            this.mySounds.push(newMap);
        }
    }

    // Fonction pour jouer un son à partir de son nom
    playSoundName(index) {
        let playedSound = this.findSoundName(index);
        playedSound.playSound();     
    };

    // Fonction qui renvoie un son à partir de son nom 
    findSoundName(index) {
        console.log("Searching index : " + index);
        let s = this.mySounds.filter(el => el.strIndex == str(index));
        let playedSound = this.mySounds[0]; // Défault sound
        if (s.length > 0) {
            playedSound = s[0];
        } else {
            console.log(" -> not found (playing defaut)");    
        }
        return playedSound;
    }

    // Plays an array of names as a sequence 
    // One starts when one ends
    playSequence(arNames) {
        if (arNames.length<1) { 
            return; // No sounds 
        }
        if (arNames.length<2) { 
            this.playSoundName(arNames[0]); return; // Only one sound
        }

        // At least 2 sounds to play
        let nextName = arNames.pop();
        let curName = arNames.pop();
        let curSound = this.findSoundName(curName);
        let nextSound = this.findSoundName(nextName);
        
        while (curName) {
            curSound.appendNext(nextSound);
            // Current becomes next to play for the previous sound
            nextSound = curSound;
            // Look for previous sound in array
            curName = arNames.pop();
            curSound = this.findSoundName(curName);
        }
        // Play first sound in array
        nextSound.playSound();
    }

    getPctLoaded() {
        const countLoaded = (accum, snd) => accum + Number(snd.isLoaded());
        return floor(100*this.mySounds.reduce(countLoaded,0) / this.mySounds.length);
    }

};

