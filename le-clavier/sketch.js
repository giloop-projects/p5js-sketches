let myList;
let currentKey = undefined;
let targetKey = undefined;
let txtSize = 0;
let prevErr = 0;
let targetSize = 100;
let bFound = false;
let bAllLoaded = false;
// Mode de jeu :
// 0: découverte du clavier 
// 1: appui sur une touche
let appMode = 0; 

function preload() {
  soundFormats('mp3', 'ogg');
  myList = new SoundList('data/sounds', 'sounds.json');
  // lighter version used for debugging (loads faster)
  myList = new SoundList('data/sounds_2', 'sounds_2.json');
}

function windowResized() {
  resizeCanvas(0.98*windowWidth, 0.98*windowHeight);
  targetSize = min(floor(0.75*windowHeight), floor(0.75*windowWidth));
} 

function setup() {
  let cnv = createCanvas(windowWidth, windowHeight);
  background(240,255,200);
  fill(0, 102, 253);
  // text('Chargement des sons ...', 10, 20);
  textAlign(CENTER, CENTER);
  windowResized();
}

function drawLoading() {
  let pct = myList.getPctLoaded();
  background(240,245,240,125);
  fill(0, 102, 253);
  textSize(20);
  textAlign(CENTER, CENTER)
  if (pct>99) {
    text('Chargé', 0.5*width, 0.5*height);
    bAllLoaded = true;
  } else {
    noStroke();
    fill(0, 102/pct, 253);
    rect(0,0,0.01*pct*width,height);
    fill(pct*2, pct*2, 253);
    text('Chargement des sons ' + pct + '%', 0.5*width, 0.5*height);
  }
}

function draw() {
  if (!bAllLoaded) {
    // Chargement des samples en cours
    drawLoading();
    return;
  }
  // 
  if (appMode) {
    drawGameMode();
  } else {
    drawKeyboardMode();
  }

  drawHelp();
  update();
}

function drawHelp() {
  // todo
}
function drawKeyboardMode() {
  background(240,245,240,125);
  fill(0, 102, 253);
  textSize(20);
  textAlign(LEFT, CENTER);
  text('Mode Découverte : Appuyer sur une touche', 10, height-20);
 // Dessin de la touche appuyée// Dessin de la touche appuyée
    if (currentKey) {
      textAlign(CENTER, CENTER);
      textSize(txtSize);
      text(currentKey.toUpperCase(), 0.5*width, 0.5*height);
    }

}

function drawGameMode() {
   background(240,255,255,50);
   fill(0, 102, 253);
   textSize(20);
   textAlign(LEFT, CENTER);
   text('Mode Jeu : Retrouver la touche affichée', 10, height-20); 
   // Dessin de la touche appuyée
   
  if (targetKey) {
    fill(0, 253, 102);
    textAlign(CENTER, CENTER);
    textSize(0.75*targetSize);
    text(targetKey.toUpperCase(), 0.3*width, 0.5*height);
  }

  if (currentKey) {
    if (bFound) {
      fill(0, 253, 102);
    } else {
      fill(253, 0, 102);
    }
    textAlign(CENTER, CENTER);
    textSize(0.75*txtSize);
    text(currentKey.toUpperCase(), 0.7*width, 0.5*height);
  }

}

function update()  {
  // Animation des lettres
  let Kp = 0.75;
  let Kd = 0.20;
  let Ki = 0.00;
  let pErr = targetSize - txtSize;
  let iErr =  pErr * deltaTime;
  let dErr = (prevErr - pErr) / deltaTime;
  if (currentKey && abs(pErr)>1) {
    prevErr = pErr;
    txtSize += Kp*pErr + Kd*dErr +   Ki*iErr;
  }
}


function keyPressed() {
  // Info sur les touches pressées
  console.log(`key:${key}, keyCode:${keyCode}`);

  // F1 : changement de mode
  if (keyCode == 112) {
    appMode = !appMode;
    if (appMode) {
      myList.playSoundName('modejeu');
      // Génération d'une Nouvelle lettre en mode Jeu
      setTimeout(() => { randomLetter();}, 2500 );
    } else {
      myList.playSoundName('modedecouverte'); 
    }
    return false; // prevent any default behaviour
  } 

  // Exclusion des touches spéciales : on ne fait rien 
  if (keyCode<=40 || (keyCode>=112 && keyCode<=123) || keyCode == 160) {
    return false;
  }

  if (appMode) {
    // mode jeu : on compare à la touche cherchée
    if (!bFound) { // Si la touche a été trouvée, on attend le reset à false
      // Stocke la touche pour affichage
      currentKey = key;
      prevSize = 0;
      txtSize = 0; 
      // Vérification 
      bFound = targetKey.toUpperCase() === currentKey.toUpperCase();
      if (bFound) {
        myList.playSequence([str(keyCode), "bravo"]);
        // Génère une nouvelle lettre à trouver
        setTimeout(() => { randomLetter();}, 4500);
      } else {
        myList.playSequence([str(keyCode), "non"]);
      }
    }
  } else {
    // mode découverte : on joue le son associé à la touche appuyée

    // Renvoie sur les nombres
    if (key.length==1 && "0123456789".includes(key)) {
      keyCode = 96+Number(key);
      console.log(` → changed keyCode:${keyCode}`);
    }

    // Son aléatoire 
    myList.playSoundName(str(keyCode));

    // Stocke la touche pour affichage
    currentKey = key;
    prevSize = 0;
    txtSize = 0; 

  }

  return false; // prevent any default behaviour
}

// Generate a new letter
function randomLetter(arBefore = undefined) {
  
  // Find a new random letter for the game
  let newKeyCode = floor(random(0,26)) + 65;
  if (newKeyCode>90) { 
    newKeyCode += 6;
  }
  // Stocke la touche pour affichage
  targetKey = newKeyCode>90?char(newKeyCode-48):char(newKeyCode);
  console.log(`Jeu : trouvez key:${targetKey}, keyCode:${newKeyCode}`);


 if (arBefore && arBefore.constructor === Array) {
  myList.playSequence(arBefore.concat(["oncherche", str(newKeyCode)]));
 } else {
  myList.playSequence(["oncherche", str(newKeyCode)]);
 }

  // Reset des touches appuyées
  currentKey = undefined;
  prevSize = 0;
  txtSize = 0;
  // Reset bFound
  bFound = false;

}