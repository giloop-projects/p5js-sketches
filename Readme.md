# Sketch P5js

[P5js](https://p5js.org) est la version Javascript du langage Processing (Java). C'est un ensemble de librairies orientées Creative Coding.

Pour avoir passé plusieurs années à utiliser Processing, cette version JS rassemble tous les éléments qu'il manquait pour en faire des applis web directement déployables en ligne. C'est top ! 

Au niveau des ressources, je vous conseille la chaîne [The Coding Train](https://thecodingtrain.com/) animée par Daniel Shiffman 😍. J'aime ce gars, depuis des années. 

Pour les sketches rassemblés ici, je pars souvent soit des exemples fournis sur le site, soit des challenges du Coding Train. 

Amusez vous !

## Installation

- Cloner le dépot
- Ouvrer studio code dans le dossier
- Lancer le live serveur et 

## Création d'un nouveau sketch

Pour démarrer avec une page blanche, je recopie le dossier `empty-example` et le renomme. 

Utilisez la commande suivante pour copier un dossier dans la console de studio code ou autre (l'option /E assure la copie des sous-dossiers et /I considère la destination comme un dossier et le crée si besoin).

```
xcopy.exe /E/I empty-example new-example
```

## Mise à jour de l'index.html

Un script python dans le dossier ressources permet de générer une page d'accueil / portfolio des sketches qui contiennent dans leur dossier un fichier `index.md`. La génération utilise jinja2 comme moteur de template et vient lire les données frontmatter en yaml des fichiers `index.md` situés dans les sous-dossiers du répertoire racine. 

[À voir en **Démo** ici](https://gueulomaton.org/p5js/)

```
pip install jinja2 python-frontmatter
```

Le template est plutôt sympa, facilement customisable et basé sur le codepen CodePen [Material Design - Responsive card
de David Foliti](https://codepen.io/marlenesco/pen/NqOozj).

La mise à jour d'`index.html` est faite avec la commande suivante : 

```
python .\resources\generate_index.py
```

