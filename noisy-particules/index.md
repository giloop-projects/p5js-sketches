---
title: Simplement du bruit
description: Test sur des séquences de bruit de Perlin. Applications autour du coding train sur le thème.
image: image4.jpg
category: code
publish: true
tags: [visual]
---

# Always start with a blank page 

Pour démarrer avec une page blanche, je recopie ce dossier et le renomme. 

Utiliser la commande suivante pour copier un dossier dans la console de studio code ou autre (l'option /E assure la copie des sous-dossiers et /I considère la destionation comme un dossier et le crée si besoin).  

```
xcopy.exe /E/I empty-example new-example
```
