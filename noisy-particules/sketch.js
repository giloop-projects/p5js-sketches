// Inspiration from : 
// The coding train : https://www.youtube.com/watch?v=ZI1dmHv3MeM
// Nice line renders : https://genekogan.com/code/p5js-perlin-noise/
//

let speed = 0.005
let arParts = [];

class CirclePart {
  constructor(r, s = 0.005, n = 0.005, c = color) {
    this.color = c;
    this.radius = r;
    this.angle = random(0, TWO_PI);
    this.pos = p5.Vector.fromAngle(this.angle, (0.5+0.5*noise(n)) * this.radius);
    this.prevPos = this.pos.copy();
    this.speed = s; // rotation speed
    this.n = n; // noise level
    this.dir = round(random())*2 - 1
    this.type = 'heart'; // 'circle'; // 
  }

  update() {
    // Store last pos
    this.prevPos = this.pos.copy();
    // Update angle & pos
    this.angle = this.angle + this.dir*this.speed;

    if (this.type=='heart') {
      let r = (0.5+0.5*noise(this.n)) * this.radius;
      this.pos = createVector(r * pow(sin(this.angle), 3),
        0.1*height- 0.075*r * (13 * cos(this.angle) - 5 * cos(2 * this.angle) - 2 * cos(3 * this.angle) - cos(4 * this.angle) + 5) );
  
    } else { // if (this.type=='circle') {
      let r = (0.5+0.5*noise(this.n)) * this.radius;
      this.pos = p5.Vector.fromAngle(this.angle, r);
  
    } 

    // update noise
    this.n += this.speed;
  }

  draw() {
    noFill();
    stroke(this.color);
    strokeWeight(1)
    line(this.pos.x, this.pos.y, this.prevPos.x, this.prevPos.y)
  }
}

function setup() {
  createCanvas(400,400);
  for (let i = 0; i < 30; i++) {
    arParts.push(new CirclePart(width/2.4, speed+0.01*random(), random(50), color(random(255), random(255), random(255)))) // color(10, 10, 10))) // 
  }
  background(250)
}


function draw() {
  background(255,10)
  push()
  translate(width/2, height/2);
  for (const circ of arParts) {
    circ.update()
    circ.draw()
  }
  pop()
}

function mousePressed() {
  background(250, 250, 255);
}