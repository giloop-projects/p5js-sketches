// A class for defining a simple markov chain
// without hidden states, only transition between states
// A very nice resource of Markov Chains visually explained : https://setosa.io/ev/markov-chains/

class MarkovChain {
    // 
    constructor(transitionMatrix, idxStartState = 0) {
        // transitionMatrix should be a 2D array, i.e. an array of numerical vectors
        // ex : transMtx = [
        //  [0.1, 0.5, 0.4],
        //  [0.3, 0.4, 0.3],
        //  [0.6, 0.1, 0.3],
        // ]
        // Each Row gives the transition probabilities to reach other states
        // The input matrix will be normalized so that : 1: rows sum to 1 and 2: columns sum to 1
        // 
        this.currentState = idxStartState;
        this.nStates = transitionMatrix.length;
        // Ensure deep copy of lines
        this.transMtx = JSON.parse(JSON.stringify(transitionMatrix));
        this.normalizeRowsTo1();

    }

    normalizeRowsTo1() {
        // Normalize transition matrix so that : 
        // - rows sum to 1
        let r,c;
        for (r = 0; r < this.transMtx.length; r++) {
            let lineSum = this.transMtx[r].reduce((a,b) => a+b, 0)
            for (c = 0; c < this.transMtx[r].length; c++) {
                this.transMtx[r][c] = this.transMtx[r][c]/lineSum;
            }
        }
    }

    normalizeColsTo1() {
        // Normalize transition matrix so that : 
        // - columns sum to 1
        let r,c;
        for (c = 0; c < this.transMtx[0].length; c++) {
            // Foreach columns, 
            // get sum of rows
            let colSum = 0;
            for (r = 0; r < this.transMtx.length; r++) {
                colSum += this.transMtx[r][c];
            }
            // and normalize to one
            for (r = 0; r < this.transMtx.length; r++) {
                this.transMtx[r][c] = this.transMtx[r][c]/lineSum;
            }
        }
    }

    updateState() {
        // Return next state according to transition matrix probabilities
        let prob = Math.random();
        let nextState = 0;
        let cumsum = 0;
        for (let c = 0; c < this.transMtx[this.currentState].length; c++) {
            cumsum += this.transMtx[this.currentState][c];
            if (prob<=cumsum) {
                break;
            } else {
                nextState++;
            }
        }

        this.currentState = nextState;
        return this.currentState;
    }

    updateTransitionP(idxStateCurrent, idxStateNext, proba) {
        this.transMtx[idxStateCurrent][idxStateNext] = proba;
        // Renormalize (all) rows
        this.normalizeRowsTo1()
    }

    updateFullMatrix(newTransitionMatrix) {
        // Ensure deep copy of lines
        this.transMtx = JSON.parse(JSON.stringify(newTransitionMatrix));
        this.nStates = this.transMtx.length;
        // Renormalize rows
        this.normalizeRowsTo1()
    }
}