let nRows = 20;
let nCones = 10;
let epais = 3;
function setup() {
    myCanvas = createCanvas(0.99*windowWidth, 0.7*windowHeight);
    myCanvas.mouseWheel(addCones);  
}
  
function draw() {
    // Draw some points
    background(30,30,50, 150)
    const col = color(241,250,238); // (230,57,70); // 
    stroke(col);
    strokeWeight(epais);
    noFill()

    let v1 = createVector(mouseX-width/2, mouseY-height/2);
    
    translate(width/2, height/2)
    rotate(v1.heading())
    
    // Outer cone
    let radius = 150
    let step = 8
    for (let idx = 1; idx < nCones; idx++) {
        // Inner cone
        circle(idx*step-0.5*epais,0,radius+idx*step*2)
        // Outer cone
        circle((nCones-idx+1)*step-0.5*epais,0,radius+(nCones+idx-1)*step*2)

    }   
}

function addCones(event) {
    if (event.deltaY > 0) {
        nCones = nCones+1;
    } else {
        nCones = max(3, nCones-1);
    }
  }