let nRows = 20;
let nCols = 20;
let sx, sy, xOff, yOff;
let distortion, zoomHard;

function setup() {
    myCanvas = createCanvas(0.99*windowWidth, 0.7*windowHeight);
    myCanvas.mouseWheel(changeSize);
    // Grid size
    sx = 0.9*width / nCols;
    sy = 0.9*height / nRows;
    xOff = 0.5*(width-sx*nCols);
    yOff = 0.5*(height-sy*nRows);
    zoomHard = false;
    distortion = new SphericalDist(width/2, height/2, height/5);
}
  
function draw() {
    // Draw some points
    background(240);
   
    stroke(0);
    // Draw lines
    for (let r = 0; r < nRows; r++) {
        for (let c = 0; c < nCols; c++) {
            let p = createVector(xOff+sx*c, yOff+sy*r);
            let pd = distortion.distort(p);
            // points only
             strokeWeight(4);
             point(pd); 
            // Draw segments with neighbours
            strokeWeight(2);
            if (c>0) { // Point on previous col
                let pdc = distortion.distort(createVector(xOff+sx*(c-1), yOff+sy*r))
                drawLine(pd,pdc);
            }
            if (r>0) { // Point on previous row
                let pdr = distortion.distort(createVector(xOff+sx*c, yOff+sy*(r-1)))
                drawLine(pd,pdr);
            }
            
        }
    }

    // Draw circle
    strokeWeight(1);
    stroke(220,0,0);
    distortion.draw(mouseX, mouseY);
}

function drawLine(p1, p2) {
    line(p1.x, p1.y, p2.x, p2.y); // straight
}

class SphericalDist {
    constructor(x,y, radius) {
        this.c = createVector(x,y);
        this.r = radius;
    }

    draw(x=undefined, y=undefined) {
        if (x) { this.c.x = x; }
        if (y) { this.c.y = y; }
        noFill();
        circle(this.c.x,this.c.y, 2*this.r);
    }

    // Return distorded point coordinates
    distort(p) {
        let pdist;
        let d = this.c.dist(p);
        if (d<this.r && d>0) {
            let vPC = p5.Vector.sub(p, this.c);
            
            if (zoomHard) {
                // Hard spherical disto : (x-R)^2 + y^2 = R^2
                let dd = this.r*(2*sqrt(d/this.r) - d/this.r);
                pdist = p5.Vector.add(this.c, vPC.setMag(dd));
            } else {
                // Soft y^2 = x;
                let dd = this.r*(sqrt(d/this.r));
                pdist = p5.Vector.add(this.c, vPC.setMag(dd));
            }

        } else {
            // no distortion outisde circle
            pdist = p.copy();
        }

        return pdist
    }
}

function mousePressed() {
    zoomHard = true;
}

function mouseReleased() {
    zoomHard = false;
}

function changeSize(event) {
    if (event.deltaY > 0) {
        distortion.r = min(height/1.5, distortion.r + 2);
    } else {
        distortion.r = max(10, distortion.r - 2);
    }
  }