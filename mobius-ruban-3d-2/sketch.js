// Création d'un ruban de Möbius à partir des équations
// https://fr.wikipedia.org/wiki/Ruban_de_M%C3%B6bius
// Author : Gilles Gonon
// Site : http://gilles.gonon.free.fr
//

let a = 0;
let nTurns = 4;
let radius = 80;

function setup() {
  createCanvas(800, 400, WEBGL);
  // Setup for GIF loop creation (un|comment)
  frameRate(20);
  createLoop(10, { gif:false}); // { fileName: "moebiusLoop2.gif" } }); // 
}

function draw() {
  background(250);
  // Rotating visualization
  // rotateY(frameCount * 0.01);
  rotateY(animLoop.theta); // Rotation for GIF creation
  rotateX(HALF_PI);

  //drag to move the world.
  orbitControl();

  noFill();
  // fill(237, 34, 93, 128);
  strokeWeight(2);
  stroke(200,153,0);
  moebius(nTurns, radius);
  noStroke();
  stroke(255,0,0,200)
  fill(255,0,0,200);
  // Moving cubes with var a
  // moebiusPoint(nTurns, radius, a);
  for (let i = 0; i < 20; i++) {
    moebiusPoint(nTurns, radius, animLoop.theta+HALF_PI*i/40, sin(PI*i/20)); // Moving cubes for GIF creation
  }
  a += radians(1);
  if (a > TWO_PI) {a = 0;}
}


function moebius(k, r=50) {
  colorMode(HSB, 1, 1, 1, 1);
  for (let t = -1; t < 1; t += 0.4) {
    const myColor = color(map(t,-1,1,0,1), 0.5, 1,0.5);
    stroke(myColor); 
    beginShape();  // LINES
    for (let v = 0; v < PI; v += radians(2)) {
      px = (2 +  t*cos(k*v)) * cos(2*v);
      py = (2 +  t*cos(k*v)) * sin(2*v);
      pz = t * sin(k*v);
      vertex(r*px, r*py, r*pz);
    }
    endShape(CLOSE);
  }
  colorMode(RGB, 255);
}

function moebiusPoint(k,r=50, angle, amp) {
  colorMode(HSB, 1, 1, 1, 1);
  
  for (let t = -1; t < 1; t += 0.4) {

      const myColor = color(map(t,-1,1,0,1), 0.85, 1.0,0.5);
      const zdev = amp*r*animLoop.noise({ seed: floor(10*(t+2)) })
      px = (2 +  t*cos(k*angle)) * cos(2*angle);
      py = (2 +  t*cos(k*angle)) * sin(2*angle);
      pz = t * sin(k*angle);
      push(); 
      fill(myColor); noStroke();
      translate(r*px, r*py, r*pz+zdev);
      stroke(myColor); 
      line(0,0,0,0,0,-zdev)
      box(4);
      pop();
  }

  colorMode(RGB, 255);
}