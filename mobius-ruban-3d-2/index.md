---
title: Le ruban de Möbius 2/2
description: (In progress) Balade infinie sur un ruban de Möbius 3D. Cette version travaille sur les éléments qui défilent dans le ruban
image: mobius-3d.jpg
category: visual
tags: [generative, art, 3d]
---

# Balade infinie sur un ruban de Möbius 3D

C'est un travail en cours pour associer une boucle générative infinie audio. L'idée serait alors d'interpréter ce ruban comme une partition qui se boucle sur elle-même en réponse à la musique. 
