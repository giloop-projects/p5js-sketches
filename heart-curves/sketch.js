// Personnal fork of Heart Curve
// Daniel Shiffman
// https://thecodingtrain.com/CodingChallenges/134-heart-curve.html
// https://editor.p5js.org/codingtrain/sketches/egvieHyt0
// https://youtu.be/oUBAi9xQ2X4
// I <3 you
// Different Heart curves, from https://mathworld.wolfram.com/HeartCurve.html
// https://www.desmos.com/calculator/5icqqjjvh3?lang=fr
// Many pratical explanations here : 
// http://www.mathematische-basteleien.de/heart.htm

const heart = [];
let a = 0;
let curveIndex = 0;
let r;
let numHearts;

function setup() {
  createCanvas(windowWidth, 0.8 * windowHeight);
  hart_curve(0);
  numHearts = hart_curve();
}

function hart_curve(idx) {
  // update Heart variable
  if (isNaN(Number(idx))) {
    return 10; // number of heart
  }
  heart.length = 0;
  let x, y, t;
  switch (idx) {
    case 0: // Cardioid
      r = height / 4;
      for (t = PI / 2; t > -TWO_PI + PI / 2; t -= radians(5)) {
        x = -r * (1 - sin(t)) * cos(t);
        y = -r * (1 - sin(t)) * sin(t) - r;
        heart.push(createVector(x, y));
      }
      break;

    case 1:
      r = height / 40;
      for (t = 0; t > -TWO_PI; t -= radians(5)) {
        x = r * 16 * pow(sin(t), 3);
        y = -r * (13 * cos(t) - 5 * cos(2 * t) - 2 * cos(3 * t) - cos(4 * t) + 5);
        heart.push(createVector(x, y));
      }
      break;

    case 2:
      r = height / 1.5;
      for (t = 0; t < TWO_PI; t += radians(5)) {
        let p = (t - PI) / PI;
        if (p == 0) { continue; }

        x = -r * sin(p) * cos(p) * log(abs(p));
        y = -r * sqrt(abs(p)) * cos(p) + 0.36 * r;
        heart.push(createVector(x, y));
      }
      break;

    case 3:
      r = height / 10;
      for (t = PI / 2; t > -TWO_PI + PI / 2; t -= radians(5)) {
        let rt = 2 - 2 * sin(t) + sin(t) * sqrt(abs(cos(t))) / (sin(t) + 1.4);
        x = -r * (rt * cos(t));
        y = -r * rt * sin(t) - 2.5 * r;
        heart.push(createVector(x, y));
      }
      break;

    case 4:
      // This variant is from https://editor.p5js.org/AnonymousPyro/full/klkuDmy4
      for (t = 0; t < TWO_PI; t += radians(5)) {
        r = map(sin(t), 0, 1, height / 48, height / 40);
        x = r * 16 * pow(sin(t), 3);
        y = -r * (13 * cos(t) - 5 * cos(2 * t) - 2 * cos(3 * t) - cos(4 * t) + 5);
        heart.push(createVector(x, y));
      }
      break;

    case 5:
      r = height / 4;
      for (t = -PI ; t < PI; t += radians(5)) {
        x = -1 - r * (0.7 * t * sin( PI*sin(t) / t));
        y = -2 + r * (0.7* abs(t) * cos( PI*sin(t) / t)) - 0.5*r;
        heart.push(createVector(x, y));
      }
      break;

    case 6:
      r = height / 4;
      for (t = PI / 2; t > -TWO_PI + PI / 2; t -= radians(5)) {
        let rm = map(pow(sin(t + PI / 2), 2), 0, 1, height / 6, height / 4);
        x = -rm * (1 - sin(t) ** 2) * cos(t);
        y = -rm * (1 - sin(t) ** 2) * sin(t) - r;
        heart.push(createVector(x, y));
      }
      break;

    case 7:
      r = height / 40;
      for (t = 0; t < TWO_PI; t += radians(5)) {
        x = -r * (12 * sin(t) - 4 * sin(3 * t));
        y = -r * (13 * cos(t) - 5 * cos(2 * t) - 2 * cos(3 * t));
        heart.push(createVector(x, y));
      }
      break;

    case 8:
      r = height / 4;
      for (t = 0; t < TWO_PI; t += radians(5)) {
        x = 3 - r * (1.3 * pow(sin(t), 3))
        y = -0.75 * r - r * (1.3 * (cos(t) - pow(cos(t), 4)));
        heart.push(createVector(x, y));
      }
      break;

    case 9:
      r = height / 4;
      for (t = 0; t < TWO_PI; t += radians(5)) {
        x = 3 - r * cos(t);
        y = 3 - r * (sin(t) + sqrt(abs(cos(t))));
        heart.push(createVector(x, y));
      }
      break;
    default:
      break;
  }
  console.log('Generating heart[' + idx + '], length:' + heart.length);
}

function draw() {
  // So that it stops
  if (a > heart.length) {
    textSize(20);
    noStroke();
    textAlign(LEFT, TOP);
    text('Curve #' + curveIndex + '\nClic of key for next curve', 10, 10);
    return;
  }
  background(0);
  translate(width / 2, height / 2);

  stroke(255);
  strokeWeight(2);
  fill(150, 0, 100);
  beginShape();
  for (let v = 0; v < heart.length; v++) {
    vertex(heart[v].x, heart[v].y);
    if (v > a) { break; }
  }
  endShape();

  // console.log(a);
  a += 1;
}

function keyPressed() {
  const num = Number(key);
  console.log(num)
  if (!isNaN(num) && num < numHearts && key != " ") {
    curveIndex = num;
  } else {
    curveIndex += 1;
    curveIndex = curveIndex % numHearts;
  }
  hart_curve(curveIndex);
  a = 0; // reset drawing with a=0;
}

function mousePressed() {
  curveIndex += 1;
  curveIndex = curveIndex % numHearts;

  hart_curve(curveIndex);
  a = 0; // reset drawing with a=0;
}