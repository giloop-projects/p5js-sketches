---
title: Heart Curves
description: Exploring Heart curves equation as suggested in The coding train. I just searched for a few more equations.
image: heart.jpg
category: visual
tags: [generative, art]
---

# Exploring Heart curves 

The starting point for this sketch is a coding train episode. I just search for a few more equations.
