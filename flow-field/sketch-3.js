// Variation aux couleurs du drapeau Ukrainien 
// RGB : (0, 87, 183)  (255, 215, 05)
// HEX : #1B5583        #FAD201

var inc = 0.1;
var t_inc = 0.01;
var scl = 10;
var nparts = 1500;
var cols, rows;
var zoff = 0;
var bDrawGrid = false;
var particles = [];
var flowfield;
var couleur = 0;
var heart = [];
var tHeart = 20; // période de vibration du coeur

function setup() {
    createCanvas(600,400);
    cols = floor(width / scl);
    rows = floor(height / scl);
    // Création duchamps de force (pas de var° temporelle)
    flowfield = new Array(cols * rows);

    // Remplissage du flowfield à partir de l'équation d'un coeur
    // Coordonnées polaires relatives à (width/2, height/2)
    r = height / 35;
    for (t = 0; t > -TWO_PI; t -= radians(10)) {
       x = width/2 + r * 16 * pow(sin(t), 3);
       y = 1.2*height/2 - r * (13 * cos(t) - 5 * cos(2 * t) - 2 * cos(3 * t) - cos(4 * t) + 5);
       heart.push(createVector(x, y));
    }

    strokeWeight(2)
    
    slider = createSlider(0.001, 0.25, 0.05, 0.01);
    btn_new = createButton('Re-generate')
    cb_time = createCheckbox('Time evolution (3D noise)', true)
    cb_field = createCheckbox('View perlin noise grid', false)
    cb_field.changed(checkBoxLatch);
    btn_new.mousePressed(genNewGrid);

    // zoff += inc/2;
    // Création des particules
    for (let i = 0; i < nparts; i++) {
        particles[i] = new Particle()
    }

    fr = createP('');
    colorMode(HSB,255);

    background(255);
  }
  
function draw() {
    background(210,50)
    inc = slider.value()
    // Mise à jour du coeur
    r = (0.25+abs(0.75*cos(0.001*millis()*TWO_PI/tHeart))) * height / 35;
    var i=0
    for (t = 0; t > -TWO_PI; t -= radians(10)) {
       x = width/2 + r * 16 * pow(sin(t), 3);
       y = 1.2*height/2 - r * (13 * cos(t) - 5 * cos(2 * t) - 2 * cos(3 * t) - cos(4 * t) + 5);
       heart[i++] = createVector(x, y);
    }

    if (bDrawGrid) {
        background(255,50)
    }

    gridUpdate();

    couleur = couleur + t_inc;
    particles.forEach(p => {
        p.follow(flowfield)
        p.update();
        p.edges();
        var hue = (p.pos.y>height/2)?42.5:170;
        p.draw(hue, 200+couleur);
    });

    fr.html('Framerate : ' + floor(frameRate()))
}

function genNewGrid() {
   // Reset particules pos and velocity
   particles.forEach(p => {
       p.pos = createVector(random(width), random(height))    
       p.vel = createVector(random(),random())
   });
   // Change noiseSeed
   noiseSeed(floor(random(255)))
   background(255)
}

function gridUpdate() {
    var wt = 0.001*millis()*TWO_PI/25;
    var forceHeart = 0.25+0.5*(1+cos(wt));
    for (let y = 0; y < rows; y++) {
        for (let x = 0; x < cols; x++) {
            var index = (x + y * cols);
            // Direction = vecteur vers le point le plus proche du coeur
            // A partir du point central de la case de la grille
            const pt1 = createVector((x + 0.5) * scl, (y + 0.5) * scl);
            var v = closestHeart(pt1);
            // Bruit sur l'angle de 
            var r = (noise(zoff)-0.5) * PI;
            v.setHeading(forceHeart*v.heading() + r)
            v.setMag(max(0.5, r/10))
            flowfield[index] = v
            
             // Grid grey view of perlin noise resolution
            if (bDrawGrid) {

                var g = (v.heading()*0.5)/TWO_PI*255
                fill(g,250,250,128)
                rect(x*scl, y*scl, scl, scl)
                drawArrow(pt1, v, color(255, 204, 120))
            
            }
        }
    }
    zoff += t_inc * cb_time.checked(); // inc/5; //

    
}

function checkBoxLatch() {
    bDrawGrid = cb_field.checked();
}

// drawArrow from p5js reference 
function drawArrow(base, vec, myColor) {
    push();
    stroke(myColor);
    strokeWeight(2);
    fill(myColor);
    translate(base.x, base.y);
    line(0, 0, vec.x, vec.y);
    rotate(vec.heading());
    let arrowSize = 3;
    translate(vec.mag() - arrowSize, 0);
    triangle(0, arrowSize / 2, 0, -arrowSize / 2, arrowSize, 0);
    pop();
}


function closestHeart(pos) {
    // Renvoie la position du point le plus proche dans le tableau heart
    // Vector3D : x:x_
    var distMin = width
    var vec_out = createVector(0,0);
    var x_prev = heart[heart.length-1].x
    var y_prev = heart[heart.length-1].y
    heart.forEach(el => {
        let d = p5.Vector.dist(pos, el)
        if (d < 0.7*scl) {
            // Le point du coeur est dans la case → on garde la dérivée
            // de la courbe
            vec_out = createVector(el.x-x_prev, el.y-y_prev)
            distMin = d
        }
        if (d < distMin) {
            // Si le point est plus proche, on le garde
            vec_out = createVector(el.x - pos.x, el.y - pos.y)
            distMin = d
        }
        x_prev = el.x;
        y_prev = el.y;
    });

    return(vec_out);



}