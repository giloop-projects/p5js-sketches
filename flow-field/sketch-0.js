// Creation et visualisation d'un bruit de Perlin en 2D/3D 
var inc = 0.1;
var t_inc = 0;
var scl = 20;
var cols, rows;
var zoff = 0;
var bShowField = true;

function setup() {
    createCanvas(400, 400);
    cols = floor(width / scl);
    rows = floor(height / scl);
    // Création duchamps de force (pas de var° temporelle)
    flowfield = new Array(cols * rows);

    strokeWeight(1)

    slider = createSlider(0.001, 0.25, 0.05, 0.01);
    btn_new = createButton('Re-generate')
    cb_field = createCheckbox('View perlin noise grid', true)
    cb_time = createCheckbox('Time evolution (3D noise)', true)
    btn_new.mousePressed(genNewGrid);

    fr = createP('');

}

function draw() {
    background(255)
    inc = slider.value()
    t_inc = cb_time.checked() > 0 ? 0.005 : 0;

    var yoff = 0;
    for (let y = 0; y < rows; y++) {
        var xoff = 0;
        for (let x = 0; x < cols; x++) {
            var index = (x + y * cols);
            var r = noise(xoff, yoff, zoff) * TWO_PI;
            var v = p5.Vector.fromAngle(r, 0.5 * scl);
            flowfield[index] = v
            xoff += inc;

            // Grid grey view of perlin noise resolution
            if (cb_field.checked()) {
                fill((r / TWO_PI) * 255)
                rect(x * scl, y * scl, scl, scl)
            }
            stroke(200, 128);
            const pt1 = createVector((x + 0.5) * scl, (y + 0.5) * scl);
            drawArrow(pt1, v, color(255, 204, 120))
            // push();
            // translate(x * scl, y * scl);
            // strokeWeight(3);
            // point()
            // strokeWeight(3);
            // rotate(v.heading());
            // line(0,0, scl, 0);
            // pop();    
        }
        yoff += 1.5 * inc;
    }
    zoff += t_inc; // inc/5; //

    fr.html('Framerate : ' + floor(frameRate()))
}

function genNewGrid() {
    // Change noiseSeed
    noiseSeed(floor(random(255)))
    background(255)
}

// drawArrow from p5js reference 
function drawArrow(base, vec, myColor) {
    push();
    stroke(myColor);
    strokeWeight(2);
    fill(myColor);
    translate(base.x, base.y);
    line(0, 0, vec.x, vec.y);
    rotate(vec.heading());
    let arrowSize = 3;
    translate(vec.mag() - arrowSize, 0);
    triangle(0, arrowSize / 2, 0, -arrowSize / 2, arrowSize, 0);
    pop();
}