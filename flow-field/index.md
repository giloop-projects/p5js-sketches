---
title: Flow field test
description: >
 Venez vous perdre dans un champs de bruits de Perlin pour générer des images hypnotiques. 
 Un classique du Coding Train de Daniel Shiffman. 
image: field-of-noise-600.jpg
category: code
publish: true
tags: [generative, art, visual]
---

# Les champs de force vectoriel

Perdu dans un champs de bruit de Perlin.