#!/usr/bin/env python3

from jinja2 import Environment, FileSystemLoader
import frontmatter
import glob
import os, os.path

print('Current dir is: {}'.format(os.getcwd()))


# Loading librairies
file_loader = FileSystemLoader('resources')
env = Environment(loader=file_loader)

# Get template
template = env.get_template('template_index.html')

# Get list of sketches data
lSketches = glob.glob("*/index.md")
lSketches = sorted(lSketches, key=os.path.getmtime)
# lDirs = sorted([os.path.dirname(x) for x in lSketches], key=os.path.getmtime)
lSketches.reverse()

print('Number of index.md files found:{}'.format(len(lSketches)))
# Loop on them to prepare template content
lContent = []
with open("index.html", "w", encoding="utf-8") as index_file:
    for sketch in lSketches:
        print(' - parsing {}'.format(sketch))
        front = frontmatter.load(sketch)
        if ("publish" in front.metadata and not(front.metadata['publish'])):
            print("   +-> skipped (publish is false)")
            continue

        # Card color for categories
        if front.metadata['category'] == 'audio':
            color = "Indigo"
            iconClass = 'glyphicon glyphicon-headphones'
        elif front.metadata['category'] == 'visual':
            color = "Green"
            iconClass = 'glyphicon glyphicon-picture'
        elif front.metadata['category'] == 'geometry':
            color = "Indigo"
            iconClass = 'glyphicon glyphicon-grain'
        elif front.metadata['category'] == 'code':
            color = "Blue"
            iconClass = 'glyphicon glyphicon-sunglasses'

        else:
            color = 'Pink'
            iconClass = 'glyphicon glyphicon-heart'
        
        # Directory of sketch for link
        sketchDir = os.path.dirname(sketch)

        # Skip empty-example
        if sketchDir == 'empty-example':
            print("   +-> skipped")
            continue    
        
        # Check if image exists or use default p5js one
        if os.path.isfile(os.path.join(sketchDir, front.metadata['image'])):
            image = os.path.join(sketchDir, front.metadata['image'])
        else:
            image = 'assets/images/p5js.png'
        
        dSketch = {'title':front.metadata['title'] , 'description': front.metadata['description'], 'image':image, 
        'category': front.metadata['category'] , 'color':color, 'dir':sketchDir, 'catIcon':iconClass  }

        # Append info to content list
        lContent.append(dSketch)

    # Render it 
    rendered = template.render(lSketches=lContent)
    index_file.write(rendered)

