let myList;
let reverb;
let audio;
let bAllLoaded = false;
const phrase = 'etre soi et huit milliards individus a la fois';
const arMots = phrase.split(' ');
let bInstructions = true;
let txtSize = 16;
let txtSpeakers = '';
let txtColor;
let nCombis = 0;

function setup() {
  let cnv = createCanvas(windowWidth*0.99, 200);
  // Load sounds
  soundFormats('mp3');
  myList = new SoundList('data/sounds', 'sounds.json');

  // Add reverb for all sounds 
  reverb = new p5.Reverb();
  reverb.set(1, 5);
  
  txtColor = color(172,3,3);

  background(200,0,0,100);
  fill(txtColor);
  textAlign(CENTER, CENTER);
  windowResized();
}

function windowResized() {
  resizeCanvas(0.99*windowWidth, 0.15*windowHeight);
  txtSize = floor(map(width, 300, 1600, 12, 30));
} 

function drawLoading() {
  let pct = myList.getPctLoaded();
  background(200,0,0,100);
  textSize(txtSize);
  textAlign(CENTER, CENTER)
  if (pct>99) {
    // Fin du chargement
    text('Chargé', 0.5*width, 0.5*height);
    bAllLoaded = true;
    myList.addEffect(reverb);
    nCombis = myList.mySounds.reduce((acc, snd) => acc * snd.sounds.length, 1)
    let contributeurs = new Set(myList.mySounds[0].person);
    // Crédits 
    document.getElementById('credits').innerHTML = '<p><b>'+ round(nCombis/1e9) + 
    '</b> milliards de possibilités de phrases 🤯, grace aux '+ contributeurs.size +
    ' contributeurs !<br>Un grand merci 😍 à : '+ 
    new Array(...contributeurs).sort().reduce((chaine, el) => chaine + '<a href="javascript:playPhrase(\''+el+'\');">'+el+'</a>, ','').slice(0,-2) + 
    '<br>Cliquez sur un nom pour entendre la phrase lue uniquement avec cette voix.</p>';
    '<br>Cliquez sur l\'image pour lire avec toutes les voix.</p>';
    // Ajout du callback sur l'image
    document.getElementById('image').onclick = function() { playPhrase(); }
    windowResized();
  } else {
    noStroke();
    fill(253, 200/(201-2*pct), 200/(201-2*pct) );
    rect(0,0,0.01*pct*width,0.1*height);
    fill(253, 200, 200);
    
    text('Chargement des sons en cours ...', 0.5*width, 0.5*height);
  }
}

function draw() {
  if (!bAllLoaded) {
    drawLoading();
  } else {
    background(255,230,200,100);
    fill(txtColor);
    textSize(txtSize);
    if (bInstructions) {
        // drawInstructions();
        textAlign(CENTER, CENTER);
        text('Cliquez sur l\'image pour entendre la phrase', 0.05*width, 0.05*height, 0.9 * width, 0.9 * height);
    } else {
        // updated by displaySpeakers()
        textSize(txtSize);
        textAlign(CENTER, CENTER);
        
        text('Phrase lue par : \n' + txtSpeakers, 0.05 * width, 0.05 * height, 0.9 * width, 0.9 * height);
    }
  }
}

function splitLongString(myText, minWidth, minChars) {
    // Texte sur 2 ou 3 lignes suivant la taille
    if (width < minWidth && myText.length > minChars) {
        let txtAffiche = myText;
        // 2 lignes 
        let vPos = myText.search(',');
        while (vPos > 0 && vPos < txtSpeakers.length * 0.5) {
            vPos += txtSpeakers.substring(vPos + 1).search(',') + 1;
        }
        // Replace ',' at vPos
        if (vPos > -1) {
            txtAffiche = txtSpeakers.substr(0, vPos) + '\n' + txtSpeakers.substr(vPos + txtSpeakers.length);
        }
        return myText;
    } else {
        // no change
        return myText;
    }
}

function playPhrase(person = undefined) {
  bInstructions = false;
  myList.playSequence(phrase.split(' '), person);
}

function displaySpeakers() {
  // Get persons who spoke
  let arSpeakers = [];
  for (let i = 0; i < myList.arPlayed.length; i++) {
    const name = arMots[i];
    const iPlayed = myList.arPlayed[i];
    arSpeakers.push(myList.findSoundName(name).person[iPlayed])
  }
  document.getElementById('info').innerText = 'Phrase lue par : ' + arSpeakers.join(', ')
  // Supprime l'initiale du prénom dans 
  arSpeakers = arSpeakers.map(el => {
    let arPerson = el.split(' ');
    return arPerson[0];
  })
  txtSpeakers = arSpeakers.join(', ');
}

/*
function mousePressed() {
  bInstructions = false;
  playPhrase();
}

function keyPressed() {
  playPhrase();
  bInstructions = false;
}
*/