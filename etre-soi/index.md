---
title: Être Soi
description: Être Soi et 8 milliards d'individus. Cette phrase est lue en concaténant différentes voix de manière aléatoire. Comme une mise en abîme du message lu. Plus de 500 milliards de possibilités 🤯
image: image.jpg
category: audio
tags: [generative, art, visual, audio]
---

# Être Soi et 8 milliards d'individus à la fois

Être Soi et 8 milliards d'individus est à l'origine une affiche de 1.5m par 1m exposée devant ma maison. Pour faire vivre ce message, j'enregistre des amis qui lisent la phrase et elle est en lue en concaténant les différentes voix de manière aléatoire. Comme une mise en abîme du message lu. Si vous souhaitez contribuer, c'est également possible en m'envoyant vos enregistrements.
