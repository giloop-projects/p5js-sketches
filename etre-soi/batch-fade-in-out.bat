rem Batch sur les fichiers sons d'origine avec SOX
rem pour ajouter un fade in / fade out / normalisation aux sons de l'appli

rem Passage en UTF-8
CHCP 65001

rem Ajout du chemin de sox au PATH Windows
SET PATH=%PATH%;D:\Apps\sox-14-4-2\
E:
SET SRCDIR=E:\Reaper songs\Citation-Etre-Soi\regions
SET DESTDIR=M:\IMTS\JS\p5js\etre-soi\data\sounds

REM echo Traitement des fichiers avec SOX > log-sox.txt
rem Boucle de traitement
FOR %%A IN ("%SRCDIR%\*.mp3") DO (
 	REM @echo "%%A"  >> log-sox.txt
    REM sox.exe "%%A" "%DESTDIR%\%%~nxA" fade l 400s -0 400s norm >> log-sox.txt 2>&1
	sox.exe "%%A" "%DESTDIR%\%%~nxA" fade l 400s -0 400s norm
)
	
REM for /f "tokens=*" %%s in ('dir /b "%SRCDIR%\*.mp3" ^| sort') do echo "%%s" >> log-sox-print.txt
pause
