function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

class mapSound {
    constructor(name) {
        this.strIndex = name; 
        this.person = [];
        this.sounds = [];
        this.effect = undefined;
    }

    // Add a sound
    // Name should follow pattern : name-person-#.mp3        
    addSound(soundPath) {
        let newSound = new p5.SoundFile(soundPath,
            function success() { console.log(soundPath + ' loaded');  },
            function error() { console.log('problem loading ' + soundPath); },
            function loading() { console.log("Loading " + soundPath); }
        );
        this.sounds.push(newSound);
        this.person.push(this.parsePerson(soundPath));
    };

    // 
    addEffect(effect) {
        this.effect = effect;
    }

    parsePerson(soundPath) {
        // Remove path if present
        const filename = soundPath.split('/').slice(-1).join();
        // Name should follow pattern : name-person-#.mp3
        let arParts = filename.split('-').slice(1,-1).map(capitalizeFirstLetter);
        if (arParts[arParts.length-1].length == 1) {
            return arParts.slice(0,-1).join('-')+' '+arParts[arParts.length-1];
        } else {
            return arParts.join('-');
        }
    }

    // play one of the sound with given or random index
    playSound(idx = -1, parent = undefined, person = undefined) {
        if (this.sounds.length == 0) return -1; // Empty sound

        if (idx == -1 || idx >= this.sounds.length) { 
            
            if (person) { // If a person is provided : search for its samples            
                let arIdxPerson = [];
                for (let i=0; i<this.person.length; i++) {
                    if (this.person[i]==person) {
                        arIdxPerson.push(i)
                    }
                }
                if (arIdxPerson.length) {
                    idx = arIdxPerson[floor(random(0, arIdxPerson.length))]; 
                } else {
                    idx = floor(random(0, this.sounds.length)); 
                }
            } else { // pick a random sample
                idx = floor(random(0, this.sounds.length)); 
            }
        }
        console.log(`Playing ${this.strIndex}[${idx}]`)
        if (this.effect) {
            this.effect.process(this.sounds[idx]);
        }
        this.sounds[idx].play();
        
        // Renvoie l'indice du son joué dans la série
        if (parent) {
            parent.arPlayed.push(idx);
            displaySpeakers()
        }
        return idx;
    };

    // Append next : play a map sound after this one reaches the end
    appendNext(nextSound, parent, person=undefined) {
        let thisSound = this;
        for (let s of this.sounds) {
            // Remove all callbacks 
            s.onended(() => { 
                nextSound.playSound(-1,parent, person); 
                thisSound.removeNext(); 
            }); 
        }
    }

    removeNext() {
        for (let s of this.sounds) {
            // Remove all onended callbacks 
            s.onended(() => {}); 
        }
    }

    isLoaded() {
        const testLoaded = (accum, snd) => accum && snd.isLoaded();
        return this.sounds.reduce(testLoaded,true);
    }
};

class SoundList {
    // Constructor loads all sounds given in a json list in soundsDir
    constructor(soundsDir, soundsJson) {
        this.mySounds = [];
        this.arPlayed = []; // array of played sounds
        // Load all sounds listed in a JSON file
        const thisSL = this; // Todo : closure
        fetch(soundsJson).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            data.map(el => {
                // Add the sample to mySounds
                console.log(el);
                thisSL.addSound(soundsDir, el);
            });
        }).catch(err => {
            // Do something for an error here
            console.log('Something went wrong while reading file list :' + err);
        });
    }

    addSound(dir, name) {
        /* Add a sample in mySounds. Each sample must be named 'key-name.mp3' 
        * where key is the ascii code
        */
        let fileParts = name.split("-");
        // Check if result is non empty
        if (fileParts.length < 2) {
            console.log("file " + name + " has no '-' for parsing");
            return;
        }
        let index = fileParts[0];
        // Lookfor 
        let s = this.mySounds.filter(el => el.strIndex == index);
        if (s.length > 0) {
            // append sample in list
            console.log("ajout de " + name + " à " + index);
            s[0].addSound(dir + "/" + name);
        } else {
            // create new sample and append it in array mySounds
            console.log("nouvel index : " + index + ", nom : " + name);
            let newMap = new mapSound(index);
            newMap.addSound(dir + "/" + name);
            this.mySounds.push(newMap);
        }
    }

    // Fonction pour jouer un son à partir de son nom
    playSoundName(index) {
        let playedSound = this.findSoundName(index);
        if (playedSound) {
            return playedSound.playSound();
        } else {
            return -1;
        }
    };

    // Fonction qui renvoie un son (mapSound) à partir de son nom 
    findSoundName(index) {
        console.log("Searching index : " + index);
        let s = this.mySounds.filter(el => el.strIndex == str(index));
        let playedSound;
        if (s.length > 0) {
            playedSound = s[0];
        } else {
            playedSound = undefined; // not found 
            console.log(" -> not found (no sound played)");    
        }
        return playedSound;
    }

    // Add effect to all sounds
    addEffect(effect) {
        for (let s of this.mySounds) {
            s.addEffect(effect);
        }
    }

    // Plays an array of names as a sequence 
    // One starts when one ends
    playSequence(arNames, person=undefined) {
        let idx; 
        this.arPlayed = []; // reset array

        if (arNames.length<1) { 
            return []; // No sounds 
        }
        if (arNames.length<2) { 
            idx = this.playSoundName(arNames[0],undefined, person); 
            return [idx]; // Only one sound
        }

        // At least 2 sounds to play
        let nextName = arNames.pop();
        let curName = arNames.pop();
        let curSound = this.findSoundName(curName);
        let nextSound = this.findSoundName(nextName);
        
        while (curName) {
            curSound.appendNext(nextSound, this, person);
            // Current becomes next to play for the previous sound
            nextSound = curSound;
            // Look for previous sound in array
            curName = arNames.pop();
            curSound = this.findSoundName(curName);
        }
        // Play first sound in array
        nextSound.playSound(-1, this, person);
    }

    getPctLoaded() {
        const countLoaded = (accum, snd) => accum + Number(snd.isLoaded());
        return floor(100*this.mySounds.reduce(countLoaded,0) / this.mySounds.length);
    }

};

