// Inspiration from : 
// The coding train : https://www.youtube.com/watch?v=ZI1dmHv3MeM
// Nice line renders : https://genekogan.com/code/p5js-perlin-noise/
//

let speed = 0.005
var n = 0
var drawing_function
let h_ang = 0
let sel; 

function setup() {
  createCanvas(400,400);
  let desc = createP('Génération simple de bruit de Perlin')
  desc.style('font-size', '16px');
  sel = createSelect();
  sel.option('Lines');
  sel.option('Triangle Strip');
  sel.option('Circle');
  sel.option('Circle Bis');
  sel.option('Bézier');
  sel.option('Rotating Ellipses');
  sel.option('Distorted Ellipses');
  sel.option('Heart');
  sel.selected('Heart');
  sel.changed(selectDraw);
  selectDraw();
}

function selectDraw() {
  let item = sel.value();
  if (item=='Lines') {
    drawing_function = drawlines;
  } else if (item=='Triangle Strip') {
    drawing_function = drawStrip;
  } else if (item=='Circle') {
    drawing_function = drawCircle;
  } else if (item=='Circle Bis') {
    drawing_function = drawCircleBis;
  } else if (item=='Bézier') {
    drawing_function = drawBezier;
  } else if (item=='Rotating Ellipses') {
    drawing_function = drawCircleRotating;
  } else if (item=='Distorted Ellipses') {
    drawing_function = drawOverEllipses;
  } else if (item=='Heart') {
    drawing_function = drawHeart;
  }
}

function draw() {
  
  // put drawing code here
  drawing_function()
}

function drawlines() {
  background(240,240,255);

  noFill();
  stroke(0,0,100);
  strokeWeight(2);
  n = n + speed;
  for (let y = 0; y < 10; y++) {
    let y_off = (0.5+y)*height/10
    beginShape();
    for (let v = 0; v < 100; v++) {
      let pos_x = v*width/100
      let pos_y = y_off + 11*(5-y)*noise(y/2, v+n)
      vertex(pos_x, pos_y);
    }
    endShape();
  }

  // strokeWeight(1);
  // for (let v = 0; v < 100; v++) {
  //   beginShape();
  //   for (let y = 0; y < 10; y++) {
  //     let y_off = (0.5+y)*height/10
  //     let pos_x = v*width/100
  //     let pos_y = y_off + 11*(5-y)*noise(y/2, v+n)
  //     vertex(pos_x, pos_y);
  //   }
  //   endShape();
  // }

} // drawlines

function drawStrip() {
  background(240,240,255);

  noFill();
  stroke(0,0,100);
  strokeWeight(2);
  n = n + speed;

  for (let yy = 0; yy < 9; yy++) {
    beginShape(TRIANGLE_STRIP);
    for (let v = 0; v < 20; v++) {
      for (let y=yy; y<yy+2; y++) {
        let y_off = (0.25+y)*height/10
          let pos_x = (v+0.5*((y+1)%2))*width/20
          let pos_y = y_off + 35*noise(y/2, v+n)
          vertex(pos_x, pos_y);
        }
    }
    endShape();
  }
}

function drawCircle() {
  background(240,240,255);
  noFill();
  stroke(0,0,100);
  strokeWeight(2);
  var radius = height * 0.05
  
  push()
  translate(0.5*width, 0.5*height)
  for (radius = height * 0.05; radius < height * 0.3; radius = radius + height * 0.05 ) {
    fill(0,0,100, map(radius, height*0.05, height*0.3, 128, 0))
    beginShape();
    n = n + speed;
    const n_pts = 50;
    for (let v = 0; v < n_pts; v++) {
        let r = radius + noise(n,ceil(v/2), v%2) * radius/3
        let pos_x = r * cos(TWO_PI/n_pts*v)
        let pos_y = r * sin(TWO_PI/n_pts*v)
        curveVertex(pos_x, pos_y);
      }
    endShape(CLOSE);
  }
pop()

}

function bell(x) {
  return sin(PI*(x/width))
}


function drawCircleBis() {
  stroke(0, 0, 200, 10);
  noFill();
  
  push()
  translate(width/2, height/2);
  beginShape();
  let n_pts = 200;
  for (var i = 0; i < n_pts; i++) {
    var ang = map(i, 0, n_pts, 0, TWO_PI);
    // n*0.005 : global speed of circle evolution
    let rad_noise = 0.25 // radial noise on circle
    var rad = 0.55*width * noise(rad_noise*(cos(ang)+1), rad_noise*(sin(ang)+1), n*0.005)
    var x = rad * cos(ang);
    var y = rad * sin(ang);
    curveVertex(x, y);
  }
  endShape(CLOSE);
  pop()
  n += 1;

  // clear the background every 600 frames using mod (%) operator
  // if (frameCount % 500 == 0) {
  //   background(250, 250, 255);
  // }

}

function drawBezier() {
  let points = []
  for (let i=0; i<4; i++) {
    if (i!=2) {
      points.push(createVector(width * noise(n+i), height * noise(n+4+i)))
    } else {
      points.push(createVector(width * (1-noise(n+i)), height * (1-noise(n+4+i))))
    }
  }

  // bezier(points[0].x, points[0].y, points[1].x, points[1].y, 
  //    points[2].x, points[2].y, points[3].x, points[3].y);
  bezier(width*(0.2+0.6*noise(n+15)),0, 
    width * cos(noise(n)*PI), height * sin(noise(n)*PI), 
    width * (1-cos(noise(n+30)*PI)), height * (1-sin(noise(n+30)*PI)),
    width*(0.8-0.6*noise(n+16)), height); // points[3].x, points[3].y);

  n += speed;

  // clear the background every 500 frames using mod (%) operator
  // if (frameCount % 500 == 0) {
  //   background(255);
  // }
}


function drawCircleRotating() {
  stroke(200 *noise(millis()/1000), 0, 200, 10);
  noFill();
  
  push()
  translate(width/2, height/2);
  beginShape();
  let n_pts = 100;
  let rot_speed = 0.1;
  for (var i = 0; i < n_pts; i++) {
    var ang = map(i, 0, n_pts, 0, TWO_PI);
    // n*0.005 : global speed of circle evolution
    let a = 0.15*width*noise(5-.005*n) ;
    let b = 0.45*width*noise(5+.005*n) ;
    var rad = a*b / sqrt((a*sin(ang))**2 + (b*cos(ang))**2)
    // var rad = 0.55*width * noise(rad_noise*(cos(ang)+1), rad_noise*(sin(ang)+1), n*0.005)
    var x = rad * cos(ang + rot_speed*n/TWO_PI);
    var y = rad * sin(ang + rot_speed*n/TWO_PI);
    curveVertex(x, y);
  }
  endShape(CLOSE);
  pop()
  n += 1;

  // clear the background every 600 frames using mod (%) operator
  // if (frameCount % 500 == 0) {
  //   background(250, 250, 255);
  // }

}

function drawOverEllipses() {
  stroke(200 *noise(millis()/1000), 0, 200, 10);
  noFill();
  
  push()
  translate(width/2, height/2);
  beginShape();
  let n_pts = 100;
  let rot_speed = 0.1;
  for (var i = 0; i < n_pts; i++) {
    var ang = map(i, 0, n_pts, 0, TWO_PI);
    // n*0.005 : global speed of circle evolution
    let b = 0.45*width*noise(5+.005*n) ;
    let e = 2-0.5*noise(0.001*n); // +0.9*noise(.01*n); // radial noise on eccentricity (0>e>1)
    var rad = b / sqrt(1 - (e*cos(ang))**2)
    // var rad = 0.55*width * noise(rad_noise*(cos(ang)+1), rad_noise*(sin(ang)+1), n*0.005)
    var x = rad * cos(ang + rot_speed*n/TWO_PI);
    var y = rad * sin(ang + rot_speed*n/TWO_PI);
    curveVertex(x, y);
  }
  endShape(CLOSE);
  pop()
  n += 1;

  // clear the background every 600 frames using mod (%) operator
  // if (frameCount % 500 == 0) {
  //   background(250, 250, 255);
  // }

}

function drawHeart() {
  // background(250, 250, 255);
  strokeWeight(1);
  stroke(250 , 0, 200*noise(millis()/1000), 10);
  noFill();
  
  push()
  translate(width/2, height/2);
  beginShape();
  let n_pts = 100;
  let rot_speed = 0.1;
  for (var i = 0; i < n_pts; i++) {
    var ang = map(i, 0, n_pts, 0, TWO_PI);
    var r = (height / 40) //*(0.9+0.1*noise(0.01*i));
    r = r + noise(0.005*n, ceil(i/2), i%2) * r/10 // angular noise
    r = r * (1 - .9*noise(0.005*n)) // global radius noise
    var  x = r * 16 * pow(sin(ang), 3);
    var  y = 0.2*height/2 - r * (13 * cos(ang) - 5 * cos(2 * ang) - 2 * cos(3 * ang) - cos(4 * ang) + 5);
    curveVertex(x, y);
  }
  endShape(CLOSE);
  pop()
  n += 1;

}

function mousePressed() {
  background(250, 250, 255);
}