---
title: Multi-track loops
description: Synchronized loops with parts and mixing GUI
image: multitrack.jpg
publish: true
category: audio
tags: [audio]
---

# Multi-track loops

Play synchronized loops with the ability to mix them. 
This sketch uses Tone.js along with p5.sound.js to have a lower level access to loops synchronization.
