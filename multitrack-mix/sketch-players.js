let arPlayers;
let arSounds;
let songList;
let isPlaying;
let bLoaded = false;
let arVolume = [];
let arPan = [];
let playTime;
let selectSong; 
let btnPlay;
let currentSong; 
let nToLoad;

function preload() {
  Tone.loaded().then(function(){
    console.log('Tone loaded');
    Tone.Transport.start();
    
    fetch('./sounds.json').then(response => {
      return response.json();
    }).then(data => {
        let i = 0;
        // console.log(data);
        songList = data.songs;
        currentSong = data.songs[0];
        arSounds = data.songs[0].tracks;
        nToLoad = arSounds.length;
        for (let i = 0; i < arSounds.length; i++) {
            const el = arSounds[i];
            arSounds[i].channel = new Tone.Channel(0, 0).toDestination();
            arSounds[i].player = new Tone.Player({url: "data/sounds/" + el.file, loop: true});
            arSounds[i].player.connect(arSounds[i].channel);
        }
        console.log("sounds loaded");
        console.log(arSounds);
        createInterface();

    }).catch(err => {
        // Do something for an error here
        console.log('Something went wrong while reading file list :' + err);
    }); 
  });
}
  
function setup() {
  let cnv = createCanvas(360, 250);
  // cnv.mouseClicked(togglePlay);

  // ensure audio is enabled
  Tone.Transport.start();

  isPlaying = false;

  txtColor = color(172, 3, 3);
  txtInfo = 'Chargement en cours ...';
}


  function draw() {
    background(200);
    background(255, 230, 200, 100);
    fill(txtColor);
    textSize(10);
        
    if (bLoaded) {
    if (arSounds.reduce((acc, el) => acc & el.player.loaded, true)) {
      for (let i = 0; i < arSounds.length; i++) {
        text(arSounds[i].track, 5, 35+(i+1)*25);
      }
      text("Volume", 100, 35+(arSounds.length+1)*25);
      text("Pan", 120+ 0.3*width, 35+(arSounds.length+1)*25);
      for (let i = 0; i < arSounds.length; i++) {
        arSounds[i].channel.set({volume: map(arVolume[i].value(), 0, 1, -30, 3), pan: arPan[i].value()})
      }
    } 
  }
    noStroke();
    // text(txtInfo, 10, 20);
  }

  function togglePlay() {
    // ensure audio is enabled
    if (arSounds.reduce((acc, el) => acc & el.player.loaded, true))
    
    if (isPlaying) {
        // Stop it all
        arSounds.map(el => { try { el.player.stop() } catch (error) { console.log("stop error") } });
        btnPlay.html("Click to Play")
        console.log("stopping");
        isPlaying = false;
        playTime = Tone.now();
    } else {
      // start the loop
      arSounds.map(el => el.player.start(0, playTime));
      btnPlay.html("Click to Pause")
      console.log("starting");
      isPlaying = true;
    }
  }

function createInterface() {
  selectSong = createSelect();
  songList.map(el => selectSong.option(el.name));
  selectSong.changed(changeSong);
  
  // Création des sliders volume / pan
  for (let i = 0; i < arSounds.length; i++) {
    // Volume
    sl = createSlider(0, 1, 1, 0.01);
    sl.position(100, 20+(i+1)*25 );
    sl.style('width', (0.3*width) + 'px');
    arVolume.push(sl);
    // Pan
    sl = createSlider(-1, 1, 0, 0.1);
    sl.position(120+ 0.3*width, 20+(i+1)*25 );
    sl.style('width', (0.3*width) + 'px');
    arPan.push(sl);   
  }  
  btnPlay = createButton('Click to Play');
  btnPlay.position(10, 5);
  btnPlay.mousePressed(togglePlay);

  bLoaded = true;
  playTime = 0;
  txtInfo = 'Click to start music';        
}

function changeSong() {
  // New song selected ? 
  if (currentSong.name == selectSong.value()) { return; }

  currentSong = songList.filter(el => el.name == selectSong.value())[0]
  console.log('Switching to song :' + currentSong.name);

  // Stop it all
  arSounds.map(el => { if (el.player.state ==" started") {el.player.stop()}});
  // Dispose player
  arSounds.map(el => { el.player.dispose() });
  // txtInfo = 'Click to start music';
  btnPlay.html("Click to Play")
  console.log("stopping");
  isPlaying = false;

  arSounds = currentSong.tracks;
  for (let i = 0; i < arSounds.length; i++) {
    const el = arSounds[i];
    arSounds[i].channel = new Tone.Channel(0, 0).toDestination();
    arSounds[i].player = new Tone.Player({url: "data/sounds/" + el.file, loop: true});
    arSounds[i].player.connect(arSounds[i].channel);
  }

  playTime = 0;
}