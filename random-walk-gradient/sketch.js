// Constants
const tileSize = 40;
let nRows;
let nCols;
let vTilePos;
const arSizes = [0,2,4,8,12]

// Colors palettes, from https://coolors.co/palettes/trending
// import { arPalettes } from '../lib/color-palettes.js';

let oPalette;
let btnPalette;
let bRotate; 
let selGeom; // pop to choose geometry

function setup() {
  createCanvas(windowWidth*0.99, windowHeight*0.7);
  pickPalette()
  bRotate = true;
  nCols = floor(width/(1.01*tileSize))
  nRows = floor(height/(1.01*tileSize));
  vTilePos = createVector(floor(random(0,nCols)), floor(random(0,nRows)))
  // selGeom = document.getElementById('selGeom');
  selGeom = createSelect();
  selGeom.option('square');
  selGeom.option('circle');
  selGeom.option('mixed');
  background(250);
}

function pickPalette() {
  oPalette =  random(arPalettes);
  document.getElementById('info').innerHTML = "<i>"+oPalette.name+"</i>";
}

function windowResized() {
  resizeCanvas(0.99*windowWidth, 0.7*windowHeight);
  nCols = round(width/tileSize);
  nRows = round(height/tileSize);
  background(250);
  txtSize = floor(map(width, 300, 1600, 12, 30));
} 

function draw() {
  // background(250,2)
    // Draw one tiles
    let vPos = createVector((0.1+vTilePos.x)*tileSize, (0.1+vTilePos.y)*tileSize)
    drawTile(vPos, 0.95*random(tileSize), selGeom.value())
    // Random walk
    vTilePos.x = (vTilePos.x + random([-2,-1,-1,0,1,1,2])) % nCols
    if (vTilePos.x<0) {vTilePos.x=nCols-1}
    vTilePos.y = (vTilePos.y + random([-2,-1,-1,0,1,1,2])) % nRows
    if (vTilePos.y<0) {vTilePos.y=nRows-1}
}

function drawTile(tPos, tSize, tType = "square") {
  c1 = color(random(oPalette.palette),)
  c2 = color(random(oPalette.palette))
  c2.setAlpha(175)
  stroke(c1)
  fill(c2)
  let s = random([0,1,2,3])
  strokeWeight(s)

  if (tType==="mixed") { tType = random(["square", "circle"]) }
  if (tType==="square") {
    rect(tPos.x+.5*(tileSize-tSize), tPos.y+.5*(tileSize-tSize), tSize, tSize, random(arSizes), random(arSizes), random(arSizes), random(arSizes));
  } else {
    circle(tPos.x+.5*tileSize, tPos.y+.5*tileSize, tSize);
  }
}

function setTriGradient(cx, cy, radius, c1, c2) {
  noFill();
  let x1 = cx + cos(TWO_PI / 3) * radius;
  let y1 = cy + sin(TWO_PI / 3) * radius;
  let x2 = cx + radius;
  let y2 = cy;
  let x3 = x1;
  let y3 = cy - sin(TWO_PI / 3) * radius;
  let h_w = 0.5*(x2-x1);
  // Left to right gradient
  push()
  translate(-h_w-x1,0)
  rotate(bRotate * random([0,1,2]) * TWO_PI/3)
  for (let x = x1; x <= x2; x++) {
    let inter = map(x, x1, x2, 0, 1);
    let h = map(x, x1, x2, y3 - y2, 0);
    let c = lerpColor(c1, c2, inter);
    stroke(c);
    line(x, cy - h, x, cy + h);
  }
  pop()
}

function mousePressed() {
  pickPalette();
  background(250)
  // drawTriGrid();
  // bRotate = !bRotate
}


function drawTriGrid() {
    // Grid 
    yOffset = tileSize ;
    for (let y = 0; y < nRows; y++) {
      yRow = y*0.8825*tileSize;
      xOffset = 0.5*tileSize; // 0; // 
      aOffset = 180 * (y%2)
      
      for (let x = 0; x < nCols; x++) {
        // c1 = color(random(0,150), random(0,255),random(0,125)); // color(200,0,0); // 
        // c2 = color(random(128,255),random(0,128), random(0,250)); // color(0,0,200); // 
        c1 = color(random(oPalette.palette))
        c2 = color(random(oPalette.palette))
        
        push()
          translate(xOffset + x*1.5*tileSize, yRow + yOffset);        
          rotate(PI * (x%2 + y%2));
          translate((x%2)*tileSize)
          setTriGradient(0, 0, tileSize, c1, c2);
        pop()
      }
    }
}


function draw_test_poly() {
  // Test de placement des triangles
  c1 = color(random(0,200), random(58,255),random(0,250)); // color(200,0,0); // 
  c2 = color(random(128,255),random(0,128), random(0,250)); // color(0,0,200); // 
  push()
  for (let i = 1; i < 6; i++) {
    push()
    translate(i*1.5*tileSize,2*tileSize)
    rotate(PI*(i%2))
    setTriGradient(0, 0, tileSize, c1, c2);
    pop()
  }
  pop()
}