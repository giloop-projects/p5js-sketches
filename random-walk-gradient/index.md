---
title: Random Tile Walker
description: Une balade aléatoire avec des pavages couleurs douces.
image: random-walk-gradient.jpg
category: op-art
publish: true
tags: [generative, art]
---

# Exploring triangle tilings

A colorful inspirational sketch click and watch. 
The starting point for this sketch is a great [codepen](https://codepen.io/smlsvnssn) from swedish artist/coder Samuel Svensson. I spent a great time on his [blog](https://blog.lhli.net/). There is a nice work on colours palette and [some tools](https://lokeshdhakar.com/projects/color-thief/) to get them.