---
title: Multi-track loops
description: Synchronized loops with parts and mixing GUI
image: image.jpg
publish: false
category: audio
tags: [audio]
---

# Multi-track loops

Play synchronized loops with the ability to mix them. 
