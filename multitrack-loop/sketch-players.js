let arPlayers;
let arSounds;
let isPlaying;
let bLoaded = false;
let arVolume = [];
let arPan = [];

function preload() {
  Tone.loaded().then(function(){
    console.log('Tone loaded');
    Tone.Transport.start();
    
    fetch('./sounds.json').then(response => {
      return response.json();
    }).then(data => {
        let i = 0;
        // console.log(data);
        arSounds = data.song;
        for (let i = 0; i < arSounds.length; i++) {
            const el = arSounds[i];
            arSounds[i].channel = new Tone.Channel(0, 0).toDestination();
            arSounds[i].player = new Tone.Player({url: "data/sounds/" + el.file, loop: true});
            arSounds[i].player.connect(arSounds[i].channel);
        }
        console.log("sounds loaded");
        console.log(arSounds);
        createInterface();

    }).catch(err => {
        // Do something for an error here
        console.log('Something went wrong while reading file list :' + err);
    }); 
  });
}
  
function setup() {
  let cnv = createCanvas(360, 250);
  cnv.mouseClicked(togglePlay);

  // ensure audio is enabled
  Tone.Transport.start();

  isPlaying = false;

  txtColor = color(172, 3, 3);
  txtInfo = 'Chargement en cours ...';
}


  function draw() {
    background(200);
    background(255, 230, 200, 100);
    fill(txtColor);
    textSize(10);
        
    if (bLoaded) {
      for (let i = 0; i < arSounds.length; i++) {
        text(arSounds[i].track, 5, 25+(i+1)*25);
      }
      text("Volume", 100, 25+(arSounds.length+1)*25);
      text("Pan", 120+ 0.3*width, 25+(arSounds.length+1)*25);
      for (let i = 0; i < arSounds.length; i++) {
        arSounds[i].channel.set({volume: map(arVolume[i].value(), 0, 1, -30, 3), pan: arPan[i].value()})
      }
    } 
  
    noStroke();
    text(txtInfo, 10, 20);
  }

  function togglePlay() {
    // ensure audio is enabled
    
    if (isPlaying) {
        // Stop it all
        arSounds.map(el => el.player.stop());
        txtInfo = 'Click to start music';
        console.log("stopping");
        isPlaying = false;
    } else {
      // start the loop
      arSounds.map(el => el.player.start());
      txtInfo = 'Click to stop music';
      console.log("starting");
      isPlaying = true;
    }
  }

  function createInterface() {
    // Création des sliders volume / pan
    for (let i = 0; i < arSounds.length; i++) {
      // Volume
      sl = createSlider(0, 1, 1, 0.01);
      sl.position(100, 10+(i+1)*25 );
      sl.style('width', (0.3*width) + 'px');
      arVolume.push(sl);
      // Pan
      sl = createSlider(-1, 1, 0, 0.1);
      sl.position(120+ 0.3*width, 10+(i+1)*25 );
      sl.style('width', (0.3*width) + 'px');
      arPan.push(sl);   
  }  
  bLoaded = true;
  txtInfo = 'Click to start music';        
}
