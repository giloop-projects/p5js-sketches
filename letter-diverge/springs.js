
let springs = [];
let num = 3; 

function setup() {
  canv = createCanvas(710, 400);
  // canv.mouseMoved(myMouseMoved); // attach listener for activity on canvas only
  noStroke();

    // Create Springs : Spring (_x, _y, _s, _d, _m, _k_in) 
  springs[0] = new Spring(240, 260, 10, 0.95, 2.0, 0.1);
  springs[1] = new Spring(320, 210, 30, 0.95, 4.0, 0.1);
  springs[2] = new Spring(180, 170, 50, 0.95, 8.0, 0.1);
}

function draw() {
  background(51);
    // Apply a force according to spring distance
    for (let i = 0; i < num; i++) {
        springs[i].update();
        springs[i].display();
    }
}

// Spring class
function Spring (_x, _y, _s, _d, _m, _k_in) {
  // Screen values
  this.x_pos = _x;
  this.y_pos= _y;

  this.size = 20;
  this.size = _s;
  
  // Spring simulation constants
  this.mass = _m;       // Mass
  this.k = 0.2;    // Spring constant
  this.k = _k_in;
  this.damp = _d;       // Damping
  this.rest_posx = _x;  // Rest position X
  this.rest_posy = _y;  // Rest position Y

  // Spring simulation variables
  //float pos = 20.0; // Position
  this.velx = 0.0;   // X Velocity
  this.vely = 0.0;   // Y Velocity
  this.accel = 0;    // Acceleration
  this.force = 0;    // Force

  this.update = function() {

    let f = createVector(this.rest_posx - mouseX, this.rest_posy - mouseY);
    let fx = 0;
    let fy = 0;
    
    if (f.mag()<100) {
        fx = 1/max(0.1,f.x);
        fy = 1/max(0.1,f.y);
    } 

	this.force = -this.k * (this.x_pos - this.rest_posx) - fx;  // f=-kx -f_mousex
	this.accel = this.force / this.mass;                 // Set the acceleration, f=ma == a=f/m
	this.velx = this.damp * (this.velx + this.accel);    // Set the velocity
	this.x_pos = this.x_pos + this.velx;                 // Updated position

	this.force = -this.k * (this.y_pos - this.rest_posy) - fy;  // f=-ky -f_mousey
	this.accel = this.force / this.mass;                 // Set the acceleration, f=ma == a=f/m
	this.vely = this.damp * (this.vely + this.accel);    // Set the velocity
	this.y_pos = this.y_pos + this.vely;                 // Updated position

  }

  this.display = function() {
    fill(255);
	ellipse(this.x_pos, this.y_pos, this.size, this.size);
  }

};