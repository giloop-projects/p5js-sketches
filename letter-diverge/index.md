---
title: Dancing Letters
description: Une étude de déformation de lettres à comportement ressort. Une alternative à la méthode flocking présentée dans le Coding Train. Taper une lettre au clavier et faites la bouger avec la souris.
image: image.jpg
category: visual
tags: [text, deform, spring, particles]
---

# Faites bouncer les lettres !

Une étude de déformation de lettres à comportement ressort. Une alternative à la méthode flocking présentée dans le Coding Train. Taper une lettre au clavier et faites la bouger avec la souris 