
let springs = [];
let num = 3; 

function setup() {
  canv = createCanvas(710, 400);
  // canv.mouseMoved(myMouseMoved); // attach listener for activity on canvas only
  noStroke();

    // Create Springs : Spring (_x, _y, _s, _d, _m, _k_in) 
  springs[0] = new Spring(240, 260, 10, 0.9, 2.0, 0.1);
  springs[1] = new Spring(320, 210, 30, 0.9, 2.0, 0.1);
  springs[2] = new Spring(180, 170, 50, 0.9, 2.0, 0.1);
}

function draw() {
  background(51);
    // Apply a force according to spring distance
    for (let i = 0; i < num; i++) {
        springs[i].update();
        springs[i].display();
    }
}


// Spring class
class Spring {
    constructor(_x, _y, _s=20, _d=0.9, _m=1, _k_in=0.2) {
  
      // Spring simulation constants
      this.mass = _m;       // Mass
      this.k = _k_in;       // Spring constant
      this.damp = _d;       // Damping
      this.rest_pos = createVector(_x, _y); // Rest position X
      this.size = _s; // circle size
  
      // Spring simulation variables
      this.pos = createVector(_x, _y); // Current position 
      this.vel = createVector(0,0);    // Velocity
      this.accel = createVector(0,0);  // Acceleration
      this.force = createVector(0,0);  // Force
    }
  
    update () {
  
      //let f = createVector(this.rest_pos.x - mouseX, this.rest_pos.y - mouseY);
      let f = createVector(mouseX - this.pos.x, mouseY - this.pos.y);
      let frepuls = createVector(0,0);
      
      if (f.mag()<200) {
        frepuls.set( 
            f.x/max(.1,f.mag()), 
            f.y/max(.1,f.mag()) 
        );
      } 
  
      // SUM(F) = 0 -> f = -k -f_mouse
      this.force = p5.Vector.sub(this.pos, this.rest_pos);  
      this.force.mult(-this.k);
      this.force.sub(frepuls);
      // Set the acceleration, f=ma == a=f/m
      this.accel = p5.Vector.div(this.force, this.mass);                 
      // Set the velocity
      this.vel.add(this.accel); 
      this.vel.mult(this.damp);
      // Updated position 
        this.pos.add(this.vel);                     
  
      // this.force = -this.k * (this.y_pos - this.rest_posy) - fy;  // f=-ky -f_mousey
      // this.accel = this.force / this.mass;                 // Set the acceleration, f=ma == a=f/m
      // this.vely = this.damp * (this.vely + this.accel);    // Set the velocity
      // this.y_pos = this.y_pos + this.vely;                 // Updated position
  
    }
  
    display() {
        fill(255,128,0);
      ellipse(this.rest_pos.x, this.rest_pos.y, 2, 2);
      fill(255,255,255);
        ellipse(this.pos.x, this.pos.y, this.size, this.size);
        
    }
  
  };

// Spring class
function Spring2 (_x, _y, _s, _d, _m, _k_in) {
  // Screen values
  this.x_pos = _x;
  this.y_pos= _y;

  this.size = 20;
  this.size = _s;
  
  // Spring simulation constants
  this.mass = _m;       // Mass
  this.k = 0.2;    // Spring constant
  this.k = _k_in;
  this.damp = _d;       // Damping
  this.rest_posx = _x;  // Rest position X
  this.rest_posy = _y;  // Rest position Y

  // Spring simulation variables
  //float pos = 20.0; // Position
  this.velx = 0.0;   // X Velocity
  this.vely = 0.0;   // Y Velocity
  this.accel = 0;    // Acceleration
  this.force = 0;    // Force

  this.update = function() {

    let f = createVector(this.rest_posx - mouseX, this.rest_posy - mouseY);
    let fx = 0;
    let fy = 0;
    
    if (f.mag()<200) {
        fx = 1/max(0.1,f.x);
        fy = 1/max(0.1,f.y);
        /*if (f.x > 0.0) {
            fx = 1.0 / max(0.1, f.x);
        } else {
            fx = 1.0 / min(-0.1, f.x);
        }
        if (f.y > 0.0) {
            fy = 1.0 / max(0.1, f.y);
        } else {
            fy = 1.0 / min(-0.1, f.y);
        }*/
    } 

	this.force = -this.k * (this.x_pos - this.rest_posx) - fx;  // f=-kx -f_mousex
	this.accel = this.force / this.mass;                 // Set the acceleration, f=ma == a=f/m
	this.velx = this.damp * (this.velx + this.accel);    // Set the velocity
	this.x_pos = this.x_pos + this.velx;                 // Updated position

	this.force = -this.k * (this.y_pos - this.rest_posy) - fy;  // f=-ky -f_mousey
	this.accel = this.force / this.mass;                 // Set the acceleration, f=ma == a=f/m
	this.vely = this.damp * (this.vely + this.accel);    // Set the velocity
	this.y_pos = this.y_pos + this.vely;                 // Updated position

  }

  this.display = function() {
    fill(255);
	ellipse(this.x_pos, this.y_pos, this.size, this.size);
  }

};