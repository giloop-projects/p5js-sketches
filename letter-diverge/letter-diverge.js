let font;
function preload() {
  font = loadFont('../assets/fonts/Aileron-Thin.otf');
}

let springs = [];
let points;
let letter = "G ";
let bViewMouseField = false;
let bViewHelp = true;
let txtSize = 250;

function windowResized() {
  resizeCanvas(min(500, 0.99*windowWidth), 500);
  txtSize = map(width,200,2000,200,500);
  updateLetter(letter);
} 

function setup() {
  canv = createCanvas(0.75*windowWidth, 0.75*windowHeight);
  noStroke();

  windowResized();
  // Création des points pour le texte
  updateLetter(letter);
}

function updateLetter(myText) {
    points = font.textToPoints(myText, 0.35*width, 0.65*height, txtSize, {
        sampleFactor: 2,
        simplifyThreshold: 0
    });
    
      // Création/Maj des ressorts sur les points de la lettre
      springs =  [];
      for (p of points) {
        springs.push(new Spring(p.x, p.y, 10, 0.85, 2, 0.1));
      }
    
}

function draw() {
  background(51,51,51,100);
    // Apply a force according to spring distance
    for (s of springs) {
        s.update();
        s.display();
    }
    if (bViewMouseField) {
      // View mouse field of action
      noFill();
      stroke(255,128,0);
      ellipse(mouseX, mouseY, 200, 200);
    }
    if (bViewHelp) {
      textSize(12);
      noStroke();
      fill(255,128,0);
      text('Type a key to change letter', 5,20);
      fill(0,255,128);
      text('Spacebar to toggle mouse field', 5,height-20 );
      text('Enter to toggle help text', 5, height-5 );
    }

}

function keyTyped() {
  if (keyCode>50 && keyCode <188) {
      letter = key;
      updateLetter(key);
  }
  if (key == ' ') {
    bViewMouseField = ! bViewMouseField; 
  }
  if (keyCode == 13) {
    bViewHelp = ! bViewHelp; 
  }
  // console.log(keyCode)
}

// Spring class
class Spring {
  constructor(_x, _y, _s=20, _d=0.9, _m=1, _k_in=0.2) {

    // Spring simulation constants
    this.mass = _m;       // Mass
    this.k = _k_in;       // Spring constant
    this.damp = _d;       // Damping
    this.rest_pos = createVector(_x, _y); // Rest position X
    this.size = _s; // circle size

    // Spring simulation variables
    this.pos = createVector(_x, _y); // Current position 
    this.vel = createVector(0,0);    // Velocity
    this.accel = createVector(0,0);  // Acceleration
    this.force = createVector(0,0);  // Force
  }

  update () {

    let f = createVector(mouseX - this.pos.x, mouseY - this.pos.y);
    let frepuls = createVector(0,0);
    
    if (f.mag()<200) {
      frepuls.set( 
          5.0*f.x/max(.1,f.mag()), 
          5.0*f.y/max(.1,f.mag()) 
      );
    } 

    // SUM(F) = 0 -> f = -k -f_mouse
    this.force = p5.Vector.sub(this.pos, this.rest_pos);  
    this.force.mult(-this.k);
    this.force.sub(frepuls);
    // Set the acceleration, f=ma == a=f/m
	  this.accel = p5.Vector.div(this.force, this.mass);                 
    // Set the velocity
    this.vel.add(this.accel); 
    this.vel.mult(this.damp);
    // Updated position 
	  this.pos.add(this.vel);
  }

  display() {
    fill(255,255,255);
    noStroke();
	  ellipse(this.pos.x, this.pos.y, this.size, this.size);
  }

};