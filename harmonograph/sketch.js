var pt, ptPrev;
var t;
var dt;
var speed;
let bgColor, lineColor, textColor; 
let pendul1, pendul2;
let cb_phi, cb_damp;
let thick = 1;    
const arConfig = [
  [ {a:[0.8, 0.5], f:[0.5, 0.5], d:[0.05, 0.025], phi:[1.25, 0]},
    {a:[0.2, 0.5], f:[0.5, 0.5], d:[0.05, 0.01], phi:[Math.PI/2, 0]} ],
  [ {a:[1, 0.55], f:[0.5, 3], d:[0.05, 0.05], phi:[0, 0]},
    {a:[0.25, 0.5], f:[1, 0.5], d:[0.05, 0.01], phi:[0, 1.5]} ],
  [ {a:[1, 0.55], f:[0.5, 0.5], d:[0.05, 0.025], phi:[0, 0]},
    {a:[0.25, 0.5], f:[0.5, 0.5], d:[0.05, 0.01], phi:[0, 1.5]} ],
  [ {a:[1, 0.55], f:[2, 3], d:[0.05, 0.05], phi:[0, 0]},
    {a:[0, 0.5], f:[1, 1], d:[0.05, 0.1], phi:[0, 1.5]} ],
  [ {a:[0.45, 0.3], f:[4, 3], d:[0.05, 0.05], phi:[0.1, 0.1]},
    {a:[0.37, 0.31], f:[1, 2], d:[0.07, 0.05], phi:[0.1, 0.2]} ],
  [ {a:[0.5, 0.25], f:[3, 1], d:[0.05, 0.05], phi:[0, 0]},
    {a:[0.5, 0.25], f:[5, 1], d:[0.2, 0.05], phi:[1.5, 0]} ],
  [ {a:[0.25, 0.5], f:[3, 1], d:[0.05, 0.05], phi:[0, 0]},
    {a:[0.25, 0.25], f:[5, 1], d:[0.2, 0.05], phi:[0, 0]} ],
  [ {a:[0.25, 0.5], f:[1, 1], d:[0.01, 0.01], phi:[0, 0]},
    {a:[0.25, 0.5], f:[1, 4], d:[0.02, 0.05], phi:[0, 0]} ],
  [ {a:[0.75, 0], f:[1, 0], d:[0.01, 0.01], phi:[Math.PI/2, Math.PI]},
    {a:[0, 0.4], f:[0, 4], d:[0.01, 0.021], phi:[Math.PI*0, Math.PI]} ],
  [ {a:[0.25, 0.25], f:[3, 3], d:[0.02, 0.01], phi:[Math.PI*0, Math.PI*0]},
    {a:[0.75, 0.75], f:[1, 1], d:[0.02, 0.01], phi:[Math.PI*0, Math.PI]} ],
  [ {a:[0.5, 0.5], f:[3, 1], d:[0.025, 0.02], phi:[Math.PI/2, Math.PI*0]},
    {a:[0.5, 0.5], f:[1, 4], d:[0.025, 0.02], phi:[Math.PI*0, Math.PI/2]} ],
  [ {a:[0.8, 0.5], f:[0.25, 0.5], d:[0.001, 0.024], phi:[3.11, 1.94]},
    {a:[0.2, 0.5], f:[0.25, 7], d:[0.05, 0.01], phi:[Math.PI/2, 0]} ],
];
let idxConfig = arConfig.length-1;

// Simple XY oscillator class
// x = Ax * exp(-dx * t) * sin(2pi*fx*t + phix)
// y = Ay * exp(-dy * t) * sin(2pi*fy*t + phiy)
class Pendulum {
  // Constructor 
  constructor(a, f, d, phi) {
    this.t = 0;

    if (a===undefined) {
      this.a = [random(1), random(1)];
    } else {
      this.a = a;
    }
      
    if (f===undefined) {
      this.f = [random(100), random(100)];
    } else {
      this.f = f;
    }
    
    if (d===undefined) {
      this.d = [0.5+random(0.5), 0.5+random(0.5)];
    } else {
      this.d = d;
    }
    
    if (phi===undefined) {
      this.phi = [random(2*Math.PI), random(2*Math.PI)];
    } else {
      this.phi = phi;
    }

    this.moveDirs = {a:[1,1], f:[1,1], d:[1,1], phi:[1,1]}
    this.moveOffsets = {a:[0,0], f:[0,0], d:[0,0], phi:[0,0]}

  }
  
  getPosition(idx, t) {
    return(
      (this.a[idx]+this.moveOffsets.a[idx]) * 
      Math.exp(-(this.d[idx]+this.moveOffsets.d[idx])*t) * 
      Math.sin(2*Math.PI * (this.f[idx]+this.moveOffsets.f[idx]) * t + 
                this.phi[idx] + this.moveOffsets.phi[idx])
    )
  }

  getVector(t) {
    return(createVector(this.getPosition(0,t), this.getPosition(1,t)))
  }

  moveParam(name, idx, lowVal, upVal, step) {
    // Update direction
    if (this[name][idx]+this.moveOffsets[name][idx] < lowVal) { this.moveDirs[name][idx] = 1; }
    if (this[name][idx]+this.moveOffsets[name][idx] > upVal) { this.moveDirs[name][idx] = -1; }
  
    // Update offset value
    this.moveOffsets[name][idx] += this.moveDirs[name][idx] * step
    // console.log(name+"/"+idx+": offset="+this.moveOffsets[name][idx])
  }

  getInfo() {
    // .map((v, i) => v + arr2[i])
    return("A="+ this.a.map((el,idx) => round(el+this.moveOffsets.a[idx], 3)).toString() + 
    ", f="+this.f.map((el,idx) => round(el+this.moveOffsets.f[idx], 3)).toString() + 
    ", phi="+ this.phi.map((el,idx) => round(el+this.moveOffsets.phi[idx], 3)).toString() + 
    ", damp="+ this.d.map((el,idx) => round(el+this.moveOffsets.d[idx], 3)).toString());
  }
}    

function reconfig() {
  idxConfig = (idxConfig+1) % arConfig.length;

  for (const[k,v] of Object.entries(arConfig[idxConfig][0])) { 
    pendul1[k] = arConfig[idxConfig][0][k];
    pendul2[k] = arConfig[idxConfig][1][k];
  }
}


function setup() {
  createCanvas(0.94*windowWidth, 0.7 * windowHeight);
  bgColor = color("#581388");
  lineColor = color("#F1E9DABB");
  textColor = color("#F1E9DAFF");
  cb_phi = createCheckbox('Phase evolves', false)
  cb_damp = createCheckbox('Damp evolves', false)

  background(bgColor);
  stroke(lineColor);

  dimMin = 0.475*Math.min(width, height);
  // Paramètres : a, f, d, phi
  pendul1 = new Pendulum([1, 0.55], [2, 3], [0.1, 0.1], [0, 0]);
  pendul2 = new Pendulum([0, 0.5], [1, 1], [0.01, 0.01], [0, 1.1]);
  // a = [ 0.45, 0.13, 0.27, 0.21];
  // a = [ 0.3*dimMin, 100, 100, 1000];
  // w = [1.5, 1, 4, 1];
  // p = [0.3, 0.4,-0.1,0.9]; // [1,2,3,4]; // .map(el => random(2*Math.PI));
  // d = 0.9998;
  t = 0;
  dt = 0.01;
  reconfig();
}

function draw() {
  background(bgColor);
  showParams();
  stroke(lineColor);
  noFill();
  t = 0
  // x(t) = Ax(t) sin(wx t + px) + As(t) sin(ws t + ps)
  // y(t) = Ay(t) sin(wy t + py)
  // A(t) = A(t-1) (1 - d)

  // Mise à l'échelle du canvas
  // centrage
  push();
  translate(width/2, height/2);
  
  beginShape();
  for (var n=0; n<10000; n++) {
    pt = pendul1.getVector(t).add(pendul2.getVector(t)).mult(dimMin)
  
    // if(t !== 0) {
    //   line(ptPrev.x, ptPrev.y, pt.x, pt.y);
    // }
    curveVertex(pt.x, pt.y);

    ptPrev = pt.copy();
    
    t = t + dt;
  }
  endShape();
  pop();

  if (cb_phi.checked()) {
    pendul1.moveParam("phi", 0, 0, Math.PI, 0.01)
    pendul1.moveParam("phi", 1, 0.5, Math.PI, 0.01)  
  }

  if (cb_damp.checked()) {
    pendul1.moveParam("d", 0, 0.001, 0.05, 0.0005)
    pendul1.moveParam("d", 1, 0.001, 0.05, 0.0005)  
  }

}

function showParams() {
// Params infos
noStroke();
fill(textColor);
textSize(10);
text(pendul1.getInfo(), 5, 15);
text(pendul2.getInfo(), 5, 25);
text("dt="+dt, 5, 35);
}


function keyPressed() {
  if (key === '+') { thick += 1; strokeWeight(thick); } 
  if (key === '-') { thick -= 1; thick = max(1, thick); strokeWeight(thick); } 
  if (key === ' ') {  reconfig(); }
  if (key === 'c') { 
    // Change colors 
    const oPalette = random(arPalettes);
    bgColor = color(random(oPalette.palette));
    lineColor = color(random(oPalette.palette));
    textColor = color(lineColor);
    lineColor.setAlpha(175);
  }
}