---
title: Harmonograph
description: A p5js Harmonograph, will draw hypnotic graphs to lissajous you.
image: harmonograph_02.jpg
category: visual
publish: true
tags: [generative, art, visual]
---

# L'harmonographe

L'harmonographe est à l'orgine une machine "à dessiner" qui utilise 2 pendules asynchrones 
pour créer des oscillations hypnotiques. Ce sketch en propose une implémentation avec quelques 
exemples et la possibilité de faire varier les paramètres. 