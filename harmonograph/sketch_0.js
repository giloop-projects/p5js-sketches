// x(t) = Ax(t) sin(wx t + px) + As(t) sin(ws t + ps)
// y(t) = Ay(t) sin(wy t + py)
// A(t) = A(t-1) (1 - d)
var a = [], a0 = [];
var f = [], f0 = [];
var p = [], p0 = [];
var d = [], d0 = [];
var x, y;
var px, py;
var t;
var dt;
var speed;
var bgColor;
var lineColor;
var i;
var drawing = false;
var myFrameCount = 0;
let arPalette = ["#264653", "#2a9d8f", "#e9c46a", "#f4a261", "#e76f51"];
var elInfo = document.getElementById('info');

function pickPalette() {
  if (random()>0.5) {
    // ligth line on dark bg
    bgColor = color(random(arPalette.slice(0,2)));
    lineColor = color(random(arPalette.slice(2)));
    lineColor.setAlpha(128);
  } else {
    // dark line on light bg
    bgColor = color(random(arPalette.slice(2)));
    lineColor = color(random(arPalette.slice(0,2)));
    lineColor.setAlpha(128);
  }
}

function setup() {
  createCanvas(windowWidth, 0.8 * windowHeight);
  pickPalette();
  init();
}

function draw() {
  
  if(!drawing) { 
      return;
  } else {
  background(bgColor);
  addNoise();
  drawIt();
  myFrameCount++;
}
}

function drawIt() {
  stroke(lineColor);
  push();
  translate(width/2, height/2);
  t = 0;
  for(i = 0; i < 5000; i++) {
    x = a[0] * Math.sin(t * f[0] + p[0]) * Math.exp(-d[0] * t) + a[1] * Math.sin(t * f[1] + p[1]) * Math.exp(-d[1] * t);
    y = a[2] * Math.sin(t * f[2] + p[2]) * Math.exp(-d[2] * t) + a[3] * Math.sin(t * f[3] + p[3]) * Math.exp(-d[3] * t);
    
    if(t !== 0) {
      line(px, py, x, y);
    }
    px = x;
    py = y;
    t += dt;
  }
  pop();

// Log INfo page
elInfo.innerHTML = "<ul><li>a = "+ a.map(el => round(el, 1)).toString()+"</li>"+
"<li>f = "+ f.map(el => round(el, 1)).toString()+"</li>"+
"<li>p = "+ p.map(el => round(el, 1)).toString()+"</li></ul>";
}

function init() {
  
  speed = 5;
  dt = speed / 400.0;
  var smallDim = min(width, height);

  // a = [ 135.4,121.6,146.9,115];
  // f = [1,1,1,0.9];
  // p = [4.6,5.2,0.1,0.1];
  // d = [0.0005, 0.01];

  // a0 = a;
  // f0 = f;
  // p0 = p;
  // d0 = d;
  for(var i = 0; i < 4; i++) {
    a0[i] = random(smallDim/6, smallDim/4);
    f0[i] = random(0.95, 1);
    p0[i] = random(2*PI);
    d0[i] = random(0.0005, 0.01);
    
    a[i] = a0[i];
    f[i] = f0[i];
    p[i] = p0[i];
    d[i] = d0[i];
  }

}

function addNoise() {
  for(i = 0; i < 4; i++) {
    a[i] = a0[i] + getNoise(50, myFrameCount / 500.0 + 1000*i);
    f[i] = f0[i] + getNoise(0.1, myFrameCount / 500.0 + 2000*i);
    // p[i] = p0[i] + getNoise(2*PI/100, myFrameCount / 1000.0 + 2000*i);;
    // d[i] = d0[i] + getNoise(0.001, myFrameCount / 100.0 + 4000*i);
  }
}


function getNoise(amp, t) {
  return amp * (noise(t) - 0.5)
}


function mouseClicked() {
  pickPalette();
  init();
  drawIt();
}

function keyTyped() {
  if(key == 'p') {
    drawing = !drawing;
  }
}
