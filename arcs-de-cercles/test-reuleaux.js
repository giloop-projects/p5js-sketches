/** Test de l'objet Cercle, création d'un triangle de Reuleaux
 * @author Gilles Gonon 
 * @date 2021/05/27
 * */

let h,a,r; // hauteur du triangle, longueur d'un côté, rayon
let oC1, oC2, oC3; // Déclarés ici pour débug console

function setup() {
    createCanvas(0.9*windowWidth, 0.8*windowHeight);
    h = 0.25*min(height,width);
    a = h * 2/sqrt(3);
    r = 2/3*h;
  }
  
  function draw() {
    // Résolution du cercle passant par 3 points
    let p1 = createVector(0,r);
    let p11 = createVector(0,r-a);
    let p2 = createVector(r*cos(-PI/6),r*sin(-PI/6));
    let p21 = createVector((r-a)*cos(-PI/6),(r-a)*sin(-PI/6));
    let p3 = createVector(-p2.x,p2.y); // Eviter y1=y3
    let p31 = createVector(-p21.x,p21.y);
    // Cercles de Treuleaux
    oC1 = new Cercle(p2.x, p2.y, p1.x, p1.y , p31.x, p31.y);
    oC2 = new Cercle(p2.x, p2.y, p11.x, p11.y , p3.x, p3.y);
    oC3 = new Cercle(p21.x, p21.y ,p1.x, p1.y , p3.x, p3.y);
    
    // Dessin
    background(240)
    noStroke(); fill(128);
    translate(.5*width,0.5*height)
    // Axes 
    noFill(); stroke(128); strokeWeight(1);
    line(-width*0.5, 0, width*0.5, 0)
    line(0, -height*0.5, 0, height*0.5)
    // Triangle équilatéral
    stroke(0);
    line(p1.x, p1.y, p2.x, p2.y)
    line(p3.x, p3.y, p2.x, p2.y)
    line(p1.x, p1.y, p3.x, p3.y)
    // Points fournis
    strokeWeight(10);
    stroke(200,0,0); point(p1); point(p2); point(p3);
    stroke(100,100,100); point(p11); point(p21); point(p31);
    noFill(); strokeWeight(1); text('p1',p1.x+10, p1.y); text('p11',p11.x+10, p11.y)
    text('p2',p2.x+10, p2.y); text('p21',p21.x+10, p21.y)
    text('p3',p3.x+10, p3.y); text('p31',p31.x+10, p31.y)
    
    // Dessin des cercles
    noFill(); stroke(128,0,0); strokeWeight(2);
    stroke(128,0,0); oC1.draw()
    stroke(0,128,0); oC2.draw()
    stroke(0,0,128); oC3.draw()
    // Dessin des arcs de cercle
    noFill(); strokeWeight(4);
    stroke(128,0,128); oC1.drawArc(p2, p1);
    stroke(128,0,128); oC2.drawArc(p3, p2);
    stroke(128,0,128); oC3.drawArc(p1, p3);
  
    noLoop();
  }
  
  