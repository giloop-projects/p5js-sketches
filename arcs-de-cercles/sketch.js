/** P5js version of "Arcs de cercles" by Gilles Gonon
 * https://www.geogebra.org/classic/EAprZaHj
 * Possibilty to export in SVG
 * @author Gilles Gonon
 * 
*/

let rExt;
let rInt;
let nCercles, nInner;
let bViewAxes;
let myCanvas;


document.getElementById('ArcForm').addEventListener('submit', (e) => {
  e.preventDefault();
});


function setup() {
  if (document.URL.search('arc-svg')>0) {
      myCanvas = createCanvas(0.99*windowWidth, 0.7*windowHeight, SVG);
  } else {
      myCanvas = createCanvas(0.99*windowWidth, 0.7*windowHeight);
  }

  updateSliders();
}

function draw() {
  // Résolution du cercle passant par 3 points
  let rot = TWO_PI / nCercles;
  let p1 = createVector(rInt,0);
  let p2 = createVector(rInt*cos(rot),-rInt*sin(rot));
  let p3 = createVector(-rExt,0);
  let oC = new Cercle(p1.x, p1.y ,p2.x, p2.y ,p3.x, p3.y);
  
  let ocIn = [];
  
  let pNext = createVector(oC.x*cos(rot)-oC.y*sin(rot), oC.x*sin(rot)+oC.y*cos(rot))
  let dStep = createVector((oC.x-pNext.x)/nInner, (oC.y-pNext.y)/nInner)
  for (let k = 0; k < nInner; k++) {
    let v = createVector(oC.x,)
    ocIn.push(new Cercle(p1.x, p1.y , oC.x+(k+1)*dStep.x, oC.y+oC.r-(k+1)*dStep.y,p3.x, p3.y))
  }
  // Dessin debug 
  background(240)
  translate(.5*width,0.5*height); // Center drawing

  if (bViewAxes) {
    stroke(128); noFill(); strokeWeight(1);
    // text('(x,y)='+oC.x.toPrecision(4)+','+oC.y.toPrecision(4)+', r='+oC.r.toPrecision(4)+', nInner='+nInner, 20-width/2, 20-height/2);
    line(-width*0.5, 0, width*0.5, 0)
    line(0, -height*0.5, 0, height*0.5)
    circle(0,0,2*rExt);circle(0,0,2*rInt);
    circle(oC.x,oC.y,2);
    strokeWeight(10);
    point(p1); point(p2); point(p3);  
  }

  // Dessin des cercles
  noFill()
  for (let c = 0; c < nCercles; c++) {
    push()
    rotate(c*rot);
    
    stroke(128,0,0); strokeWeight(2);
    // Inner circles
    for (let j = 0; j < ocIn.length; j++) {
      ocIn[j].drawArc(p1, p3)
    }
   pop()
  }
  for (let c = 0; c < nCercles; c++) {
      push()
      rotate(c*rot);
      // Outer circles 
      stroke(0); strokeWeight(4);
      oC.drawArc(p2, p3);
      pop()
    }
  }

function updateSliders() {
  rInt = Number(document.getElementById('rInt').value)
  rExt = Number(document.getElementById('rExt').value) 
  nCercles = Number(document.getElementById('slCercles').value); 
  nInner = Number(document.getElementById('slInner').value); 
  bViewAxes = document.getElementById('cbAxes').checked
}

function saveImage() {
  let d = new Date();
  if (document.URL.search('arc-svg')>0) {
      save('Arcs-'+d.toISOString().replaceAll(':','').slice(0,17), 'svg');
  } else {
      saveCanvas(myCanvas, 'Arcs-'+d.toISOString().replaceAll(':','').slice(0,17), 'png');
  }
}