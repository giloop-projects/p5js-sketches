// Test de l'objet Cercle
// Gilles Gonon

function setup() {
  createCanvas(700,700);
}

function draw() {
  // Résolution du cercle passant par 3 points
  let p1 = createVector(50,0);
  let pMouse = createVector(mouseX-width/2, mouseY-height/2);
  let p3 = createVector(-200,0); // Eviter y1=y3
  let p12 = createVector(0,75);
  let p32 = createVector(0,-200);
  // Cercle 1
  let oC1 = new Cercle(p1.x, p1.y ,pMouse.x, pMouse.y ,p3.x, p3.y);
  // Cercle 2 (tracé arc de cercle)
  let oC2 = new Cercle(p12.x, p12.y ,pMouse.x, pMouse.y ,p32.x, p32.y);
  
  // Dessin
  background(240)
  // Trace debug
  noStroke(); fill(128);
  text('(x,y)='+oC1.x.toPrecision(4)+','+oC1.y.toPrecision(4)+', r='+oC1.r.toPrecision(4), 20, 20);
  translate(.5*width,0.5*height)
  // Axes 
  noFill(); stroke(128); strokeWeight(1);
  line(-width*0.5, 0, width*0.5, 0)
  line(0, -height*0.5, 0, height*0.5)
  // Points fournis
  strokeWeight(10);
  stroke(200,0,0); point(p1); point(p3);
  stroke(0,0,200); point(p12); point(p32);
  stroke(0,200,0); point(pMouse); 
  // Dessin des cercles
  noStroke(); fill(128,0,0); text('Cercle', oC1.x-10,oC1.y-6)
  noFill(); stroke(128,0,0); strokeWeight(2);
  oC1.draw()
  // Dessin de l'arc de cercle entre 2 points
  noStroke(); fill(0,0,128);text('Arc', p32.x-5, p32.y+15)
  noFill(); stroke(0,0,128); strokeWeight(2);
  // Inverser les points selon vos besoins
  if (pMouse.x>0) {
    oC2.drawArc(p32, p12);
  } else {
    oC2.drawArc(p12,p32);
  }

}

