/** Une classe de Cercle pour :
 * - instancier un cercle à partir de 3 points
 * - tracer le cercle complet
 * - tracer un arc de cercle entre 2 angles ou 2 points */

class Cercle {
    constructor(x1,y1,x2,y2,x3,y3) {
      // Construction d'un cercle à partir de 3 points
      // Éviter les cas où y2=y1 & y3=y2 → NaN
      this.x = 0.5*( ((x3**2-x2**2+y3**2-y2**2)/(y3-y2)) - ((x2**2-x1**2+y2**2-y1**2)/(y2-y1)) ) / ( -(x2-x1)/(y2-y1)+(x3-x2)/(y3-y2) );
      this.y = -this.x*(x2-x1)/(y2-y1) + 0.5*(x2**2-x1**2+y2**2-y1**2)/(y2-y1);
      this.r = sqrt((x1-this.x)**2 + (y1-this.y)**2);
    }
  
    draw() {
      circle(this.x, this.y, 2*this.r);
    }
  
    drawArc(start,stop) {
        let arcStart = start;
        let arcStop = stop;
        if (typeof(start)!="number") {
            arcStart = atan2(start.y-this.y, start.x-this.x );
            arcStop = atan2(stop.y-this.y, stop.x-this.x )
        } 
        arc(this.x, this.y, 2*this.r, 2*this.r, arcStart, arcStop);
    }
    drawArcAbs(xstart, xstop) {

    }
    drawArcAbs(ystart, ystop) {

    }
  }
  