---
title: Arcs de cercle
description: Les arcs de cercle, c'est comme les spaghettis, c'est plus beau quand ça s'entrelace.
image: arcs-de-cercles.jpg
category: geometry
tags: [generative, art, visual]
---

# Op Art

This sketch is a reproduction of the Geogebra example Arcs de Cercles
by Patrick Clément. If you are insterest in Op-Art, you will find infinite
resources on his Geogebra page, as well as on Christian Mercat.
