var elastics = [];
var i;
var j;
var fft, song, amplitude;
var fftBands = [0,25, 80, 100, 130, 200, 400, 500];
var trigged;
var red, blue, yellow;
var xOffset, yOffest;
var bLoaded;
var spectrum = [];
var sign = [];
var button;
    
function setup() {
  hCanvas = createCanvas(0.94*windowWidth, 0.7 * windowHeight); 
  hCanvas.parent("canvas");
  
  // Add Elastic objects
  for (i=0;i<20; i++) {
    var e = new Elastic();
	e.init(width*random(0.1,0.6), height*random(0.2,0.8));
	e.c = chooseColor(i);
	elastics.push(e);  
	// Dancing direction
	sign.push(round(random(0,1))*2 - 1); // +1 or -1
  }
  // Load song 
  bLoaded = false;
  song = loadSound('Animale KO2 v2.mp3', startSong); // Gilou - Danser Petits pieds.mp3'); // 
  // Init sound vars
  fft = new p5.FFT(0.6, 1024);
  amplitude = new p5.Amplitude();  
  trigged = false;
  spectrum = fft.analyze();
  for (i=0; i<spectrum.length; i++) {
	spectrum[i] = 200;
  }
  
  // Other paramaters, text, colors, positions
  textFont("Helvetica");
  textSize(16);
  red = chooseColor(2);
  yellow = chooseColor(1);
  blue =  chooseColor(0);
  xOffset = width-300;
  yOffset = height*0.6;
}

function startSong() {
	bLoaded = true;
	song.loop();
	button.label = 'Pause';
}

// Add an elastic on mouse event (or touch)
function mouseReleased() {
  if (mouseX < 150 && mouseY < 100) {
    if (song.isPlaying()) {
	  song.pause();
    } else { 
      song.play();
    }
  } else {
	var e = new Elastic();
	e.init(mouseX, mouseY);
	elastics.push(e); 
	sign.push(round(random(0,1))*2 - 1); // +1 or -1
  }
}

function touchEnded() {
  if (mouseX < 150 && mouseY < 100) {
    if (song.isPlaying()) {
	  song.pause();
    } else { 
      song.play();
    }
  } else {
	var e = new Elastic();
	e.init(mouseX, mouseY);
	elastics.push(e); 
	sign.push(round(random(0,1))*2 - 1); // +1 or -1
  }
}

function draw() {
  background(0);
  
  // Sound analysis (first layer)
  if (song.isPlaying()) {
	spectrum = fft.analyze(); 
  } else {
		for (i=0;i<spectrum.length;i++) { 
			spectrum[i] *=  0.95;
		}
		
  }
  var level = amplitude.getLevel();
  
  // Show spectrum
  strokeWeight(1);
  noFill();
  stroke(chooseColor(i));
  // Show frequencies
  for (i=0;i<90;i++) { 	
	stroke(chooseColor(i/30));  
	line( 10*i, height-spectrum[i]/2, 10*i, height);
  }
  // Show some freq bands
  for (i=0;i<fftBands.length;i++) { 
	stroke(chooseColor(i));
	hRect = fft.getEnergy(fftBands[i])/2;
	rect( (i+0.1)*width/fftBands.length, height-hRect, 0.8*width/fftBands.length, hRect );
  }
  
  // Bump elastics on music, 
  // eventually works better with amplitude
  /* var trigLevel = 0.3;  
  if (level > trigLevel) {
	for (i = 0; i < elastics.length; i++) {
      elastics[i].bump(round(random(-1.5,1.5))*level*300);
    }
	trigged = true;
  }*/
  var trigLevel = 125; 
  var iBand = 80;
  var fftLevel = spectrum[3]/2; // fft.getEnergy(fftBands[2])/2;
  if (fftLevel > trigLevel&& trigged===false) {
	for (i = 0; i < elastics.length; i++) {
	  elastics[i].bump(sign[i]*fftLevel);
	  //trigged = true;
    }	
  }
   /*if (spectrum[iBand] > trigLevel) { //  && trigged===false) {
    for (i = 0; i < elastics.length; i++) {
	  elastics[i].bump(sign[i]*spectrum[iBand]);
    }
	//trigged = true;
  }*/
  
  /* detrig */
  if (fftLevel < trigLevel) { // (level<trigLevel) { // 
	trigged = false;
  }
  
  // Info Text
  textSize(13);
  noStroke();
  fill(red);
  if (bLoaded===true) {
	text("clic/touch here to toggle music", 2, 14); 
  } else {
	text("song is loading", 2, 14); 
	line(110, 14, max(110,(120 + millis()/20)%width), 14);
  }
  fill(yellow);
  text("click anywhere else to add a shape",2,30);
  
  // "Les trans" text
  fill(255);
  stroke(255);
  textSize(16);
  
  text("36e RENC   NTRES", xOffset, yOffset); 
  text("3-7 DEC. 2   14", xOffset, yOffset+90); 
  textSize(38);
  text("MUSICALES", xOffset, yOffset+40); 
  
  stroke(red);
  fill(red);
  text("TRANS", xOffset-130, yOffset+40); 
  textSize(20);
  text("DE RENNES", xOffset, yOffset+70); 
  stroke(yellow);
  fill(yellow);
  // Bump the 'O'
  ellipse(xOffset+83, yOffset-6, level*20, level*20);
  ellipse(xOffset+86, yOffset+84, level*20, level*20);
  
  // Draw Elastics at last (above)
  for (i=0;i<elastics.length; i++) {
	elastics[i].update();
	elastics[i].display();
  }
  
}

function keyReleased()
{
  if (key==='p' && bLoaded===true) {
    if (song.isPlaying()) {
      song.pause();
    } else { 
      song.play();
    }
  }
}

// Spring Class - Nature of Code 2011
// Daniel Shiffman
// Chapter 3: Oscillation
var Spring = function(a, b, l) { 
  this.pt1 = a;
  this.pt2 = b;
  // Rest length and spring constant
  this.len = l;
  this.k = 0.5;
};

// Class to describe an anchor point that can connect to "Mover" objects via a spring
// Thank you: http://www.myphysicslab.com/spring2d.html

// Calculate spring force
Spring.prototype.update = function() {
    // Vector pointing from anchor to mover position
    var force = p5.Vector.sub(this.pt1.position, this.pt2.position);
    // What is distance
    var d = force.mag();
    // Stretch is difference between current distance and rest length
    var stretch = d - this.len;

    // Calculate force according to Hooke's Law
    // F = k * stretch
    force.normalize();
    force.mult(-1 * this.k * stretch);
    this.pt1.applyForce(force);
    force.mult(-1);
    this.pt2.applyForce(force);
 };

Spring.prototype.display = function() {
	this.pt1.display();
    this.pt2.display();
    strokeWeight(2);
    stroke(200);
    line(this.pt1.position.x, this.pt1.position.y, this.pt2.position.x, this.pt2.position.y);
};

// ---- Mover Class : The Nature of Code by Daniel Shiffman http://natureofcode.com
function Mover(x,y) {
  this.mass = 10;
  this.position = createVector(x,y);
  this.velocity = createVector(0,0);
  this.acceleration = createVector(0,0);
  // Arbitrary damping to simulate friction / drag 
  this.damping = 0.7;
  //this.dragOffset = createVector(0,0);
  //this.dragging = false;
}

// Newton's 2nd law: F = M * A
// or A = F / M
Mover.prototype.applyForce = function(force) {
  var f = p5.Vector.div(force,this.mass);
  this.acceleration.add(f);
};
  
Mover.prototype.update = function() {
  // Velocity changes according to acceleration
  this.velocity.add(this.acceleration);
  // Damping
  this.velocity.mult(this.damping);
  // position changes by velocity
  this.position.add(this.velocity);
  // bounce
  // this.bounce();
  // We must clear acceleration each frame
  this.acceleration.mult(0);
};

Mover.prototype.display = function() {
  stroke(255);
  strokeWeight(2);
  fill(180,127);
  ellipse(this.position.x,this.position.y,this.mass*2,this.mass*2);
};

// Bounce off bottom of window
Mover.prototype.bounce = function() {
  var hasBounced = 0; // 0:no bounce, 1:X, 2:Y, 3:both
  if (this.position.y > height) {
    // A little dampening when hitting the bottom
    this.velocity.y *= -1;
    this.position.y = height;
	hasBounced += 2;
  }
   if (this.position.y < 0 ) {
    // A little dampening when hitting the bottom
    this.velocity.y *= -1;
    this.position.y = 0;
	hasBounced += 2;
  }
  if (this.position.x > width) {
    // A little dampening when hitting the bottom
    this.velocity.x *= -1;
    this.position.x = width;
	hasBounced += 1;
  }
   if (this.position.x < 0 ) {
    // A little dampening when hitting the bottom
    this.velocity.x *= -1;
    this.position.x = 0;
	hasBounced += 1;
  }
  return hasBounced;
};

// Elastic Class - made of Movers and Springs
function Elastic() {
  // Properties
  this.coords = [];
  this.centroid = createVector(0,0);
  this.arBob = [];
  this.arSpring = [];

  this.alpha = 255; // 
  this.c = color(2, 167, 224, this.alpha);
  this.wind = createVector(random(-20, 20), random(-20, 20)); 
  this.iWind = 0; // Apply wind to 1 point
  this.bounceTimeX = 0;
  this.bounceTimeY = 0;
  this.nbShapes = 7; 
}

// Init Elastic
Elastic.prototype.init = function(xPos, yPos) {
    this.chooseColor();
    var type = Math.floor((Math.random() * this.nbShapes));
    this.addCoords(type);
    this.initBobSpring(xPos, yPos);
};

// Update Elastic Shape
Elastic.prototype.update = function() {
	// Add wind to 1 point
	/*console.log("arBob:"+this.arBob.length);
	console.log("iWind:"+this.iWind);*/
	//console.log("arBob[iWind].position.x:"+this.arBob[this.iWind].position.x);
	
    this.arBob[this.iWind].applyForce(this.wind);
	
	var ii;    
    for (ii=0; ii<this.arSpring.length; ii++) {
      this.arSpring[ii].update();
    }
    for (ii=0; ii<this.arBob.length; ii++) {
      this.arBob[ii].update();
    }
    /*for (ii=0; i<this.arBob.length; ii++) {
      this.arBob[ii].drag(mouseX, mouseY);
    }*/

    // Update Wind
    var bChangeX = false;
    var bChangeY = false;
    var iChoc = 0;
    for (ii=0; ii<this.arBob.length; ii++) {
	  var hasBounced = this.arBob[ii].bounce();
      if (hasBounced > 0) {
	    bChangeX = bChangeX || (hasBounced & 1);
        bChangeY = bChangeY || (hasBounced & 2);
        iChoc = ii;
		// console.log("bChangeX:"+bChangeX+", bChangeY:"+bChangeY+"iChoc:"+iChoc);
      }
    }
    if (bChangeX) {
        this.wind.x *= -1;
        this.iWind = Math.floor((iChoc + ((this.arBob.length)/2)) % this.arBob.length);
		// console.log("bounceX, iChoc:"+iChoc+", iWind:"+this.iWind);
    } 

    if (bChangeY) {
		this.wind.y *= -1;
        this.iWind = Math.floor((iChoc + ((this.arBob.length)/2)) % this.arBob.length);
		// console.log("bounceY, iChoc:"+iChoc+", iWind:"+this.iWind);
    }
};

// Compute shape center
Elastic.prototype.updateCentroid = function () {
    // Update centroid
    this.centroid.set(0,0);
    for (var ii=0; ii<this.arBob.length; ii++) {    
      this.centroid.add(this.arBob[ii].position);
    }
    this.centroid.div(this.arBob.length);
  }

// Bump elastic : add force vector center->each point
Elastic.prototype.bump = function(strength) {

    this.updateCentroid();
    for (var ii=0; ii<this.arBob.length; ii++) {
      var force = p5.Vector.sub(this.arBob[ii].position, this.centroid);
      force.normalize();
      force.mult(strength/2);
      this.arBob[ii].applyForce(force);
    }
};
  
// Display Elastic
Elastic.prototype.display = function() {
    this.update();
    strokeWeight(15);
    stroke(this.c);
    noFill();
    beginShape();
    for (var ii=0; ii<this.arBob.length; ii++)
    {
      curveVertex(this.arBob[ii].position.x, this.arBob[ii].position.y);
    }
    // We need to overlap with the first three points to close the boundary.
    for (var ii=0; ii<3; ii++)
    {
      curveVertex(this.arBob[ii].position.x, this.arBob[ii].position.y);
    }
    endShape();

    // Display wind
    /*strokeWeight(2);
    line(this.arBob[this.iWind].position.x, this.arBob[this.iWind].position.y, 
    this.arBob[this.iWind].position.x+this.wind.x*4, this.arBob[this.iWind].position.y+this.wind.y*4);
    
	for (var ii=0; ii<this.arSpring.length; ii++)
		this.arSpring[ii].display();
	*/

};

// Init the springy nature of shape
Elastic.prototype.initBobSpring = function(xPos, yPos) {
    /* // Scale it 
    for (var ii=0; ii<this.coords.length; ii++) { 
      this.coords[ii].mult(1);
    } */

    // Create objects at starting position
    //this.arBob = new MoverBob[this.coords.length];
    for (var ii=0; ii<this.coords.length; ii++) {
		b = new Mover(this.coords[ii].x*0.1+xPos, this.coords[ii].y*0.1+yPos);
		this.arBob.push(b);
    }
    // Note third argument in Spring constructor is "rest length"
    //arSpring = new Spring[3*(this.coords.length)];
    // Step 1: Spring chain around points
    for (var ii=0; ii<this.coords.length; ii++) {
      var iNext = Math.floor((ii+1)%this.coords.length);
      var restLen = this.coords[ii].dist(this.coords[iNext]);
      this.arSpring.push(new Spring(this.arBob[ii], this.arBob[iNext], restLen));
    }
	// Step 2 : Spring chain skipping 1 point
    for (var ii=0; ii<this.coords.length; ii++) {
      var iNext = (ii+2)%this.coords.length;
      var restLen = this.coords[ii].dist(this.coords[iNext]);
	  this.arSpring.push(new Spring(this.arBob[ii], this.arBob[iNext], restLen));    
	}
	// Step 3 : Spring chain skipping 2 points (or more)
    for (var ii=0; ii<this.coords.length; ii++) {
      var iNext = (ii+3)%this.coords.length;
      //int iNext = (i+this.coords.length/2)%this.coords.length;
      var restLen = this.coords[ii].dist(this.coords[iNext]);
      this.arSpring.push(new Spring(this.arBob[ii], this.arBob[iNext], restLen));
    }

};

Elastic.prototype.chooseColor = function() {
    var num = Math.floor((Math.random() * 3));
    switch(num) {
    case 0:
      this.c = color(2, 167, 224, this.alpha); // Blue
      break;
    case 1:
      this.c = color(254, 243, 0, this.alpha); // Yellow
      break;
    case 2:
      this.c = color(239, 49, 56, this.alpha); // Red
      break;
    }

};

Elastic.prototype.addCoords = function(type) {
    switch(type) {
    case 0:
      this.coords.push(createVector(80.22223, 65.33333));
      this.coords.push(createVector(23.222229, 84.33333));
      this.coords.push(createVector(2.222229, 46.33333));
      this.coords.push(createVector(-49.77777, -6.6666718)); 
      this.coords.push(createVector(-85.77777, -51.66667));
      this.coords.push(createVector(-58.77777, -74.66667));
      this.coords.push(createVector(-7.777771, -39.66667));
      this.coords.push(createVector(44.22223, -3.6666718));
      this.coords.push(createVector(84.22223, 7.3333282));
      break;
    case 1:
      this.coords.push(createVector(-32.0, -27.600006));
      this.coords.push(createVector(11.0, -25.600006));
      this.coords.push(createVector(35.0, 9.399994));
      this.coords.push(createVector(18.0, 28.399994));
      this.coords.push(createVector(-32.0, 15.399994));      
      break;
    case 2:
      this.coords.push(createVector(-92.600006, -70.8));
      this.coords.push(createVector(-55.600006, -78.8));
      this.coords.push(createVector(-17.600006, -56.800003));
      this.coords.push(createVector(18.399994, -28.800003));
      this.coords.push(createVector(64.399994, 4.199997));
      this.coords.push(createVector(69.399994, 38.199997));
      this.coords.push(createVector(26.399994, 38.199997));
      this.coords.push(createVector(-16.600006, 6.199997));
      this.coords.push(createVector(-66.600006, -28.800003));
      break;
    case 3:
      this.coords.push(createVector(-87.0, 90.33333));
      this.coords.push(createVector(-91.0, 47.33333));
      this.coords.push(createVector(-54.0, 25.333328));
      this.coords.push(createVector(-17.0, -14.666672));
      this.coords.push(createVector(21.0, -47.66667));
      this.coords.push(createVector(45.0, -93.66667));
      this.coords.push(createVector(88.0, -99.66667));
      this.coords.push(createVector(94.0, -53.66667));
      this.coords.push(createVector(53.0, -22.666672));
      this.coords.push(createVector(13.0, 16.333328));
      this.coords.push(createVector(-20.0, 55.33333));
      this.coords.push(createVector(-45.0, 97.33333));
      break;
    case 4:    
      this.coords.push(createVector(-10.0, 9.333328));
      this.coords.push(createVector(-4.0, -8.666672));
      this.coords.push(createVector(14.0, -0.66667175));
      break;
    case 5:
      this.coords.push(createVector(-31.0, 4.25));
      this.coords.push(createVector(-27.0, -29.75));
      this.coords.push(createVector(8.0, -23.75));
      this.coords.push(createVector(50.0, 49.25));
      break; 
    case 6: // circle
      this.coords.push(createVector(-13.25, 19.0));
      this.coords.push(createVector(-18.25, -14.0));
      this.coords.push(createVector(12.75, -18.0));
      this.coords.push(createVector(18.75, 13.0));
      break;
    default:
      this.coords.push(createVector(-64.55556, 58.33333));
      this.coords.push(createVector(-44.555557, 6.3333282));
      this.coords.push(createVector(-71.55556, -36.66667));
      this.coords.push(createVector(-7.5555573, -39.66667));
      this.coords.push(createVector(28.444443, -84.66667));
      this.coords.push(createVector(77.44444, -30.666672));
      this.coords.push(createVector(41.444443, 12.333328));
      this.coords.push(createVector(46.444443, 70.33333));
      this.coords.push(createVector(-5.5555573, 44.33333));
      break;
    }
    // Random Move & Rotate Coords
    //float xtrans = random(-50, width-200); 
    //float ytrans = random(0, height-250); 
    var rot = random(0, 6.28);
    for (var ii=0; ii<this.coords.length; ii++)
    {
      //this.coords[ii].x += xtrans;
      //this.coords[ii].y += ytrans;
      this.coords[ii].rotate(rot);
    }
};

// Static function 
var chooseColor = function(index) {
	var alpha = 255;
	var c = color(2, 167, 224, alpha);
    switch(Math.floor(index) % 3) {
    case 0:
      c = color(2, 167, 224, alpha); // Blue
      break;
    case 1:
      c = color(254, 243, 0, alpha); // Yellow
      break;
    case 2:
      c = color(239, 49, 56, alpha); // Red
      break;
    }
	return c;
};
