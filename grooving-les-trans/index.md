---
title: Grooving les Trans
description: Faites groover l'affiche des transmusicales de Rennes par (c) Jamesmarsh.com, 2014.
image: grooving-les-trans.jpg
category: audio
publish: true
tags: [generative, art, visual, audio]
---

# L'harmonographe

L'harmonographe est à l'orgine une machine "à dessiner" qui utilise 2 pendules asynchrones 
pour créer des oscillations hypnotiques. Ce sketch en propose une implémentation avec quelques 
exemples et la possibilité de faire varier les paramètres. 