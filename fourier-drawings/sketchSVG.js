var img, path;
let idx = 0; 

function preload() { 
  img = loadSVG('data/logoGueuloNoir.svg');
}

function setup() {
    createCanvas(1000, 600, SVG);
    background(100,100,0);
    image(img.elt, 0, 0, 1000, 600);
    path = img.elt.querySelectorAll('path')
    path.attribute('fill', color(200, 0, 0));
    print("idx "+idx  );
  }
  
  function draw() {
    show_idx();
  }

  function show_idx() {
    fill(0);
    rect(width-400,height-20,450,20);
    fill(200);
    text("use UP DOWN key, idx: "+idx+", id= "+path.attribute('id')+", id2= "+path.attribute('id2')  ,width-395,height-5);
  }
  