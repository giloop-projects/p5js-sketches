function setup() {
  // put setup code here
}

function draw() {
  // put drawing code here
  noFill();
  curve(5, 26, 73, 24, 73, 61, 15, 65);
  let steps = 6;
  for (let i = 0; i <= steps; i++) {
    let t = i / steps;
    let x = curvePoint(5, 73, 73, 15, t);
    let y = curvePoint(26, 24, 61, 65, t);
    //ellipse(x, y, 5, 5);
    let tx = curveTangent(5, 73, 73, 15, t);
    let ty = curveTangent(26, 24, 61, 65, t);
    let a = atan2(ty, tx);
    a -= PI / 2.0;
    line(x, y, cos(a) * 8 + x, sin(a) * 8 + y);
  }
}