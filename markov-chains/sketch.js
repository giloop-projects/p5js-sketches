let stateNames = ['Rez de Chaussée', '1er étage', '2è étage', 'Grenier']
let upstairsProbas = [
    [1, 1, 0, 0], // Ground 0 → others
    [0, 1, 1, 0], // 1st floor → others
    [0, 0, 1, 1], // 2nd floor→ others
    [0, 0, 0, 1], // 3rd floor → others
]
let downstairsProbas = [
  [1, 0, 0, 0], // Ground 0 → others
  [1, 1, 0, 0], // 1st floor → others
  [0, 1, 1, 0], // 2nd floor→ others
  [0, 0, 1, 1], // 3rd floor → others
]


let mc;
let txtColor;
let updateFr = 20; // Update state every ... frames
let nbChanges = 0;
let direction = 'monte';

function setup() {
  let cnv = createCanvas(windowWidth * 0.99, 150);
  txtColor = color(172, 3, 3);
  mc = new MarkovChain(upstairsProbas,0);
  frameRate(20);
  textAlign(LEFT, TOP);
}

function windowResized() {
  resizeCanvas(0.98*windowWidth, 150);
  targetSize = min(floor(0.75*windowHeight), floor(0.75*windowWidth));
} 

function draw() {
  background(240,245,240,125);
  
  fill(128, map(mc.currentState, 0, mc.nStates-1, 200, 255), 128);
  noStroke()
  rect(290, height-30*(1+mc.currentState)-5, 200, 30);

  noStroke();
  if (direction=='monte') {
    fill(0, 102, 253);
    triangle(5,height-20, 50,height-20, 25, 20)
  } else {
    fill(253, 102, 0);
    triangle(5,20, 50, 20, 25, height-20)
  }

  text(stateNames[mc.currentState], 300, height-30*(1+mc.currentState))
  
  textSize(20);
  text('Allez Hop, on ' + direction + ' ...' , 55, 25);
  text('Mise à jour :'+nbChanges, 55, 50 )
  
  
  
  // Update state 
  if (frameCount % updateFr == 0) {
    mc.updateState()
    nbChanges++;
  }
  // Change direction when last target floor reached
  if (direction == 'monte') {
    if (mc.currentState == mc.nStates-1) {
      direction = 'descend';
      mc.updateFullMatrix(downstairsProbas);
    }
  } else { // downstairs
    if (mc.currentState == 0) {
      direction = 'monte'
      mc.updateFullMatrix(upstairsProbas);
    }
  }
}