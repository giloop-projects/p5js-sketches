---
title: Chaînes de Markov
description: Un exemple simple pour comprendre l'utilité des chaînes de Markov (MC), ainsi qu'une librairie pour implémenter le décodage et la simulation d'un MC. 
image: image.jpg
category: code
tags: [markov chains, algorithm, library]
---

# Always start with a blank page 

Pour démarrer avec une page blanche, je recopie ce dossier et le renomme. 

Utiliser la commande suivante pour copier un dossier dans la console de studio code ou autre (l'option /E assure la copie des sous-dossiers et /I considère la destionation comme un dossier et le crée si besoin).  

```
xcopy.exe empty-example new-example /E/I
```
