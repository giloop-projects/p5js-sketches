/** Sketch for playing a infinite random song based on synchronized loops
 * Samples are listed in sounds.json
 * There are grouped by instrument as defined in arInstruments. If the filename contains 
 * the instrument name it is affected to the instrument track.
 * The main audio loop is a p5.Part object. 
 * For each loop the phrases are regenerated at the last step of the phrase. 
 * The instruments added to the part are chosen randomly using a Markov Chain 
 * When an instrument is to be played, one of the available samples is randomly selected.
 * 
 * @author Gilles Gonon <gilles.gonon@free.fr>
 */

let arFiles = []; // Array of objects  { 'part': partName, 'num': soundNum, 'file': el, 'sound': sound }
let setParts = []; // Set of tracks based on filenames 
let myPart; // p5.Part object holding all phrases
let reverb; // Global reverb effect
let startTime = undefined;
let arStateInstr;
let oLoad; // BatchLoaderInfo object for tracking samples loading process
let bLoaded = false; // Flag : all samples loaded
let bAudioStarted = false; // Used to start audio on user event (for Chrome)
let bPlaying = false;

// Sketch text props
let txtSize = 16;
let txtColor;
let txtInfo;

// Sample filenames for each track must contains one of these text
const arInstruments = ['piano', 'bass', 'drum', 'velvet', 'kontakt'];
// Tracks are C(hords), P(iano), B(ass), S(ynth), D(rums)
const dictInstr = {'C':'velvet', 'P':'piano', 'B':'bass','D':'drum','S':'kontakt'};
// States are : C, C+P, C+D, C+P+B, C+P+B+D, C+P+B+D+S, B, B+D, C+S, P
const stateNames = ['C', 'C+P', 'C+D', 'C+P+B', 'C+P+B+D', 'C+P+B+D+S', 'B', 'B+D', 'C+S', 'P']
let markovParts; // Markov Chain object
// Transition matrix for track arrangement
let partsProbas = [
  // C CP CD CPB CPBD CPBDS B  BD CS P
    [1, 1, 2, 3,  5,   0,   0, 1, 0, 0], // C → other
    [0, 0, 0, 1,  2,   1,   0, 0, 0, 1], // C+P → other
    [0, 0, 0, 1,  1,   1,   1, 0, 0, 0], // C+D → other
    [0, 1, 0, 1,  3,   3,   0, 0, 1, 0], // C+P+B → other
    [0, 1, 0, 1,  3,   3,   0, 0, 1, 0], // C+P+B+D → other
    [1, 1, 0, 1,  3,   1,   0, 0, 1, 0], // C+P+B+D+S → other
    [0, 0, 0, 1,  1,   1,   0, 1, 0, 0], // B → other
    [0, 0, 0, 1,  1,   1,   0, 0, 0, 0], // B+D → other
    [1, 1, 1, 3,  3,   2,   0, 0, 1, 0], // C+S → other
    [0, 1, 1, 2,  3,   1,   0, 0, 0, 1], // P → other
]


class BatchLoaderInfo {
    constructor() {
        this.nFiles = 0;
        this.arLoading = [];
    }

    addFile() {
        // Add a new loading percentage and return its idx in array
        this.nFiles++;
        return (this.arLoading.push(0) - 1);
    }

    updateLoading(idx, value) {
        // Callback used for loadSound 
        this.arLoading[idx] = value;
    }

    getLoadRate() {
        if (this.arLoading.length) {
            return (this.arLoading.reduce((a, b) => a + b, 0) / this.nFiles);
        } else {
            return 0;
        }
    }
    isLoaded() {
        // For some reason load rate never reaches 1.0 with p5
        return (this.getLoadRate() >= 0.989);
    }
}


function setup() {
    let cnv = createCanvas(360, 250);
    // Add reverb for all sounds 
    reverb = new p5.Reverb();
    reverb.set(1, 5);
    myPart = new p5.Part();
    myPart.setBPM(15, 0.1); // original BPM is 120, loops are 4 bars long -> 120/4
    oLoad = new BatchLoaderInfo();
    loadJson('sounds.json');
    txtColor = color(172, 3, 3);
    markovParts = new MarkovChain(partsProbas, 4);
    txtInfo = 'Click anywhere to start music (and wait a bit)';
}

function draw() {
    background(200);
    background(255, 230, 200, 100);
    fill(txtColor);
    textSize(txtSize);
    if (bLoaded) {
        noStroke();
        text(txtInfo, 10, 20);
        // text('Loaded ' + arFiles.length + ' files from JSON \n' + 'Parts:' + (new Array(...setParts).join(', ')), 10, 20);
        drawInstrument();
    } else {
        if (oLoad.isLoaded()) {
            bLoaded = true;
        } else {
            text('Loading sounds ... ' + round(oLoad.getLoadRate() * 100) + '%, ' + arFiles.length + ' files', 10, 0.25 * height);
        }
    }
}

function mouseClicked() {
    if (bAudioStarted == false) { // Toggle first start (for Chrome)
        startPart();
        bAudioStarted = true;
        txtInfo = 'Click anywhere to stop music';
        startTime = millis();
    } else {
        if (bPlaying){ 
            console.log('pause');
            txtInfo = 'Click anywhere to start music (and wait a bit)';
            stopSong();
            logPlayTime();
        } else {
            console.log('play');
            txtInfo = 'Click anywhere to stop music';
            myPart.start();
            startTime = millis();
        }
        bPlaying = !bPlaying;
    }
    
    
  }

function loadJson(file) {
    // Load files
    fetch(file).then(response => { return response.json(); })
        .then(data => {
            console.log(data);

            arFiles = data.map(el => {
                // Add the infos in ar sounds
                let arParse = el.split('-');
                let partName = arParse.slice(0, -1).join('-');
                let soundNum = arParse[arParse.length - 1].split('.')[0];
                // Start downloading sound
                let sound = loadPartSounds(el);
                return { 'part': partName, 'num': soundNum, 'file': el, 'sound': sound };
            })
            setParts = new Set(arFiles.map(el => el.part));

            // setParts.forEach(el => console.log(el))

        })
        .catch(err => {
            console.log('Something went wrong while reading file list :' + err);
        });
}



function loadPartSounds(filename) {
    let idx = oLoad.addFile();
    let newSound = new p5.SoundFile('data/sounds/' + filename,
        function success() { console.log(filename + ' loaded'); },
        function error(err) { console.log('problem loading ' + filename + ':' + err); },
        function loading(value) { oLoad.updateLoading(idx, value); }
    );
    return newSound;
}


function generatePart() {
    arInstruments.forEach(
        inst => {
            // True at the end of piano phrase will be used to trigger update of all phrases
            let instPhrase = new p5.Phrase(inst, playSound, [inst, 0, 0, 0, 0, 0, 0, inst == 'piano']);
            myPart.addPhrase(instPhrase);
        }
    )
}

function playSound(time, name) {
    // Callback appelé par la partie, permet soit de : 
    // - déclencher les sons de chaque piste 
    // - générer une nouvelle part (generatePart())
    if (typeof (name) == "string") {
        // Filter only sounds with given name 
        let arSounds = arFiles.filter(el => el.part.toLowerCase().includes(name))
        // Choix d'un son aléatoire pour l'instrument
        idx = floor(random(0, arSounds.length)); // 0; // 
        console.log('Playing : '+name+'['+idx+']')

        arSounds[idx].sound.play(time);
    } else if (typeof (name) == "boolean") {
        if (name) {
            // regenerate all phrases according to new Markov
            markovParts.updateState();
            console.log('Generate new part → ' + stateNames[markovParts.currentState]);
            updatePhrases();
        } // else { // nothing to do }
    }
}

function startPart() {
    userStartAudio();
    generatePart();
    myPart.loop();
    // myPart.start();
    bPlaying = true;
}

function stopSong() {
    myPart.stop();
    // Also stop all sounds
    arFiles.forEach(el => el.sound.stop());
}



function updatePhrases() {
    // stateNames = ['C', 'C+P', 'C+D', 'C+P+B', 'C+P+B+D', 'C+P+B+D+S', 'B', 'B+D', 'C+S', 'P']
    // Convert stateNames into an array of instruments for creating phrases
    arStateInstr = stateNames[markovParts.currentState].split('+').map(el=>dictInstr[el]);
    let bFirst = true;
    
    arInstruments.forEach(
        inst => {
            if (arStateInstr.includes(inst)) {
                // Play inst
                sequence = [inst,0,0,0,0,0,0,bFirst];
            } else {
                // Don't play it
                sequence = [0,0,0,0,0,0,0,bFirst];
            }
            
            myPart.replaceSequence(inst, sequence)
            // True at the end of piano phrase will be used to trigger update of all phrases
            bFirst = false;
        }
    )
}


function logPlayTime() {
    // Try to log how long it was played
    var fd = new FormData();
    fd.append('sketch', 'infinite-loop');
    fd.append('playTime', round((millis()-startTime) / 1000));
    navigator.sendBeacon('logTime.php', fd);
}

// window.onbeforeunload = function(event) {
//         event.preventDefault();
// 	var fd = new FormData();
// 	fd.append('sketch', 'infinite-loop');
//     fd.append('playTime', round(millis()/1000));
// 	navigator.sendBeacon('logTime.php', fd);
//     event.returnValue = '';
// }

function drawInstrument() {
    let row = 0;
    const cActive = color(255,128,0);
    const strokeActive = txtColor;
    const cInactive = color(255,230,200);
    const xOff = 75;
    const yOff = 55;
    textSize(16);
    fill(cInactive); noStroke();
    text('samples', xOff, yOff-0.5*txtSize);

    for (let inst=0; inst<arInstruments.length; inst++) {
        let instFiles = arFiles.filter(el => el.part.toLowerCase().includes(arInstruments[inst]))
        
        for (let col = 0; col < instFiles.length; col++) {
            const el = instFiles[col];
            if (el.sound.isPlaying()) {
                fill(cActive);
                stroke(strokeActive)
                strokeWeight(2)
            } else {
                fill(cInactive)
                noStroke()
            }
            rect(xOff+col*36, yOff+inst*36, 34, 34, 6);
        }
        
        fill(cInactive);
        if (arStateInstr) {
            if (arStateInstr.includes(arInstruments[inst])) { 
                fill(cActive);
            } else {
                fill(cInactive);
            }
        }
        noStroke();  
        text(arInstruments[inst], 10, yOff+txtSize+inst*36)
    }
}
