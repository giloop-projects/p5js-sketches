<?php
// Connexion à la BDD
require_once('../connect_db.php'); // create $dbh with your credentials

// try {
    
    if (isset($_POST['sketch']) && isset($_POST['playTime']) ) {
        $sql='INSERT INTO p5js (`id`, `date`, `sketch`, `log_1`, `log_2`) VALUES (NULL, CURRENT_TIMESTAMP, :sketch, :playTime, NULL)'; 
        $sth = $dbh->prepare($sql);
        $sth->execute($_POST);
    } else {
         // Redirect
         header('location:/404.html');
    }

    // Ferme la connexion à la DB
    $dbh = null;

// } catch (PDOException $e) {
//     // Redirect
//     header('location:/404.html');
// }

?>