/**
 * Loads a JavaScript file and returns a Promise for when it is loaded
 * Solution explained on https://aaronsmith.online/easily-load-an-external-script-using-javascript/
 */

const loadScript = src => {
  return new Promise((resolve, reject) => {
    const script = document.createElement('script')
    script.type = 'text/javascript'
    script.onload = resolve
    script.onerror = reject
    script.src = src
    document.head.append(script)
  })
}

loadScript('../lib/p5.sound.min.js')
  .then(() => loadScript('sketch-load.js'))
  .then(() => {
    // now safe to use all dependencies
  })
  .catch(() => console.error('Something went wrong.'))