---
title: Infinite Loop
description: Turn a few music loops into never-ending generative music
image: infinite-loop.png
category: audio
tags: [generative, art, visual, audio]
---

# Infinite music 

How long will you ear this nearly-loop generative music
