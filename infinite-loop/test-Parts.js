let reverb;
let pctLoaded = 0;
let myParts;
let bInstructions = true;
let txtSize = 16;
let txtSpeakers = '';
let txtColor;

class soundPart {
    constructor(name) {
        this.name = name;
        this.sounds = [];
        this.pctLoading = []
        this.soundsName = [];
        this.effect = undefined;
    }

    // Add a sound
    // Name should follow pattern : part-#.mp3        
    addSound(soundPath) {

        let nSounds = this.sounds.length;
        this.pctLoading[nSounds].push(0);

        function dLoading(progress, idx) {
            this.pctLoading[idx] = progress;
        }

        let newSound = new p5.SoundFile(soundPath,
            function success() { console.log(soundPath + ' loaded'); },
            function error() { console.log('problem loading ' + soundPath); },
            function loading(d) { dLoading(progress, nSounds); }
        );

        this.sounds.push(newSound);
    };

    // 
    addEffect(effect) {
        this.effect = effect;
    }

    parsePerson(soundPath) {
        // Remove path if present
        const filename = soundPath.split('/').slice(-1).join();
        // Name should follow pattern : name-person-#.mp3
        return filename.split('-').slice(1, -1).map(capitalizeFirstLetter).join('-');
    }

    // play one of the sound with given or random index
    playSound(idx = -1) {
        if (this.sounds.length == 0) return -1;
        if (idx == -1 || idx >= this.sounds.length) { idx = floor(random(0, this.sounds.length)); };
        //console.log(`Playing ${this.strIndex}[${idx}]`)
        if (this.effect) {
            this.effect.process(this.sounds[idx]);
        }
        this.sounds[idx].play();

        return idx;
    };


    pctLoaded() {
        const testLoaded = (accum, snd) => accum && snd.isLoaded();
        return this.sounds.reduce(testLoaded, true);
    }
};


function loadParts(jsonFile) {
    // Load all sounds listed in a JSON file
    function processJSON(data) {
        console.log(data);
        data.map(el => {
            // Add the sample to mySounds
            console.log(el);
            this.addSound(soundsDir, el);
        });
    }
    fetch(soundsJson).then(response => { return response.json(); })
    .then(data => {
        console.log(data);
        data.map(el => {
            // Add the sample to mySounds
            console.log(el);
            this.addSound(soundsDir, el);
        })}
    .catch(err => {
        console.log('Something went wrong while reading file list :' + err);
        });
}

function setup() {
    let cnv = createCanvas(windowWidth * 0.99, 200);
    // Load sounds
    loadParts()
    soundFormats('mp3');
    myList = new SoundList('data/', 'sounds.json');

    // Add reverb for all sounds 
    reverb = new p5.Reverb();
    reverb.set(1, 5);

    txtColor = color(172, 3, 3);

    background(200, 0, 0, 100);
    fill(txtColor);
    textAlign(CENTER, CENTER);
    windowResized();
}

function windowResized() {
                resizeCanvas(0.99 * windowWidth, 0.15 * windowHeight);
                txtSize = floor(map(width, 300, 1600, 12, 30));
            } 

function drawLoading() {
                let pct = myList.getPctLoaded();
                background(200, 0, 0, 100);
                textSize(txtSize);
                textAlign(CENTER, CENTER)
                if (pct > 99) {
                    text('Chargé', 0.5 * width, 0.5 * height);
                    bAllLoaded = true;
                    myList.addEffect(reverb);
                    windowResized();
                } else {
                    noStroke();
                    fill(253, 200 / (201 - 2 * pct), 200 / (201 - 2 * pct));
                    rect(0, 0, 0.01 * pct * width, 0.1 * height);
                    fill(253, 200, 200);

                    text('Chargement des sons en cours ...', 0.5 * width, 0.5 * height);
                }
            }

function draw() {
                if (!bAllLoaded) {
                    drawLoading();
                } else {
                    background(255, 230, 200, 100);
                    fill(txtColor);
                    textSize(txtSize);
                    if (bInstructions) {
                        // drawInstructions();
                        textAlign(CENTER, CENTER);
                        text('Cliquez sur l\'image pour entendre la phrase', 0.05 * width, 0.05 * height, 0.9 * width, 0.9 * height);
                    } else {
                        // updated by displaySpeakers()
                        textSize(txtSize);
                        textAlign(CENTER, CENTER);

                        text('Phrase lue par : \n' + txtSpeakers, 0.05 * width, 0.05 * height, 0.9 * width, 0.9 * height);
                    }
                }
            }

function splitLongString(myText, minWidth, minChars) {
                // Texte sur 2 ou 3 lignes suivant la taille
                if (width < minWidth && myText.length > minChars) {
                    let txtAffiche = myText;
                    // 2 lignes 
                    let vPos = myText.search(',');
                    while (vPos > 0 && vPos < txtSpeakers.length * 0.5) {
                        vPos += txtSpeakers.substring(vPos + 1).search(',') + 1;
                    }
                    // Replace ',' at vPos
                    if (vPos > -1) {
                        txtAffiche = txtSpeakers.substr(0, vPos) + '\n' + txtSpeakers.substr(vPos + txtSpeakers.length);
                    }
                    return myText;
                } else {
                    // no change
                    return myText;
                }
            }

function playPhrase() {
                bInstructions = false;
                myList.playSequence(phrase.split(' '));
            }

function displaySpeakers() {
                // Get persons who spoke
                let arSpeakers = [];
                for (let i = 0; i < myList.arPlayed.length; i++) {
                    const name = arMots[i];
                    const iPlayed = myList.arPlayed[i];
                    arSpeakers.push(myList.findSoundName(name).person[iPlayed])
                }
                document.getElementById('info').innerText = 'Phrase lue par : ' + arSpeakers.join(', ')
                txtSpeakers = arSpeakers.join(', ');
            }

/*
function mousePressed() {
  bInstructions = false;
  playPhrase();
}

function keyPressed() {
  playPhrase();
  bInstructions = false;
}
*/
